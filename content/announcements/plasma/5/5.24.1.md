---
date: 2022-02-15
changelog: 5.24.0-5.24.1
layout: plasma
video: false
asBugfix: true
draft: false
highlights_title: Highlights
---

{{< highlight-grid >}}

+ Discover: Don't trigger updates while busy. [Commit.](http://commits.kde.org/discover/f8638e857c346c8293ec1e47c535219eb3f7961c)
+ KScreen: Workaround unknown Qt issue that causes the revert dialog to be invisible. [Commit.](http://commits.kde.org/kscreen/3fb6eb435b752981721e152c39c7d0fbf589042e) Fixes bug [#449560](https://bugs.kde.org/449560)
+ Powerdevil: Support hardware with only one charging threshold, not both. [Commit.](http://commits.kde.org/powerdevil/29154c13a186f5d13191db4bca1984ba2a21b839) Fixes bug [#449997](https://bugs.kde.org/449997)
