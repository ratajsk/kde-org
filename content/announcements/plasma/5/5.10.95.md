---
aliases:
- ../../plasma-5.10.95
changelog: 5.10.5-5.10.95
date: 2017-09-14
layout: plasma
title: Plasma 5.11 Beta Makes the Desktop More Powerful, Elegant and Secure
figure:
  src: /announcements/plasma/5/5.11.0/plasma-5.11.png
---

{{% i18n_date %}}

Today KDE publishes a testing release of this autumn's Plasma feature release, KDE Plasma 5.11, to be released in mid-October 2017. Plasma 5.11 will bring a redesigned settings app, improved notifications, a more powerful task manager. Plasma 5.11 will be the first release to contain the new “Vault”, a system to allow the user to encrypt and open sets of documents in a secure and user-friendly way, making Plasma an excellent choice for people dealing with private and confidential information.

## New System Settings Design

{{<figure src="/announcements/plasma/5/5.11.0/system-settings.png" alt="System Settings New Design " class="text-center" width="600px" caption="System Settings New Design">}}

The revamped System Settings user interface allows easier access to commonly used settings. It is the first step in making this often-used and complex application easier to navigate and more user-friendly. The new design is added as an option, users who prefer the older icon or tree views can move back to their preferred way of navigation.

## Notification History

{{<figure src="/announcements/plasma/5/5.11.0/notification-history.png" alt="Notification History " class="text-center" width="600px" caption="Notification History">}}

Due to popular demand notifications optionally stores missed and expired notifications in a history. This is the first part of an ongoing effort to modernize the notification system in Plasma. This allows the user to override applications not marking their notifications as persistent, and viewing what happened in her absence. The notifications history is enabled by default for testing purposes, but may be switched off in the final 5.11 release as to provide a cleaner and uncluttered appearance out of the box. The Plasma team welcomes feedback from testers specifically about this new feature.

## Task Manager Improvements

{{<figure src="/announcements/plasma/5/5.11.0/task-manager-kate.png" alt="Kate with Session Jump List Actions " class="text-center" width="600px" caption="Kate with Session Jump List Actions">}}

Plasma's Task Manager lays the foundation for enabling applications to provide dynamic jump list actions. In Plasma 5.10, applications had to define additional actions added to their task entries statically. The new functions make it possible for applications to provide access to internal functions (such as a text editor's list of sessions, options to change application or document state, etc.), depending on what the application is currently doing. Moreover, rearranging windows in group popups is now possible, allowing the user to make the ordering of his opened applications more predictable. On top of all these changes, performance of the task manager has been improved for a smoother operation.

## Plasma Vault

{{<figure src="/announcements/plasma/5/5.11.0/plasma-vault.png" alt="Plasma Vault Stores Your Files Securely " class="text-center" width="600px" caption="Plasma Vault Stores Your Files Securely">}}

For users who often deal with sensitive, confidential and private information, the new Plasma Vault offers strong encryption features presented in a user-friendly way. Plasma Vault allows to lock and encrypt sets of documents and hide them from prying eyes even when the user is logged in. These 'vaults' can be decrypted and opened easily. Plasma Vault extends Plasma's activities feature with secure storage.

## App Launcher Menu Improvements

{{<figure src="/announcements/plasma/5/5.11.0/kickoff-edit-applications.png" alt="Edit Application Entries Direct from Menu. " class="text-center" width="600px" caption="Edit Application Entries Direct from Menu.">}}

{{<figure src="/announcements/plasma/5/5.11.0/kicker.png" alt="Kicker without Sidebar " class="text-center" width="600px" caption="Kicker without Sidebar">}}

Search results in launchers have gained features previously only available to applications listed on the menu. You no longer need to manually look for an application just to edit or uninstall it. The Kicker application launcher now hides its sidebar if no favorites are present, leading to a cleaner look. It also supports choosing an icon from the current icon theme rather than only pictures on your hard drive.

Folder View which became the default desktop layout in Plasma 5.10 saw many improvement based on user feedback. It supports more keyboard shortcuts, such as Ctrl+A to “Select All”, and spreads icons more uniformly across the visible area to avoid unpleasant gaps on the right and bottom edges of a screen. Moreover, startup has been sped up and interacting with icons results in significantly less disk access.

The different app menus now all share which applications are listed as favourites so you don't lose your settings if you decide to change your launcher.

## Wayland

{{<figure src="/announcements/plasma/5/5.11.0/dual-monitor-dpi-change-wayland.jpg" alt="One app window, two monitors, two DPIs " class="text-center" width="600px" caption="One app window, two monitors, two DPIs">}}

Wayland is the next generation display server technology making its entry in the Linux desktop. Wayland allows for improved visual quality and less overhead while providing more security with its clearer protocol semantics. A complete Wayland session's most visible feature is probably smoother graphics, leading to a cleaner and more enjoyable user experience. Plasma's Wayland support has been long in the making, and while it isn't fully there as a replacement for X11, more and more users enjoy Wayland on a daily basis.

A lot of work has been put into Plasma on Wayland. KWin, Plasma's Wayland compositor, can now automatically apply scaling based on the pixel density of a screen and even do so for each screen individually. This will significantly improve user experience on setups with multiple monitors, such as when a regular external monitor is connected to a modern high-resolution laptop. Moreover, legacy applications not supporting this functionality may be upscaled to remain readable.

Work has started to allow for a completely X-free environment, starting the Xwayland compatibility layer only when an application requires it. This will eventually result in improved security and performance as well as reduced resource consumption. Furthermore, it is now possible to use ConsoleKit2 instead of logind for setting up the Wayland session, extending the number of supported platforms.

Additional improvements include:

- Application identification logic, used for e.g. pinning apps in Task Manager, has been greatly improved
- Audio indicator in Task Manager denoting an application playing sound is now available
- Restricted window move operations are now possible
- Window shortcuts can be also assigned to Wayland windows
- Window title logic has been unified between X and Wayland windows
