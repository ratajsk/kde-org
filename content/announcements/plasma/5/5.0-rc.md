---
aliases:
- ../../plasma-5.0-rc
date: '2014-07-08'
description: KDE Ships Release Candidate for Plasma 5.
layout: plasma
noinfo: true
title: KDE Ships Release Candidate of Plasma 5
---

{{<figure src="/announcements/plasma/5/5.0-rc/plasma-5.0-rc.png" class="text-center" alt="Plasma 5" width="600px" >}}

{{% i18n_date %}}

KDE has today made available the candidate for the first release of Plasma 5, the next generation desktop. This is one last chance to test for bugs and check for problems before the final release next week.
