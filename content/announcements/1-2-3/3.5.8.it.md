---
aliases:
- ../announce-3.5.8
custom_about: true
date: '2007-10-16'
title: Annuncio del rilascio di KDE 3.5.8
---

<h3 align="center">
   Il progetto KDE rilascia l'ottava versione di servizio e di traduzione della serie 3.5 di questo importante ambiente Desktop libero
</h3>

<p align="justify">
  KDE 3.5.8 include traduzioni in 65 lingue, miglioramenti nella suite KDE PIM e anche in altre applicazioni.
</p>

<p align="justify">
  Il <a href="http://www.kde.org/">progetto KDE</a> annuncia oggi l'immediata
  disponibilit&agrave; di KDE 3.5.8, una versione di manutenzione per l'ultima generazione del pi&ugrave; avanzato e
  potente Desktop <em>libero</em> per GNU/Linux e altri UNIX. KDE attualmente supporta
  <a href="http://l10n.kde.org/stats/gui/stable/">65 lingue</a> e ci&ograve; lo rende accessibile a pi&ugrave; persone
  di quanto lo possa fare la maggior parte del software proprietario. Inoltre KDE pu&ograve; essere esteso facilmente 
  per supportare il lavoro proveniente da gruppi che desiderano contribuire nel progetto Open Source.
</p>
<?php // Changed stuff from 3.5.7 from here. ?>

<p align="justify">
    Mentre l'obiettivo principale degli sviluppatori &egrave; finire KDE 4.0, la serie stabile 3.5 resta il Desktop di scelta per
    il tempo restante. &Egrave; provato, stabile e ben supportato. La versione 3.5.8 con i suoi, letteralmente, centinaia di bug corretti
    migliora nuovamente l'esperienza degli utenti. L'obiettivo principale dei miglioramenti di KDE 3.5.8 &egrave;:

  <ul>
      <li>
      miglioramenti in Konqueror e nel suo componente di navigazione del Web, KHTML. Sono stati sistemati bug di gestione delle connessioni HTTP, KHTML ha migliorato il supporto di alcune funzionalit&agrave; di CSS per una maggior conformit&agrave; agli standard.
      </li>
      <li>
      il pacchetto kdegraphics ottiene, con questo rilascio, molte correzioni nel visualizzatore PDF di KDE e in Kolourpaint, una applicazione di disegno.
      </li>
      <li>
      la suite PIM di KDE ha avuto, come sempre, numerose correzioni relative alla stabilit&agrave;; tali correzioni sono relative a KMail, l'applicativo per
      gestire la posta elettronica in KDE, a KOrganizer, applicazione di agenda, e vari altri piccoli ritocchi.
      </li>
  </ul>

</p>

<p align="justify">
  Per una lista pi&ugrave; dettagliata di miglioramenti rispetto a
  <a href="http://www.kde.org/announcements/announce-3.5.7-it">KDE 3.5.7</a> del 22 maggio 2007, fai
  riferimento al collegamento:
  <a href="http://www.kde.org/announcements/changelogs/changelog3_5_7to3_5_8">Cambiamenti KDE 3.5.8</a>.
</p>

<p align="justify">
  KDE 3.5.8 viene distribuito con il desktop base e 15 pacchetti aggiuntivi (PIM, amministrazione,
  rete, istruzione, utilit&agrave;, multimedia, giochi, artwork, sviluppo web e altro).
  Le applicazioni e gli strumenti di KDE sono vincitori di molti riconoscimenti e sono disponibili
  in 65 lingue.
</p>

<h4>
  Distribuzioni che rilasciano KDE
</h4>
<p align="justify">
  La maggior parte delle distribuzioni Linux e dei sistemi operativi UNIX non includono immediatamente
  i nuovi rilasci di KDE, ma i pacchetti di KDE 3.5.8 saranno integrati nei loro prossimi rilasci.
  Controlla <a href="http://www.kde.org/download/distributions">la lista</a> per vedere quali distribuzioni
  sono rilasciate con KDE.
</p>

<h4>
  Installazione pacchetti binari KDE 3.5.8
</h4>
<p align="justify">
  <em>Creatori dei pacchetti</em>.
  Alcuni produttori del sistema operativo hanno fornito gentilmente i pacchetti binari di
  KDE 3.5.8 per alcune versioni delle loro distribuzioni; in altri casi i pacchetti
  sono stati forniti da alcuni volontari.
  Alcuni di questi pacchetti binari sono disponibili e possono essere scaricati liberamente 
  al server di scaricamento di KDE
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.8/">http://download.kde.org</a>.
  Altri pacchetti binari, cos&igrave; come gli aggiornamenti ai pacchetti attualmente presenti, saranno
  disponibili nelle settimane successive.
</p>

<p align="justify">
  <a id="package_locations"><em>Posizione pacchetti</em></a>.
  Per una lista dei pacchetti binari disponibili dei quali il progetto KDE
  &egrave; stato informato visita <a href="/info/3.5.8">Pagina informazioni KDE 3.5.8</a>.
</p>

<h4>
  Compilazione KDE 3.5.8
</h4>
<p align="justify">
  <a id="source_code"></a><em>Codice Sorgente</em>.
  Il codice sorgente completo di KDE 3.5.8 pu&ograve; essere
  <a href="http://download.kde.org/stable/3.5.8/src/">scaricato liberamente</a>.
  Le istruzioni per compilare ed installare KDE 3.5.8
  sono disponibili nella <a href="/info/3.5.8">Pagina informazioni KDE 3.5.8</a>.
</p>

<h4>
  Sostenere KDE
</h4>
<p align="justify">
KDE &egrave; un progetto di <a href="http://www.gnu.org/philosophy/free-sw.it.html">Software Libero</a> che esiste
e cresce solo grazie all'aiuto di moltissimi volontari che donano il loro tempo e capacit&agrave;. KDE &egrave;
sempre alla ricerca di nuovi volontari e collaboratori che possano aiutare nella scrittura del codice, segnalazione
e correzione errori, scrittura della documentazione, traduzione, promozione, donazione di denaro, ecc. Tutti i
contributi sono apprezzati e calorosamente accettati. Per favore leggi la pagina <a href="/community/donations/">Come sostenere
KDE</a> per ulteriori informazioni.</p>

<p align="justify">
Non vediamo l'ora di sentir parlare di te!
</p>

<h4>
  Informazioni su KDE
</h4>
<p align="justify">
  KDE &egrave; un progetto <a href="/community/awards/">vincitore di riconoscimenti</a>, indipendente, che
  <a href="/people/">coinvolge centinaia</a> di sviluppatori, traduttori, artisti ed altri professionisti che
  collaborano da tutto il mondo attraverso internet.
  Questa squadra crea e distribuisce liberamente un ambiente desktop e una suite d'ufficio stabile e integrato.
  KDE fornisce un'architettura flessibile, suddivisa in componenti e trasparente nelle reti. KDE inoltre &egrave;
  dotato di potenti strumenti di sviluppo che offrono una piattaforma di sviluppo eccezionale.</p>

<p align="justify">
  KDE fornisce un desktop stabile e maturo che include un browser avanzato
  (<a href="http://konqueror.kde.org/">Konqueror</a>), una suite di gestione informazioni personali
  (<a href="http://kontact.org/">Kontact</a>), una suite per ufficio completa
  (<a href="http://www.koffice.org/">KOffice</a>), una grande raccolta di applicazioni
  e di utilit&agrave; di rete, e inoltre un ambiente di sviluppo efficiente,
  intuitivo con un'eccellente interfaccia integrata di sviluppo:
  <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  KDE &egrave; la prova che il modello di sviluppo di software Open Source "Stile-Bazar"
 pu&ograve; portare a tecnologie di prim'ordine alla pari e spesso superiori al software
 commerciale pi&ugrave; complesso.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Marchi registrati.</em>
  KDE<sup>&#174;</sup> ed il logo K Desktop Environment<sup>&#174;</sup> sono marchi
  registrati di KDE e.V.

  Linux &egrave; un marchio registrato di Linus Torvalds.

  UNIX &egrave; un marchio registrato di The Open Group negli USA ed in altri paesi.

  Tutti gli altri marchi registrati e copyright a cui si fa riferimento in questo annuncio sono
  di propriet&agrave; dei loro rispettivi proprietari.
  </font>
</p>

<hr />
