---
aliases:
- ../announce-2.2
custom_about: true
custom_contact: true
date: '2001-08-15'
description: Thanks to the high quality of the KDE 2 development framework and the
  invaluable feedback of our users we are delivering a more polished, better integrated
  and more feature-rich desktop experience in a relative short time.
title: KDE 2.2 Released
---

FOR IMMEDIATE RELEASE

<h3 ALIGN="center">Free KDE Desktop Ready for Enterprise Deployment</h3>

KDE Ships Leading Desktop with Advanced Web Browser and Development
Environment for Linux and Other UNIXes

The <a href="http://www.kde.org/">KDE
Project</a> today announced the immediate release of KDE 2.2,
a powerful and easy-to-use Internet-enabled desktop for Linux and other UNIXes.
Consistent with KDE's rapid and disciplined development pace, the
release of KDE 2.2 features an
<a href="/announcements/changelogs/changelog2_1to2_2">impressive
catalog</a>
of speed/performance enhancements, feature additions and stability
improvements. KDE 2.2 is available in
<a href="http://i18n.kde.org/teams/distributed.html">34 languages</a>
and ships with the core KDE libraries, the core
desktop environment, and over 100 applications from the other
base KDE packages (administration, multimedia, network, PIM,
development, etc.).

"The productivity tools and interface improvements in the new KDE
desktop provide a powerful client environment," said Kent Ferson, vice
president of <a href="http://www.compaq.com/">Compaq</a>'s
<a href="http://www.tru64unix.compaq.com/">Tru64</a> UNIX Systems
Division. "We're pleased to offer
KDE 2.2 on Tru64 UNIX and to enhance our Linux-Tru64 UNIX
<a href="http://www.compaq.com/affinity/">affinity</a>
capabilities for interoperability and application mobility. Compaq
acknowledges <a href="http://www.radar.tugraz.at/people/tom.html">Dr.
Tom Leitner</a> of <a href="http://www.TUGraz.at/">Graz
University of Technology</a> and the KDE
development team for their support in delivering this affinity capability."

KDE 2.2 will be complemented later this month by the stable release
of KOffice 1.1, a comprehensive, modular, component-based
suite of office productivity applications. This
combination is the first to provide a complete
Open Source desktop and productivity environment for Linux/Unix.

KDE and all its components are available <em><STRONG>for free</STRONG></em>
under Open Source licenses from the KDE
<a href="http://download.kde.org/stable/2.2/src/">server</a>
and its <a href="/mirrors">mirrors</a> and can
also be obtained on <a href="http://www.kde.org/cdrom.html">CD-ROM</a>.

"Thanks to the high quality of the KDE 2 development framework and the
invaluable feedback of our users we are delivering a more polished,
better integrated and more feature-rich desktop experience in a relative
short time. With the pending release of a stable KOffice this
month, KDE offers a complete high-quality desktop and development platform
without the costs and restrictions associated with proprietary
desktops," said Waldo Bastian, release manager for KDE 2.2 and a
<a href="http://www.suse.com/">SuSE</a> Labs developer.

Dave Richards, the System Administrator for
<a href="http://www.largo.com/">The City of
Largo, Florida</a> who recently spearheaded the
successful roll-out of KDE on 400 thin clients serving 800 users in
City offices, explained why the City selected KDE and Linux:
"All city terminals log into one big 'desktop'
system to get the KDE desktop. Since uptime
of this server is so critical, we picked Linux.
KDE gave us an excellent presentation layer
with which to run all of the City applications
that are running on other servers (both Unix
and Windows)."

"It is important for enterprises to realize that the huge
cost savings they can obtain immediately upon conversion to KDE
do not require any sacrifice in terms of software available to their users,"
added Andreas Pour, Chairman of the KDE League. "The
capabilities of Konqueror, the expansive array of
free software available for <a href="http://apps.kde.com/">KDE/Qt</a> and
the large number of
<a href="http://www.freshmeat.net/">other Open Source projects</a> will
solve most if not all problems natively. For those applications only
available for Windows at this juncture, several
alternative products, both commercial and Open Source, exist:
running Windows terminal sessions in KDE, running Windows
applications natively under KDE/Linux, and providing remote desktop
sessions. There is no reason
companies cannot today shed themselves of at least a substantial
portion of wasteful licensing fees for their desktop users."

<STRONG><em>KDE 2: The K Desktop Environment</em></STRONG>.
<a id="Konqueror"></a><a href="http://konqueror.kde.org/">Konqueror</a>
is KDE 2's next-generation web browser, file manager and document viewer.
The standards-compliant Konqueror has a component-based architecture
which combines the features and functionality of Internet Explorer/Netscape
Communicator and Windows Explorer.
Konqueror supports the full gamut of current Internet technologies,
including JavaScript, Java, XML, HTML 4.0, CSS-1 and -2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications),
and Netscape Communicator plug-ins (for
<a href="http://macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash">Flash</a>,
<a href="http://www.real.com/player/index.html?src=010709realhome_1">RealAudio
and RealVideo</a>, and similar technologies; Konqueror also supports
some <a href="http://www.microsoft.com/com/tech/ActiveX.asp">ActiveX</a>
controls, such as
<a href="http://macromedia.com/software/shockwaveplayer/">Shockwave Player</a>.
Support for all these features is provided through KHTML, a KDE library
widget which is available to all KDE applications as either a widget
(using normal window parenting) or as a component (using the
<a href="#KParts">KParts</a> technology).

In addition, KDE offers seamless network transparency for accessing
or browsing files on Linux, NFS shares, MS Windows
SMB shares, HTTP pages, FTP directories, LDAP directories, digital
cameras and audio CDs. The modular,
plug-in nature of KDE's file architecture makes it simple to add additional
protocols (such as IPX or WebDAV) to KDE, which would
then automatically be available to all KDE applications.

Besides the exceptional compliance with Internet and file-sharing standards
<a href="#Konqueror">mentioned above</a>, KDE 2 is a leader in
compliance with the available Linux desktop standards.
KWin, KDE's new
re-engineered window manager, complies to the new
<a href="http://www.freedesktop.org/standards/wm-spec.html">Window Manager
Specification</a>.
Konqueror and KDE comply with the <a
HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/kio/DESKTOP_ENTRY_STANDARD">Desktop
Entry Standard</a>.
Konqueror uses the
<a href="http://pyxml.sourceforge.net/topics/xbel/docs/html">XBEL</a>
standard for its bookmarks.
KDE 2 largely complies with the
<a href="http://www.newplanetsoftware.com/xdnd/">X Drag-and-Drop (XDND)
protocol</a> as well as with the
<a href="http://www.rzg.mpg.de/rzg/batch/NEC/sx4a_doc/g1ae04e/chap12.html">X11R6 session management protocol (XSMP)</a>.

The KDE 2.2 release features a number of enhancements which remove
any potential obstacles to widespread, productive enterprise adoption
of the free KDE desktop:

<ul>
<li>up to 50% improvement in application startup time on GNU/Linux systems
using an experimental
<a href="http://www.research.att.com/~leonb/objprelink/">object file
pre-linking method</a>;</li>
<li>increased stability and capabilities of HTML rendering and JavaScript;</li>
<li>the addition of IMAP support (including SSL and TLS) to KDE's mail
client KMail;</li>
<li>a new plugin-based print architecture with integrated filter and
page layout capabilities (currently supports LPR,
CUPS, RLPR, external command, generic LPD and pseudo-printers such
as print-to-fax or print-to-email gateways);</li>
<li>enhanced startup speed and user feedback;</li>
<li>a number of new plugins for Konqueror, including a
<a href="http://babelfish.altavista.com/">Babelfish</a> translator,
an image gallery generator, an HTML validator and a web archiver;</li>
<li>native iCalendar support in KOrganizer, KDE's PIM tool; and</li>
<li>a new personalization wizard.</li>
</ul>

<STRONG><em>KDE 2: The K Development Environment</em></STRONG>.
KDE 2.2 offers developers a sophisticated IDE as well as a rich set
of major technological improvements over the critically acclaimed
KDE 1 series.
Chief among the technologies are
the <a href="#DCOP">Desktop COmmunication Protocol (DCOP)</a>, the
<a href="#KIO">I/O libraries (KIO)</a>, <a href="#KParts">the component
object model (KParts)</a>, an XML-based GUI class, and
a standards-compliant HTML rendering engine (KHTML).

KDevelop is a leading Linux IDE
with numerous features for rapid application
development, including a GUI dialog builder, integrated debugging, project
management, documentation and translation facilities, built-in concurrent
development support, and much more. This release includes a number of
new features, including a setup wizard, code navigation, a console, man
page support and colored text, as well as a number of new project
templates, including KControl modules, Kicker (panel) applets, kio-slaves,
Konqueror plugins and desktop themes.

<a id="KParts">KParts</a>, KDE 2's proven component object model, handles
all aspects of application embedding, such as positioning toolbars and inserting
the proper menus when the embedded component is activated or deactivated.
KParts can also interface with the KIO trader to locate available handlers for
specific mimetypes or services/protocols.
This technology is used extensively by the
<a href="http://www.koffice.org/">KOffice</a> suite and Konqueror.

<a id="KIO">KIO</a> implements application I/O in a separate
process to enable a
non-blocking GUI without the use of threads.
The class is network and protocol transparent
and hence can be used seamlessly to access HTTP, FTP, POP, IMAP,
NFS, SMB, LDAP and local files.
Moreover, its modular
and extensible design permits developers to "drop in" additional protocols,
such as WebDAV, which will then automatically be available to all KDE
applications.
KIO also implements a trader which can locate handlers
for specified mimetypes; these handlers can then be embedded within
the requesting application using the KParts technology.

<a id="DCOP">DCOP</a> is a client-to-client communications
protocol intermediated by a
server over the standard X11 ICE library.
The protocol supports both
message passing and remote procedure calls using an XML-RPC to DCOP "gateway".
Bindings for C, C++ and Python, as well as experimental Java bindings, are
available.

KDE also provides a number of language bindings. In particular, KDE
kdejava provides full Java bindings for KDE and Qt, which look and
behave identically to a C++ version, including access to the
C++ signals/slots.

#### Installing Binary Packages

<em>Binary Packages</em>.
All major Linux distributors and some Unix distributors have provided
binary packages of KDE 2.2 for recent versions of their distribution. Some
of these binary packages are available for free download under
<a href="http://download.kde.org/stable/2.2/">http://download.kde.org/stable/2.2/</a>
or under the equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution (if you cannot find a binary package for your distribution,
please read the <a href="http://dot.kde.org/986933826/">KDE Binary Package
Policy</a>).

<em>Library Requirements</em>.
The library requirements for a particular binary package vary with the
system on which the package was compiled. Please bear in mind that
some binary packages may require a newer version of Qt and other libraries
than was included with the applicable distribution (e.g., LinuxDistro 8.0
may have shipped with qt-2.2.3 but the packages below may require
qt-2.3.x). For general library requirements for KDE, please see the text at
<a href="#Source_Code-Library_Requirements">Source Code - Library
Requirements</a> above.

<a id="Package_Locations"><em>Package Locations</em></a>.
At the time of this release, pre-compiled packages are available for:

<ul>
  <li><a href="http://www.caldera.com/">Caldera Systems</a></li>
  <ul>
    <li>OpenLinux-3.1:  <a href="http://download.kde.org/stable/2.2/Caldera/OpenLinux-3.1/RPMS/">Intel i386</a></li>
  </ul>
  <li><a href="http://www.conectiva.com/">Conectiva Linux</a> (<a href="http://download.kde.org/stable/2.2/Conectiva/7.0/README">README</a>)</li>
  <ul>
    <li>7.0:  <a href="http://download.kde.org/stable/2.2/Conectiva/7.0/i386/RPMS.main/">Intel i386</a></li>
  </ul>
  <li><a href="http://www.debian.org/">Debian GNU/Linux</a> (package "kde"):  <a href="ftp://ftp.debian.org/">ftp.debian.org</a>:  sid (devel) (see also <a href="http://http.us.debian.org/debian/pool/main/k/">here</a>)</li>
  <li><a href="http://www.FreeBSD.org/">FreeBSD</a> (<a href="http://download.kde.org/stable/2.2/FreeBSD/README">README</a>)</li>
  <ul>
    <li>Unpecified version:  <a href="http://download.kde.org/stable/2.2/FreeBSD/">Intel i386</a></li>
  </ul>
  <li><a href="http://www.ibm.com/servers/aix/products/aixos/">IBM AIX</a> (<em>Note</em>:  These are expected to be available in the coming week)</li>
  <ul>
    <li>4.3.3.0 or higher:  <a href="http://www-1.ibm.com/servers/aix/products/aixos/linux/download.html">PowerPC</a></li>
  </ul>
  <li><a href="http://www.linux-mandrake.com/en/">Linux-Mandrake</a> (<a href="http://download.kde.org/stable/2.2/Mandrake/README">README</a>)</li>
  <ul>
    <li>8.0:  <a href="http://download.kde.org/stable/2.2/Mandrake/8.0/i586/">Intel i586</a> (see also the <a href="http://download.kde.org/stable/2.2/Mandrake/8.0/noarch/">noarch</a> directory) and <a href="http://download.kde.org/stable/2.2/Mandrake/ppc/RPMS/">PowerPC</a> (see also the <a href="http://download.kde.org/stable/2.2/Mandrake/ppc/noarch/">noarch</a> directory)</li>
    <li>7.2:  <a href="http://download.kde.org/stable/2.2/Mandrake/7.2/i586/">Intel i586</a> (see also the <a href="http://download.kde.org/stable/2.2/Mandrake/7.2/noarch/">noarch</a> directory)</li>
  </ul>
  <li><a href="http://www.redhat.com/">RedHat Linux</a> (<a href="http://download.kde.org/stable/2.2/RedHat/README">README</a>):
  <ul>
    <li>7.x:  <a href="http://download.kde.org/stable/2.2/RedHat/7.x/i386/">Intel i386</a> (see also the <a href="http://download.kde.org/stable/2.2/RedHat/7.x/non-kde/i386/">add-ons</a> and <a href="http://download.kde.org/stable/2.2/RedHat/7.x/noarch/">noarch</a> directories), <a href="http://download.kde.org/stable/2.2/RedHat/7.x/ia64/">HP/Intel IA-64</a> (see also the <a href="http://download.kde.org/stable/2.2/RedHat/7.x/non-kde/ia64/">add-ons</a> and <a href="http://download.kde.org/stable/2.2/RedHat/7.x/noarch/">noarch</a> directories), <a href="http://download.kde.org/stable/2.2/RedHat/7.x/alpha/">Alpha</a> (see also the <a href="http://download.kde.org/stable/2.2/RedHat/7.x/non-kde/alpha/">add-ons</a> and <a href="http://download.kde.org/stable/2.2/RedHat/7.x/noarch/">noarch</a> directories) and <a href="http://download.kde.org/stable/2.2/RedHat/7.x/s390/">IBM S390</a> (see also the <a href="http://download.kde.org/stable/2.2/RedHat/7.x/noarch/">noarch</a> directory)</li>
  </ul>
  <li><a href="http://www.suse.com/">SuSE Linux</a> (<a href="http://download.kde.org/stable/2.2/SuSE/README">README</a>):
  <ul>
    <li>7.2:  <a href="http://download.kde.org/stable/2.2/SuSE/i386/7.2/">Intel i386</a> and <a href="http://download.kde.org/stable/2.2/SuSE/ia64/7.2/">HP/Intel IA-64</a> (see also the <a href="http://download.kde.org/stable/2.2/SuSE/noarch/">noarch</a> directory)</li>
    <li>7.1:  <a href="http://download.kde.org/stable/2.2/SuSE/i386/7.1/">Intel i386</a>, <a href="http://download.kde.org/stable/2.2/SuSE/ppc/7.1/">PowerPC</a>, <a href="http://download.kde.org/stable/2.2/SuSE/sparc/7.1/">Sun Sparc</a> and <a href="http://download.kde.org/stable/2.2/SuSE/axp/7.1/">Alpha</a> (see also the <a href="http://download.kde.org/stable/2.2/SuSE/noarch/">noarch</a> directory)</li>
    <li>7.0:  <a href="http://download.kde.org/stable/2.2/SuSE/i386/7.0/">Intel i386</a>, <a href="http://download.kde.org/stable/2.2/SuSE/ppc/7.0/">PowerPC</a> and <a href="http://download.kde.org/stable/2.2/SuSE/s390/7.0/">IBM S390</a> (see also the <a href="http://download.kde.org/stable/2.2/SuSE/noarch/">noarch</a> directory)</li>
    <li>6.4:  <a href="http://download.kde.org/stable/2.2/SuSE/i386/6.4/">Intel i386</a> (see also the <a href="http://download.kde.org/stable/2.2/SuSE/noarch/">noarch</a> directory)</li>
  </ul>
  <li><a href="http://www.tru64unix.compaq.com/">Tru64 Systems</a> (<a href="http://download.kde.org/stable/2.2/Tru64/README.Tru64">README</a>)</li>
  <ul>
    <li>Tru64 4.0d, e, f and g and 5.x:  <a href="http://download.kde.org/stable/2.2/Tru64/">Alpha</a></li>
  </ul>
</ul>

Please check the servers periodically for pre-compiled packages for other
distributions. More binary packages will become available over the
coming days and weeks.

#### Downloading and Compiling KDE 2.2

<a id="Source_Code-Library_Requirements"></a><em>Library
Requirements</em>.
KDE 2.2 requires qt-2.2.4, which is available in source code from Trolltech as
<a href="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.4.tar.gz">qt-x11-2.2.4.tar.gz</a>. In addition, for SSL support, KDE 2.2 requires <a href="http://www.openssl.org/">OpenSSL</a> &gt;= 0.9.6x; versions 0.9.5x are not supported. For
Java support, KDE 2.2 requires a JVM &gt;= 1.3(?). For Netscape plugin
support, KDE requires a recent version of
<a href="http://www.lesstif.org/">Lesstif</a> or Motif. Searching
local documentation requires <a href="http://www.htdig.org/">htdig</a>.
Other special features, such as drag'n'drop audio CD ripping, require
other packages.

<em>Compiler Requirements</em>.
Please note that some components of
KDE 2.2 will not compile with older versions of
<a href="http://gcc.gnu.org/">gcc/egcs</a>, such as egcs-1.1.2 or
gcc-2.7.2. At a minimum gcc-2.95-\* is required. In addition, some
components of KDE 2.2 (such as the multimedia backbone of KDE,
<a href="http://www.arts-project.org/">aRts</a>) will not compile with
<a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc 3.0</a> (the
problems are being corrected by the KDE and GCC teams).

<a id="Source_Code"></a><em>Source Code/RPMs</em>.
The complete source code for KDE 2.2 is available for free download at
<a href="http://download.kde.org/stable/2.2/src/">http://download.kde.org/stable/2.2/src/</a>
or in the equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>.
Additionally, source rpms are available for the following distributions:

<ul>
  <li><a href="http://download.kde.org/stable/2.2/Conectiva/7.0/SRPMS/">Conectiva Linux</a></li>
  <li><a href="http://download.kde.org/stable/2.2/Caldera/OpenLinux-3.1/SRPMS/">Caldera Systems</a></li>
  <li><a href="http://download.kde.org/stable/2.2/Mandrake/SRPMS/">Linux-Mandrake</a></li>
  <li><a href="http://download.kde.org/stable/2.2/RedHat/7.x/SRPMS/">RedHat Linux</a> (see also the <a href="http://download.kde.org/stable/2.2/RedHat/7.x/non-kde/SRPMS/">add-ons</a> directory)</li>
  <li><a href="http://download.kde.org/stable/2.2/SuSE/SRPMS/">SuSE Linux</a></li>
</ul>

<em>Further Information</em>. For further
instructions on compiling and installing KDE 2.2, please consult
the <a href="http://www.kde.org/install-source.html">installation
instructions</a> and, if you should encounter problems, the
<a href="http://www.kde.org/compilationfaq.html">compilation FAQ</a>. For
problems with source rpms, please contact the person listed in the .spec
file.

#### About KDE

KDE is an independent, collaborative project by hundreds of developers
worldwide working over the Internet to create a sophisticated,
customizable and stable desktop environment employing a component-based,
network-transparent architecture. KDE is working proof of the power of
the Open Source "Bazaar-style" software development model to create
first-rate technologies on par with and superior to even the most complex
commercial software.

Please visit the KDE family of web sites for the
<a href="http://www.kde.org/faq.html">KDE FAQ</a>,
<a href="http://www.kde.org/screenshots/kde2shots.html">screenshots</a>,
<a href="http://www.koffice.org/">KOffice information</a> and
<a href="http://developer.kde.org/documentation/kde2arch.html">developer
information</a>.
Much more <a href="http://www.kde.org/whatiskde/">information</a>
about KDE is available from KDE's
<a href="http://www.kde.org/family.html">family of web sites</a>.

#### Corporate KDE Sponsors

Besides the valuable and excellent efforts by the
<a href="http://www.kde.org/gallery/index.html">KDE developers</a>
themselves, significant support for KDE development has been provided by
<a href="http://www.mandrakesoft.com/">MandrakeSoft</a> and
<a href="http://www.suse.com/">SuSE</a>. Thanks!

<hr /><FONT SIZE=2>
<em>Trademarks Notices.</em>
KDE, K Desktop Environment and KOffice are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix and Motif are registered trademarks of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
MS Windows, Internet Explorer, Windows Explorer and ActiveX are trademarks
or registered trademarks of Microsoft Corporation.
Shockwave is a trademark or registered trademark of Macromedia, Inc. in the United States and/or other countries.
Netscape and Netscape Communicator are trademarks or registered trademarks of Netscape Communications Corporation in the United States and other countries and JavaScript is a trademark of Netscape Communications Corporation.
Java is a trademark of Sun Microsystems, Inc.
Flash is a trademark or registered trademark of Macromedia, Inc. in the United States and/or other countries.
RealAudio and RealVideo are trademarks or registered trademarks of RealNetworks, Inc.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
<br></font>

<hr /><table border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
  Eunice Kim<br>
  The Terpin Group<br>

[ekim@terpin.com](mailto:ekim@terpin.com)<br>
(1) 650 344 4944 ext. 105<br>&nbsp;<br>
Kurt Granroth <br>

[granroth@kde.org](mailto:granroth@kde.org)
<br>
(1) 480 732 1752<br>&nbsp;<br>
Andreas Pour<br>

[pour@kde.org](mailto:pour@kde.org)<br>
(1) 718-456-1165

  </td>
</tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<br>
(44) 1225 837409

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Ralf Nolden<br>
    
  [nolden@kde.org](mailto:nolden@kde.org) <br>
  (49) 2421 502758
  </td>
</tr>
</table>