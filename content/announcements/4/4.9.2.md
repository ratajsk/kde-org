---
aliases:
- ../announce-4.9.2
date: '2012-10-02'
description: KDE Ships 4.9.2 Workspaces, Applications and Platform.
title: KDE Ships October Updates to Plasma Workspaces, Applications and Platform
---

<p align="justify">
Today KDE released updates for its Workspaces, Applications, and Development Platform.
These updates are the second in a series of monthly stabilization updates to the 4.9 series. 4.9.2 updates bring many bugfixes and translation updates on top of the latest edition in the 4.9 series and are recommended updates for everyone running 4.9.1 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come.
<br /><br />
Significant bugfixes include improvements to the Kontact Suite, bugfixes in Dolphin, Plasma and many more corrections and performance improvements all over the place. KDE's development platform has received a number of updates which affect multiple applications.
The changes are listed on <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.9.2&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=">Bugzilla</a>. For a detailed list of changes that went into 4.9.2, you can browse the Subversion and Git logs. 4.9.2 also ships a more complete set of translations for many of the 55+ supported languages.
To  download source code or packages to install go to the <a href="http://www.kde.org/info/4.9.2">4.9.2 Info Page</a>. If you would like to find out more about the KDE Workspaces and Applications 4.9, please refer to the <a href="/announcements/4.9/">4.9 release notes</a> and its earlier versions.
</p>

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-dolphin_.png">
	<img src="/announcements/4/4.9.0/kde49-dolphin_thumb.png" class="img-fluid" alt="KDE's Dolphin File Manager">
	</a> <br/>
	<em>KDE's Dolphin File Manager</em>
</div>
<br/>

<p align="justify">
The KDE Software Compilation, including all its libraries and its applications, is available for free
under Open Source licenses. KDE's software can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.9.2/">http://download.kde.org</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing 4.9.2 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.9.2
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"></a><em>Package Locations</em>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.9.2#binary">4.9.2 Info
Page</a>.
</p>

<h4>
  Compiling 4.9.2
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.9.2 may be <a
href="http://download.kde.org/stable/4.9.2/src/">freely downloaded</a>.
Instructions on compiling and installing 4.9.2
  are available from the <a href="/info/4.9.2">4.9.2 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or 
become a KDE e.V. supporting member through our new 
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>


