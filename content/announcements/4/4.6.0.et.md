---
aliases:
- ../4.6
- ../4.6.et
date: '2011-01-26'
custom_about: true
custom_contact: true
title: KDE uued töötsoonid, rakendused ja platvorm annavad kontrolli kasutaja kätte
---

</p>
<p>
KDE-l on rõõm teatada uusimatest väljalasetest, mis uuendavad oluliselt KDE Plasma töötsoone, KDE rakendusi ja KDE platvormi. Kõik need järjekorranumbrit 4.6 kandvad väljalasked pakuvad hulganisti uusi omadusi ja võimalusi kõigis kolmes KDE tooteharus. Mõningad olulisemad neist on järgmised:
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w09.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w09.png" class="img-fluid" alt="KDE Plasma Desktop, Gwenview and KRunner in 4.6">
	</a> <br/>
	<em>KDE Plasma töölaud, Gwenview ja KRunner 4.6-s</em>
</div>
<br/>

<h3>
<a href="./plasma">
Plasma töötsoonid annavad juhtohjad kasutaja kätte
</a>
</h3>

<p>
<a href="./plasma">
<img src="/announcements/4/4.6.0/images/plasma.png" class="app-icon float-left m-3" alt="KDE Plasma töötsoonid 4.6.0" />
</a>

<b>KDE Plasma töötsoonidele</b> lisandus uus tegevuste süsteem, mille abil on palju hõlpsam seostada rakendusi konkreetse tegevusega, näiteks tööülesannete või koduste tegemistega. Uuendatud toitehaldus pakub uusi võimalusi, aga ka senisest lihtsamat seadistamist. Plasma töötsooni aknahaldur KWin sai skriptide kasutamise võimaluse ning töötsoonid tervikuna mitmeid visuaalseid parandusi. Mobiilsetele seadmetele kohandatud <b>Plasma Netbook</b> on senisest tunduvalt kiirem ning seda on lihtsam kasutada ka puuteekraaniga seadmete puhul. Täpsemalt kõneleb kõigest <a href="./plasma">KDE Plasma töötsoonide 4.6 teadaanne</a>.

</p>

<h3>
<a href="./applications">
KDE Dolphinile lisandus täpsustav otsing
</a>
</h3>

<p>

<a href="./applications">
<img src="/announcements/4/4.6.0/images/applications.png" class="app-icon float-left m-3" alt="KDE rakendused 4.6.0"/>
</a>
</a>
Paljude <b>KDE rakenduste</b> meeskonnad lasksid samuti välja uued versioonid. Eriti tasub tähele panna KDE virtuaalse gloobuse Marble tunduvalt parandatud teekondade väljaarvutamise ja näitamise võimalusi ning KDE failihalduri Dolphin täiustatud filtreerimist ja otsimist metaandmete abil, niinimetatud fassettsirvimist. KDE mängud said hulganisti parandusi ning pildinäitaja Gwenview ja ekraanipiltide tegemise rakendus KSnapshot võimaluse saata pilte aega viitmata mitmesse levinud sotsiaalvõrgustikku. Täpsemalt kõneleb kõigest <a href="./applications">KDE rakenduste 4.6 teadaanne</a>.<br /><br />
</p>

<h3>
<a href="./platform">
Suund mobiilsusele kahandab KDE platvormi kaalu
</a>
</h3>

<p>

<a href="./platform">
<img src="/announcements/4/4.6.0/images/platform.png" class="app-icon float-left m-3" alt="KDE arendusplatvorm 4.6.0"/>
</a>

<b>KDE platvorm</b>, millele tuginevad Plasma töötsoonid ja KDE rakendused, sai uusi omadusi, millest võidavad kõik KDE rakendused. Uus suund mobiilsusele muudab hõlpsamaks mobiilsetele seadmetele mõeldud rakenduste arendamise. Plasma raamistik toetab nüüd töölauavidinate kirjutamist Qt deklaratiivses programmeerimiskeeles QML ning pakub uusi JavaScripti liideseid andmete kasutamiseks. KDE rakendustele metaandmete ja otsinguvõimalusi pakkuv Nepomuki tehnoloogia sai graafilise liidese, mille abil andmeid varundada ja taastada. Iganenud HAL-i asemel on nüüd võimalik kasutada UPowerit, UDevi ja UDisksi. Paranes Bluetoothi toetus. Oxygeni vidina- ja stiilikomplekt sai mitmeid täiustusi ning uus GTK rakendustele mõeldud Oxygeni teema võimaldab neid sujuvalt ühendada Plasma töötsoonidega, nii et nad näevad välja nagu KDE rakendused. Täpsemalt kõneleb kõigest <a href="./platform">KDE platvormi 4.6 teadaanne</a>.

</p>

<h4>
    Levitage sõna ja jälgige toimuvat: silt "KDE" on oluline
</h4>
<p align="justify">
KDE julgustab kõiki <strong>levitama sõna</strong> sotsiaalvõrgustikes.
Edastage lugusid uudistesaitidele, kasutage selliseid kanaleid, nagu delicious, digg, reddit, twitter,
identi.ca. Laadige ekraanipilte üles sellistesse teenustesse, nagu Facebook, Flickr,
ipernity ja Picasa, ning postitage neid sobivatesse gruppidesse. Looge ekraanivideoid ja laadige
need YouTube'i, Blip.tv-sse, Vimeosse või mujale. Ärge unustage lisada üleslaaditud materjalile
<em>silti <strong>kde</strong></em>, et kõik võiksid vajaliku materjali hõlpsamini üles leida
ja KDE meeskond saaks koostada aruandeid KDE SC <?php echo $release;?> väljalasketeate
 kajastamise kohta. <strong>Aidake meil sõna levitada ja andke sellesse ka oma panus!</strong></p>

<p align="justify">
Kõike seda, mis toimub seoses 4.6 väljalaskega sotsiaalvõrgustikes, võite jälgida
<a href="http://buzz.kde.org"><strong>KDE kogukonna otsevoos</strong></a>. See sait koondab reaalajas kõik, mis toimub identi.ca-s, twitteris, youtube'is, flickris, picasawebis, ajaveebides ja paljudes teistes
sotsiaalvõrgustikes. Otsevoo leiab aadressilt <strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/news/technology/kde_software_compilation_4_6_0_released"><img src="/announcements/buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/linux/comments/f9d9t/kde_software_compilation_460_released/"><img src="/announcements/buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde46"><img src="/announcements/buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde46"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde46"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde46"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="/announcements/buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde46"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>

<h4>KDE toetamine</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.6.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Astuge mängu"/> </a>

<p align="justify"> KDE e.V. uus <a
href="http://jointhegame.kde.org/">toetajaliikme programm</a> on
nüüd avatud. 25 euro eest kvartalis võite omalt poolt kindlustada,
et KDE rahvusvaheline kogukond kasvab ja areneb ning suudab valmistada
maailmaklassiga vaba tarkvara.</p>

<p>&nbsp;</p>
