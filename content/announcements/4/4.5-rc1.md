---
aliases:
- ../announce-4.5-rc1
date: '2010-06-27'
description: KDE Ships First Release Candidate of KDE SC 4.5 Series
title: KDE Software Compilation 4.5 RC1 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
  KDE Software Compilation 4.5 RC1 Released: Codename Coppu
</h3>

<p align="justify">
  <strong>
KDE Community Ships First Release Candidate of the 4.5 Free Desktop, Applications and
Development Platform
</strong>
</p>

<p align="justify">
 Today, KDE delivers the first release candidate of the upcoming KDE Software Compilation 4.5. The final version will be available in August 2010 and this RC is intended for testers and early adopters who can help by finding and reporting bugs. It will also interest those who want an early look at what is coming to their desktops and netbooks this summer.</p>

<p align="justify">The KDE Community continues to fulfill the promise of delivering state of the art architecture with its Development Platform. The Applications in Software Compilation 4.5 take advantage of this, introducing improvements across the board in terms of innovative new features, greater ease of use, accessibility, good looks, better networking and further internationalization.</p>

<p align="justify">With the start of the release candidate cycle, the development community focusses on bug fixing and polishing in preparation for the final release while the translators finish the last texts. Since beta 2, 1233 bugs have been reported and 1165 have been closed, a sign of the hard work by the developers (and the bug triaging team) to create a solid final release.</p>

<div class="text-center">
	<a href="/announcements/4/4.5-rc1/announce-4.5-rc1.png">
	<img src="/announcements/4/4.5-rc1/announce-4.5-rc1_thumb.png" class="img-fluid" alt="The KDE Plasma Netbook in 4.5 RC1">
	</a> <br/>
	<em>The KDE Plasma Netbook in 4.5 RC1</em>
</div>
<br/>

</p>
<p>
KDE SC 4.5 sports many improvements, among which are:
<ul>
    <li>
    A <strong>reworked notification</strong> area. Thanks to the new, D-Bus-based protocol that replaces the old "system tray", a <em>uniform look and consistent interaction</em> scheme can now be guaranteed across applications and toolkits.
    </li>
    <li>
    <strong>KWin-Tiling</strong> makes it possible to automatically place windows next to each other, employing the <em>window management paradigm</em> also found in window managers such as Ion. Advanced graphical effects, such as blurring the background of translucent windows make for a more pleasurable and usable experience.
    </li>
    <li>
    Users that prefer <strong>WebKit</strong> above the KHTML rendering engine currently used in Konqueror, KDE's web browser now can install the WebKit component and switch Konqueror to use WebKit as <em>rendering engine for web sites</em>.  The WebKit component for Konqueror is available from KDE's Extragear repository, is based on the popular KPart component technology and fully integrates with password storage, content-blocking and other features users already know and love in Konqueror.
    </li>
    <li>
    A special focus of this release cycle is the <strong>stability</strong> of the software delivered with KDE SC 4.5. While there are many exciting new features, developers have spent considerable amounts of time finishing off features and polishing those that haven't come to full bloom yet.
    </li>
</ul>
</p>
<p>
Initially planned for KDE SC 4.5.0, the KDE PIM team have decided to delay the release of the Akonadi-based KMail for one month. The new version of KMail will be delivered as part of one of KDE's monthly bugfix updates. In the meantime, the stable version of KMail from KDE SC 4.4 will be maintained. Akonadi will centralize syncing and caching of PIM data, deliver wider support for groupware servers and makes handling PIM data, such as contacts, calendaring and email more efficient by sharing it across applications.

</p>
<p>
To find out more about the KDE Plasma desktop and applications, please also refer to the
<a href="/announcements/4.4/">4.4.0</a>,
<a href="/announcements/4.3/">4.3.0</a>,
<a href="/announcements/4.2/">4.2.0</a>,
<a href="/announcements/4.1/">4.1.0</a> and
<a href="/announcements/4.0/">4.0.0</a> release
notes.
</p>

<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/unstable/4.4.90/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing KDE SC 4.5 RC1 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE SC 4.5 RC1
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.4.90/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.4.90">KDE SC 4.5 RC1 Info
Page</a>.
</p>

<h4>
  Compiling KDE SC 4.5 RC1
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE SC 4.5 RC1 may be <a
href="http://download.kde.org/stable/4.4.90/src/">freely downloaded</a>.
Instructions on compiling and installing KDE SC 4.5 RC1
  are available from the <a href="/info/4.4.90#binary">KDE SC 4.5 RC1 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>


