---
aliases:
- ../announce-4.11.1
date: '2013-09-03'
description: KDE Ships Plasma Workspaces, Applications and Platform 4.11.1.
title: KDE Ships September Updates to Plasma Workspaces, Applications and Platform
---

September 3, 2013. Today KDE released updates for its Workspaces, Applications and Development Platform. These updates are the first in a series of monthly stabilization updates to the 4.11 series. As was announced on the release, the workspaces will continue to receive updates for the next two years. This release only contains bugfixes and translation updates and will be a safe and pleasant update for everyone.

More than 70 recorded bugfixes include improvements to the Window Manager KWin, the file manager Dolphin, and others. Users can expect Plasma Desktop to start up faster, Dolphin to scroll smoother, and various applications and tools will use less memory. Improvements include the return of drag-and-drop from taskbar to pager, highlighting and color fixes in Kate and MANY little bugs squashed in the Kmahjongg game. There are many stability fixes and the usual additions of translations.

{{% i18n_var "A more complete <a href='%[1]s'>list</a> of changes can be found in KDE's issue tracker. For a detailed list of changes that went into 4.11.1, you can also browse the Git logs." "https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2011-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.1&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=" %}}

To download source code or packages to install go to the <a href='http://www.kde.org/info/4.11.1.php'>4.11.1 Info Page</a>. If you want to find out more about the 4.11 versions of KDE Workspaces, Applications and Development Platform, please refer to the <a href='http://www.kde.org/announcements/4.11/'>4.11 release notes</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption="The new send-later work flow in Kontact" width="600px">}}

KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a href='http://download.kde.org/stable/4.11.1/'>http://download.kde.org</a> or from any of the <a href='http://www.kde.org/download/distributions.php'>major GNU/Linux and UNIX systems</a> shipping today.
