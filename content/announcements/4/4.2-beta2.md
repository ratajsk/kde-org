---
aliases:
- ../announce-4.2-beta2
date: '2008-12-16'
description: KDE Community Ships Second Beta Version for Next-Gen Leading Free Software
  Desktop.
title: KDE 4.2 Beta 2 Release Announcement
---

<p>FOR IMMEDIATE PUBLICATION</p>

<h3 align="center">
  KDE 4.2 Beta 2 Release for Further Testing
</h3>

<p align="justify">
  <strong>
KDE Commmunity Ships Second Beta Release of KDE 4.2, Inviting Community to Test and Report Bugs.
</strong>
</p>

<p align="justify">
The <a href="http://www.kde.org/">KDE
Community</a> today announced the immediate availability of <em>"Canaria"</em>,
(a.k.a KDE 4.2 Beta 2), the second testing release of the new KDE 4.2 desktop.
<em>Canaria</em> is aimed at testers and reviewers. It should provide a solid
ground to <a href="http://bugs.kde.org">report bugs</a> that need to be tackled
before KDE 4.2.0 is released. Reviewers can use this beta to get a first look at
the upcoming KDE 4.2 desktop which provides significant improvements all over
the desktop and applications.<br />
Since the first beta, which was released less than 4 weeks ago, 1665 new bugs
have been opened, and 2243 bugs have been closed. Since the release of KDE
4.1.0, more than ten thousand bugs were closed, showing a massive focus on
stability in the upcoming KDE 4.2.0 which will be released in January 2009, 6
months after KDE 4.1. KDE 4.2.0 will be followed up by a series of monthly
service updates and followed up by KDE 4.3.0 in summer 2009.
</p>

<div class="text-center">
	<a href="/announcements/4/4.2-beta2/panel.png">
	<img src="/announcements/4/4.2-beta2/panel_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The Panel in KDE 4.2 Beta 2</em>
</div>
<br/>

<h2>Significant refinements of Plasma and KWin, the KDE workspace</h2>
<p>
    <ul>
        <li>
            The KDE 4.2 series will offer considerable improvements in stability, 
            feature-completeness and performance over its predecessors in the KDE4 series.
        </li>
        <li>
            The Plasma desktop shell has gained many feature that users were still missing 
            in earlier KDE4 revisions.
        </li>
        <li>
            Applications shipped with KDE 4.2 Beta 2 have many features added and bugs fixed
        </li>
        <li>
            The KDE development platform has seen significant improvements on non-Linux 
            platforms such as BSD, Windows and Mac OSX, making more applications available
            to users of those Operating Systems. 
        </li>
        <li>
            Writing KDE applications and add-ons in
            scripting languages such as Python and Ruby is easier than ever.
        </li>
    </ul>
</p>

<p>
For the 4.2 release, the KDE team has fixed literally thousands of bugs and has
implemented dozens of features that were missing until now in KDE 4.2. 
This beta release gives you the opportunity to assist in ironing
out the remaining wrinkles. The KDE release team has put together a list with
the most significant improvements in 4.2 Beta 2:

</p>

<h2>Many improvements</h2>
<p>
<ul>
  <li>

    <strong>Compositing desktop effects</strong> are enabled where hardware and

drivers support it, with a basic default setup. Automatic checks confirm that
compositing works before enabling it on the workspace.

  </li>
  <li>
    <strong>New desktop effects</strong> have been added such as the Magic Lamp,
Minimize effect, Cube and Sphere desktop switchers. Others, such as the desktop
grid, have been improved. All effects have been polished and and feel natural
due to the use of motion dynamics. The user interface for choosing effects has
been reworked for easy selection of the most commonly used effects.
  </li>
  <li>
    <strong>Central elements</strong> of the desktop experience have seen
significant improvements to give a usable and coherent experience.  These
include grouping and multiple row layout in the task bar, icon hiding in the
system tray, notifications and job tracking by Plasma, the ability to have icons
on the desktop again by using a Folder View as the desktop background.  Restored
features and minor tweaks round out the work, such as the return of panel
autohiding to maximise your productive screen space, icons now remain where they
are placed in the Folder View, the location of new applets is improved, and
window previews and tooltips are back in the panel and Task Bar.
  </li>
  <li>
    <strong>New Plasma applets</strong> include applets for leaving messages on
a locked screen, previewing files, switching desktop Activity, monitoring news
feeds, and utilities like the pastebin applet, the calendar, timer, special
character selector, a quicklaunch applet, a system monitor, among many others.
  </li>
  <li>
    KRunner, the <strong>"Run command..." dialog</strong> has extended
functionality through several new plugins, including spellchecking, konqueror
browser history, power management control through PowerDevil, KDE Places, Recent
Documents, and the ability to start specific sessions of the Kate editor,
Konqueror and Konsole. The converter plugin now also supports quickly converting
between units of speed, mass and distances.
  </li>
  <li>
    The Plasma workspace can now load <strong>Google Gadgets</strong>. Plasma
<strong>applets</strong> can be written in <strong>Ruby</strong> and
<strong>Python</strong>. Support for applets written in JavaScript and Mac OS
dashboard widgets has been further improved.
  </li>
  <li>
    Wallpapers are now provided plugins, so developers can easily write
<strong>custom wallpaper systems</strong> in KDE 4.2. Available wallpaper
plugins in KDE 4.2 will be slideshows and of course regular static images and solid colors.
  </li>
  <li>
    <strong>Theming improvements</strong> in the Task Bar, Application Launcher,
System Tray and most other Plasma components streamline the look and feel and
increase consistency.  A new System Settings module, Desktop Theme Details,
gives the user control over each element of various Plasma themes.
  </li>
  <li>
    <strong>Multi-screen</strong> support has been improved through the Kephal
library, fixing many bugs when running KDE on more than one monitor.
  </li>
</ul>
</p>

<div class="text-center">
	<a href="/announcements/4/4.2-beta2/desktop.png">
	<img src="/announcements/4/4.2-beta2/desktop_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Desktop as Folder View (traditional desktop)</em>
</div>
<br/>

<h2>New and Improved Applications</h2>
<p>
All applications have seen bugfixes, feature additions, user interface
improvements and generally received more polish during the last 4 months of
development. The following list gives some ideas about the areas of improvement.
</p>
<p>
<ul>
  <li>
    Dolphin now supports previews of files in tooltips and has gained a slider
to zoom in and out on file item views. It can now also show the full path in the
breadcrumb bar.
  </li>
  <li>
    Konqueror offers <strong>increased loading speed</strong> by prefetching
domain name data in KHTML. A find-as-you-type bar improves navigation in
webpages. It also gained the option to use your bookmarks as the start page by means
of the new Bookmarks KIO slave.
  </li>
  <li>
    KMail has a powerful and attractive <strong>message header list</strong>,
and reworked attachment view.
  </li>
  <li>
    The KWrite and Kate text editors can now operate in Vi input mode,
accomodating those used to the traditional UNIX editor.
  </li>
  <li>
    PowerDevil, the new KDE4 power management infrastructure brings a modern,
integrated tool for controlling various aspects of mobile devices.
  </li>
  <li>
    Ark, the archiving tool has improved UI, gained support for
password-protected archives and is accessible via a <strong>context
menu</strong> from the file managers now.
  </li>
  <li>
    A <strong>new printing configuration system</strong> brings back a number of
features users have been missing in KDE 4.0 and 4.1. The components
"printer-applet" and "system-config-printer" are shipped with the kdeadmin and
kdeutils module.
  </li>
  <li>
    KRDC, the remote desktop client improves support for Microsoft's
<strong>Active Directory</strong> through LDAP.
  </li>
  <li>
    Kontact has gained a new <strong>planner summary</strong> and support for
drag and drop in the free/busy view.
  </li>
  <li>
    KSnapshot now stores the window title as meta data when saving screenshots, making it
easier to index them using search engines.
  </li>
  <li>
    The secure file transfer protocols SFTP and FISH are now also supported by
<strong>KDE on the Windows platform</strong>.
  </li>
  <li>
    Killbots is a new game shipped with the kdegames module. Other games have
improved user interaction and added themes and levels.
  </li>
  <li>
    Educational apps such as KAlgebra, KStars, KTurtle and Parley have seen
major improvements in UI and feature sets.
  </li>
  <li>
    Okteta, the hex editor has significantly improved various aspects of its
user interface.
  </li>
</ul>
This list of new features in KDE 4.2 is not comprehensive yet. Please refer to
the <a href="http://techbase.kde.org/Schedules/KDE4/4.2_Changelog">changelog</a>
 for more details.
</p>

<div class="text-center">
	<a href="/announcements/4/4.2-beta2/improved-desktop-grid.png">
	<img src="/announcements/4/4.2-beta2/improved-desktop-grid_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Improved Desktop Grid Effect</em>
</div>
<br/>

<p>
To find out more about the KDE 4 desktop and applications, please also refer to
the
<a href="/announcements/4.1/">KDE 4.1.0</a> and
<a href="/announcements/4.0/">KDE 4.0.0</a> release
notes. KDE 4.2 Beta 2 is not recommended for everyday use.
This beta will be followed up by a release
candidate on January 6th and the final release of KDE 4.2.0 on January 27th, 6
months after the release of KDE 4.1.0.
<p />

<h4>
  Installing KDE 4.2 Beta 2 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.2 Beta 2,
 and in other cases community volunteers have done so.
  Some of these binary packages are available for free download via the <a
href="/info/4.1.85#binary">KDE 4.2 Beta 2 Info Page</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.1.85">KDE 4.2 Beta 2 Info
Page</a>.
</p>

<h4>
  Compiling KDE 4.2 Beta 2
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE 4.1.85 may be <a
href="/info/4.1.85#desktop">freely downloaded</a>.
Instructions on compiling and installing KDE 4.1.85
  are available from the <a href="/info/4.1.85">KDE 4.2 Beta 2 Info
Page</a>.
</p>


