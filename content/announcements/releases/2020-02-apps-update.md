---
layout: page
publishDate: 2020-02-06 12:00:00
summary: What Happened in KDE's Applications This Month
title: KDE's February 2020 Apps Update
type: announcement
---

This month sees a plethora of bugfix releases. While we all might like new features, we know that people love when mistakes and crashes get tidied up and solved too!

# New releases

## KDevelop 5.5

The big release this month comes from [KDevelop 5.5](https://www.kdevelop.org/news/kdevelop-550-released), the IDE that makes writing programs in C++, Python and PHP easier. We have fixed a bunch of bugs and tidied up a lot of KDevelop's code. We added Lambda init captures in C++, configurable predefined checkset selections are now in for Clazy-tidy and Clang, and look-ahead completion now tries harder. PHP gets PHP 7.4's typed properties and support was added for array of type and class constant visibility. In Python, support has now been added for Python 3.8.

KDevelop is available from your Linux distro or as an AppImage.

{{< img class="text-center" src="KDevelop_5.5.0.png" link="https://www.kdevelop.org/news/kdevelop-550-released" caption="KDevelop" >}}

## Zanshin 0.5.71

[Zanshin](https://zanshin.kde.org/) is our TODO list tracker which integrates with Kontact. Recent releases of Kontact had broken Zanshin and users were left not knowing what tasks to carry out next. But this month, a release was made to [get it working again](https://jriddell.org/2020/01/14/zanshin-0-5-71/). We can now all easily find the tasks that we need to get on with!

{{< img class="text-center" src="zanshin.png" link="https://jriddell.org/2020/01/14/zanshin-0-5-71/" caption="Zanshin" >}}

## Latte-dock 0.9.8

https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html

{{< img class="text-center" src="latte.png" link="https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html" caption="Latte Dock" >}}

## RKWard 0.7.1

https://rkward.kde.org/News.html

{{< img class="text-center" src="rkward.png" link="https://rkward.kde.org/News.html" caption="RKWard" >}}

## Okteta 0.26.3

[Okteta](https://kde.org/applications/utilities/org.kde.okteta), KDE's Hex editor, had a bugfix release and includes a new feature: a CRC-64 algorithm for the checksum tool. Okteta also updates the code for new Qt and KDE Frameworks.

{{< img class="text-center" src="okteta.png" link="https://kde.org/applications/utilities/org.kde.okteta" caption="Okteta" >}}

## KMyMoney 5.0.8

[KMyMoney](https://kmymoney.org/), the KDE app that helps you manage your finances, [included several bugfixes](https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html) and an enhancement that added support for check forms with split protocol

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html" caption="KMyMoney" >}}

# Incoming

Keysmith is a two-factor code generator for Plasma Mobile and Plasma Desktop that uses oath-toolkit. The user interface is written in the Kirigami, making it slick on any size of screen. Expect releases soon.

# Chocolatey

{{< img class="text-center" src="chocolatey.png" link="https://chocolatey.org/profiles/KDE" caption="Chocolatey" >}}

[Chocolatey](https://chocolatey.org) is a package manager for the Windows operating system. Chocolatey deals with installing and updating all the software on the system and can make the life smoother for Windows users.

[KDE has an account on Chocolatey](https://chocolatey.org/profiles/KDE) and you can install Digikam, Krita, KDevelop and Kate through it. Installation is very easy: All you have to do is `choco install kdevelop` and Chocolatey will do the rest. Chocolatey maintainers have high standards so you can be sure the packages are tested and secure.

If you are a KDE app maintainer, do consider getting your app added, and if you're a large Windows rollout manager, do consider this for your installs.

# Website Updates

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/" caption="KMyMoney" >}}
Our web team continues to update our online presence and has recently refreshed the [KMyMoney website](https://kmymoney.org).

# Releases 19.12.2

Some of our projects release on their own timescale and some get released en-masse. The 19.12.2 bundle of projects was released today and should be available through app stores and distros soon. See the [19.12.2 releases page](https://www.kde.org/info/releases-19.12.2.php) for details. This bundle was previously called KDE Applications but has been de-branded to become a release service to avoid confusion with all the other applications by KDE and because there are dozens of different products rather than a single whole.

Some of the fixes included in this release are:

* The [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) music player now handles files that do not contain any metadata
* Attachments saved in the *Draft* folder no longer disappear when reopening a message for editing in [KMail](https://kde.org/applications/internet/org.kde.kmail2)
* A timing issue that could cause the [Okular](https://kde.org/applications/office/org.kde.okular) document viewer to stop rendering has been fixed
* The [Umbrello](https://umbrello.kde.org/) UML designer now comes with an improved Java import

[19.12.2 release notes](https://community.kde.org/Releases/19.12_Release_Notes) &bull; [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;  [19.12.2 source info page](https://kde.org/info/releases-19.12.2) &bull; [19.12.2 full changelog](https://kde.org/announcements/changelog-releases.php?version=19.12.2)
