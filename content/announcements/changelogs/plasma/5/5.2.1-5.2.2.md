---
aliases:
- /announcements/plasma-5.2.1-5.2.2-changelog
hidden: true
plasma: true
title: Plasma 5.2.2 complete changelog
type: fulllog
version: 5.2.2
---

### <a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a>

- Pass both caption rect and alignment as output from ::captionRect. <a href='http://quickgit.kde.org/?p=breeze.git&a=commit&h=8568a80a6ed05c260b088528f5f59957cd1c53e0'>Commit.</a> See bug <a href='https://bugs.kde.org/344552'>#344552</a>
- Find KF5GuiAddons and KF5I18n. <a href='http://quickgit.kde.org/?p=breeze.git&a=commit&h=efe9818c5269d721e895828a81e9c69dc307dbe6'>Commit.</a>
- Fixed background color behind scrollbars in dolphin's side panels. <a href='http://quickgit.kde.org/?p=breeze.git&a=commit&h=372e8d9af83708bb1dc3b77653c59e894535b654'>Commit.</a>
- Removed ellision flag from captionRect (broken anyway). <a href='http://quickgit.kde.org/?p=breeze.git&a=commit&h=850906e9d78e1b928d2865057310d4f52caabe6c'>Commit.</a> See bug <a href='https://bugs.kde.org/344552'>#344552</a>
- On testing again breeze icons do work in gtk. <a href='http://quickgit.kde.org/?p=breeze.git&a=commit&h=e34fd96b428150a0f772b95762cb444705d55f34'>Commit.</a>
- Create gtk-3.0 directory before putting the settings file into it. <a href='http://quickgit.kde.org/?p=breeze.git&a=commit&h=8614a8245741a1282a75a36cb7c67d181ec435a0'>Commit.</a>
- Check widget validity before calling deleteLater. <a href='http://quickgit.kde.org/?p=breeze.git&a=commit&h=72d18b7f5245d265fb0eb974854989aebc357724'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344980'>#344980</a>
- Return parent style method when option passed to tabBar rect is invalid, instead of full option rect. <a href='http://quickgit.kde.org/?p=breeze.git&a=commit&h=ac6e765800cf6e78a2cae8692f6c49b75df49bda'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344779'>#344779</a>
- Check scrollbar policy before forwarding events. <a href='http://quickgit.kde.org/?p=breeze.git&a=commit&h=759ff9bf1ba38a47214849bc0a80cfbc7333c3a9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343659'>#343659</a>
- Added an explicit flag as output from captionRect to tell whether caption must be ellided or not. <a href='http://quickgit.kde.org/?p=breeze.git&a=commit&h=a8d2104ab36c91053da1fee3ad31d68c979965a7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344552'>#344552</a>

### <a name='kde-gtk-config' href='http://quickgit.kde.org/?p=kde-gtk-config.git'>KDE GTK Config</a>

- Use the correct version of desktop_to_json. <a href='http://quickgit.kde.org/?p=kde-gtk-config.git&a=commit&h=2aabda96dc21abf1e2aa127bacf94045f9ac8efc'>Commit.</a>
- Fix ecm_install_icons warning. <a href='http://quickgit.kde.org/?p=kde-gtk-config.git&a=commit&h=3afa5cb0ca8a8c2beed0db200f8cf15c73dc2aaa'>Commit.</a>
- Configsavetest: Use QTEST_GUILESS_MAIN. <a href='http://quickgit.kde.org/?p=kde-gtk-config.git&a=commit&h=d0ea02d18664b8090b58f30ad8855f28afa8fb83'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123045'>#123045</a>
- This targets are used in project; find them. <a href='http://quickgit.kde.org/?p=kde-gtk-config.git&a=commit&h=70fed04b27c3c3b9a7c8ff69b697474d4557fa95'>Commit.</a>
- Simplify regular expression that matches fonts. <a href='http://quickgit.kde.org/?p=kde-gtk-config.git&a=commit&h=74a7f8ee6d4a51a836522dcf3d198ec2f897a995'>Commit.</a>
- Disable test for now. <a href='http://quickgit.kde.org/?p=kde-gtk-config.git&a=commit&h=56ef1808b35b040883c3a23f2a09efdc93a37390'>Commit.</a>
- Fix issue in font parser. <a href='http://quickgit.kde.org/?p=kde-gtk-config.git&a=commit&h=c6d5b96a1b4bbc1a93a98e798c8a2626d059e489'>Commit.</a>

### <a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a>

- Replace newlines by <br>. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&a=commit&h=b297537aeca83894643bb13f94ba3ff018d4a537'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/345194'>#345194</a>
- Set IBeam cursor on notes applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&a=commit&h=66d7c74b8a9b8be4b3f885f701bf537f179c1499'>Commit.</a>
- Add missing TRANSLATION_DOMAIN. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&a=commit&h=9192d38f29330e1de801e55314886e6bcd52600a'>Commit.</a>

### <a name='kio-extras' href='http://quickgit.kde.org/?p=kio-extras.git'>KIO Extras</a>

- Add header. <a href='http://quickgit.kde.org/?p=kio-extras.git&a=commit&h=afdb83d0f8986e4b842b70861f2bcd764018f1fd'>Commit.</a>
- Fix thumbnail previews for directories. <a href='http://quickgit.kde.org/?p=kio-extras.git&a=commit&h=b027b0191826d2b937213b0028831edd28eeb479'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/122917'>#122917</a>

### <a name='kscreen' href='http://quickgit.kde.org/?p=kscreen.git'>KScreen</a>

- Fix tests (upower interface was removed long time ago). <a href='http://quickgit.kde.org/?p=kscreen.git&a=commit&h=019ded5ff926cc031550343cf52d8338bc3e0c2e'>Commit.</a>
- KDED: Make Generator maximally fool-proof. <a href='http://quickgit.kde.org/?p=kscreen.git&a=commit&h=9a4f36f6973d6f9d712b6b3b1ff30f868d9910b9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/326499'>#326499</a>. Fixes bug <a href='https://bugs.kde.org/326120'>#326120</a>. Fixes bug <a href='https://bugs.kde.org/327075'>#327075</a>. Fixes bug <a href='https://bugs.kde.org/33716'>#33716</a>
- KCM: Prevent a possible crash when setting up outputs with a single mode. <a href='http://quickgit.kde.org/?p=kscreen.git&a=commit&h=527d51e3f99c80e94dbace4acaebc99fe86645cd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344694'>#344694</a>
- KCM: Don't call QQuickItem::update() upon docking. <a href='http://quickgit.kde.org/?p=kscreen.git&a=commit&h=9fdbb838053476faa1fc3c94aa7c9457d23f79ac'>Commit.</a>
- KCM: evaluate docking right after initial placement. <a href='http://quickgit.kde.org/?p=kscreen.git&a=commit&h=3aca66255540d3bdd95905d9fe1b98d2bb3fdab5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/320012'>#320012</a>
- Fix generator test. <a href='http://quickgit.kde.org/?p=kscreen.git&a=commit&h=6c77c7f75de5eeaf2c0ca02c8bbd1045c9ae079b'>Commit.</a>
- KDED: make mode lookup in Serializer::findOutput() more bullet-proof. <a href='http://quickgit.kde.org/?p=kscreen.git&a=commit&h=f056ead8bc7d7a5bdf573c092a04cd3e47ea97a9'>Commit.</a> See bug <a href='https://bugs.kde.org/342675'>#342675</a>
- KCM: Don't create new UI Widget whenever KCMKScreen::configReady is called. <a href='http://quickgit.kde.org/?p=kscreen.git&a=commit&h=d66d692b5c996fefe9e6835cbe6acece7aa8bc06'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343829'>#343829</a>
- QMLOutput: fix rounding error in updateRootProperties(). <a href='http://quickgit.kde.org/?p=kscreen.git&a=commit&h=a2d488623344e968e3e65627824e7ae369247094'>Commit.</a>

### <a name='ksysguard' href='http://quickgit.kde.org/?p=ksysguard.git'>KSysGuard</a>

- Register KSysguard to DBus. <a href='http://quickgit.kde.org/?p=ksysguard.git&a=commit&h=b3900ddedacc92db2eeec05e521fdaee94076230'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344595'>#344595</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- Fix build. <a href='http://quickgit.kde.org/?p=kwin.git&a=commit&h=25f71d1a1267d773562d8e42ecd115f1401842a8'>Commit.</a>
- Fix installation of GHNS material. <a href='http://quickgit.kde.org/?p=kwin.git&a=commit&h=9bddd0fe8a7a909e0704ce215666d4a8a23a8307'>Commit.</a>
- Calc undecorated offset _before_ removing deco. <a href='http://quickgit.kde.org/?p=kwin.git&a=commit&h=b37d88a29f8aed2caa29d1a4b2fad92c8bc168b7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344234'>#344234</a>
- Do not delete and recreate close button. <a href='http://quickgit.kde.org/?p=kwin.git&a=commit&h=ac3aef8dfcd1f849847fdc1015e4f5f817d7eb40'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344676'>#344676</a>
- Fix loading of effect config plugins. <a href='http://quickgit.kde.org/?p=kwin.git&a=commit&h=25fc115a9c3210fb430925786542f40dc7be6f1a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/122821'>#122821</a>

### <a name='libkscreen' href='http://quickgit.kde.org/?p=libkscreen.git'>libkscreen</a>

- ConfigSerializer: add missing QDBusArgument::endMap() call. <a href='http://quickgit.kde.org/?p=libkscreen.git&a=commit&h=a957d97d2687f4707aea05f85ce9d2027904690e'>Commit.</a>
- XRandR1.1: Fix crash when connected through RDP. <a href='http://quickgit.kde.org/?p=libkscreen.git&a=commit&h=1adcb4a3324959ea9be1fcdad318dafba4a3ffcc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/333277'>#333277</a>
- ConfigMonitorPrivate:: fix initialization order. <a href='http://quickgit.kde.org/?p=libkscreen.git&a=commit&h=725584f2cc0c3c9a10bb6e6f643b81a43ce125e5'>Commit.</a>

### <a name='milou' href='http://quickgit.kde.org/?p=milou.git'>Milou</a>

- Add missing KF5 dependancies. <a href='http://quickgit.kde.org/?p=milou.git&a=commit&h=a53085390be6a1f6405abd93694cd612841a60fc'>Commit.</a>

### <a name='muon' href='http://quickgit.kde.org/?p=muon.git'>Muon</a>

- Remove remaining use of muonapt_export and the file itself. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=a101442a7fb5e1915af5d1166302ac24c04f7c23'>Commit.</a>
- Remove unneeded dependency. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=a943a5174c98817c790bceb732756743b420b6d2'>Commit.</a>
- Fix build. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=3dc8fe13df69b29d1e7e420f376245b567e39966'>Commit.</a>
- Fix build on ubuntu. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=77d0150ac048567d4b02cda74a358149802b6eaf'>Commit.</a>
- Fix PackageKit::resourceByPackageName when an appstream package id is provided. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=1a805e11975626ce66ffa3294947af8e9000db5c'>Commit.</a>
- Reduce cluttering in muon-updater. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=4c65472904ced79933108041824e0438af26d0a1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/345304'>#345304</a>
- Fix warnings. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=917cc8d2f007f7fe0f20dc2260e75c66a3638fb2'>Commit.</a>
- Fix build of muon. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=28290fb503a138795400e71fc9a01f5763f6f58b'>Commit.</a>
- Fix build of muon. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=87154f20a243387368d06bb46e0d6e8faa94b8a9'>Commit.</a>
- Remove second calls to project(). <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=80a9dd9da4b6e981e43543973cfd046019e02d5a'>Commit.</a>
- Port muon package manager away from KUniqueApplication and K4AboutData. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=840420789c81a5ee5f4af44d3450d7ab43ee1544'>Commit.</a> See bug <a href='https://bugs.kde.org/344836'>#344836</a>
- Fix compile, reviewed by aleix. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=87e1eca758b7f9688ef8a8c7228495d925fa0f58'>Commit.</a>
- Hide categories coming from invalid backends. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=ad6f1a8a708ffbae1a0b301cbeb5cb06f30432de'>Commit.</a>
- Provide a set of plugins for a given Category. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=e8b7cb0b6f0dd1c83795e05be1527233ac857c88'>Commit.</a>
- Properly feed the backend name to the backend. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=f3793986153dd860c489fb9cf1cfb4f3430631bc'>Commit.</a>
- Constify. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=9997d6f79efbf5b4ee62647ad593949598742d1b'>Commit.</a>
- Take invalid resources into account. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=ff873a558b2ab0ef849c8bfab7ea8e1f5a8250dd'>Commit.</a>
- If no good categories were found, mark as invalid. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=129bd92859524333e19e308c3ad928475d8b7d1b'>Commit.</a>
- Store the name within the backend. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=6c176d5cc84ef860869db4033201fd314a84de8a'>Commit.</a>
- Micro-optimization of the day. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=bf0784857824ec90543a10d043448a19a8ae1fb1'>Commit.</a>
- Add warning if an invalid category was found. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=ab8cca09fb0dc7f18da37c2dd780d4778683f3bc'>Commit.</a>
- Make sure the plasmoid categories get mixed. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=8c6ffc2e27c65a6f66ff68a106a03455a0547270'>Commit.</a>
- Wait to fetch the KNS backend until we have OCS providers. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=a644be96c4c446b904a194278f35bb0e0540aabc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344840'>#344840</a>
- KNS Category files were interchanged. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=f86616f78ff019aeb6f029ba92fe1da70843bea4'>Commit.</a>
- --verbose. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=8455dbc54e2f75dcba1113e74ffc35f6c30403ee'>Commit.</a>
- If the shown image has a problem, skip to the next one. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=296740b864e70f7555d9e75f1ac9003a4d712b17'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344830'>#344830</a>
- Add back patch for bug 331374. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=3a9d2769b6ce8b8ec1b25d5db2782dd004fe7d7d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/331374'>#331374</a>. Fixes bug <a href='https://bugs.kde.org/344831'>#344831</a>
- Fix Exec line so muon and muon-updater can be run from the menu. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=d43a3ad280bb64b4c44eb535b34d023142c403fd'>Commit.</a>
- Port commented-out code. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=4017ffd12f6b1efacd02d170fba74048f7c5de9b'>Commit.</a>
- Remove apt-listbugs usage. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=70b3e496fccf3378c186ca32f3c7d8eb8c81b1fc'>Commit.</a>
- Fix build on older versions of KF5. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=0a43389122da3b5d6aace89f1464e6e379574b08'>Commit.</a>
- Add warning at least, needs a proper alternative path. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=1b4aa95c268091b84617a824200f1c441db60df1'>Commit.</a>
- Support for having multiple packages using the same package name. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=6f69b32400cfdf310c90580ab8e9cc18d2776e4f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344780'>#344780</a>
- Use xi18n() in case extended tags are being used. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=dfb84a4a408ce5686cff347cffcd6c579646c76b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344709'>#344709</a>
- Fix error message. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=ce9b31718c542f1361fe61aee8597e34691194d3'>Commit.</a>
- Improve title when filtering by mimetype. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=7eeb49927bc6c114d42719d5093a8d6497c1f258'>Commit.</a>
- Resolve conflict. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=0be9747c72bceed925666dbf596cbe2ef2ff604c'>Commit.</a>
- Compile-time connects for the notifier. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=420875d68c4c12a1a9f9b9d4f20a8e0d596ad9dc'>Commit.</a>
- Look for all updates to display for the notifier. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=fffe4f68e99d083f9bb181345e96b6b6576d7888'>Commit.</a>
- Prefer connecting to updatesChanged to polling. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=c94daf3757ee95461216d113e9fd33695dd32844'>Commit.</a>
- Fix launching muon-updater from menu. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=c3c5b5d8c470b972dcbededae9eaaf7458196404'>Commit.</a>
- Only look for AppstreamQt 1.1. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=ce525fa180e2981b2a92d49c68e648b9ac14881b'>Commit.</a>
- Fix connection. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=3cc8f87c7b17365eb2312fb9a4d6f3408f9ca2bb'>Commit.</a>
- Add a reload action for packagekit as well. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=cf15a6700a2d9041a7399cbc2c77999ca521c81b'>Commit.</a>
- Fix build of packagekit backend on master. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=3937010c68f1b8b6ca901596f20a750e08875dfc'>Commit.</a>
- Use screenshots provided by appstream, if available. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=eb0c46edc9c15b89f0f66c4f7b1ba1245deee1bc'>Commit.</a>
- Explicitly mark as override. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=06f20c3e8c94fd5198facf51e4e96ee1dcd5c31f'>Commit.</a>
- Fix Breadcrumbs display. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=69fb094cf62609be306ae2ecc757d9366d212bde'>Commit.</a>
- Add some spacing left and right into the MessageAction. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=a372f1ae9f1ffa52e43b885044f386deb74d0f49'>Commit.</a>
- Fix build on older versions of KF5. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=bb46150b1f7104a2161da6dfbd20e1164d589542'>Commit.</a>
- Fix build. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=4a111ffa50ca353ee71d971a0b29c74061d8225d'>Commit.</a>
- Make sure we don't get null entries. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=32dfd61bbc52d980b9155706cf2180b034142db3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/321657'>#321657</a>
- Fix last updates time report in muon-updater. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=7f2399cd3704ce3583b2c6c66c9b83211f7edb28'>Commit.</a>
- Fetch the package before finding the executables. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=1bd7d81167eb6d46deea2f1b52d4d9c4a2a577d3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/319006'>#319006</a>
- Remove muon-installer. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=5326151f7ef09aa356ea09089cc7816f98d06583'>Commit.</a>
- Make it possible to dig more about the currently selected resource. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=a04e7cd17e64ed14df9c82f8a33f372205701ee5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/324721'>#324721</a>
- Show the currently installed version together with the changelog. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=46a49bb2d76fdc2800d91d2d5677c955243e771a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/323595'>#323595</a>
- Better changelog for dummy resources. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=8e652026d1dc207003f750551f97052a56b8166b'>Commit.</a>
- Introduce a new test within the SourcesTest for APT. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=88433b3fd9ad816f1ac05d6f5205cc03165eac84'>Commit.</a>
- Simplify filtering code in ResourcesProxyModel. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=8e88df4f61a47814e6c6a226947d62d2be7f7100'>Commit.</a>
- Fix glitch when removing message actions. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=2a77ffa0afff6fedb3fc587c9c8296dce070bc41'>Commit.</a>
- Make sure we're not using the deprecated API. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=0484bba55b0e5dad257d1fdc7a039c5cc9f58d7b'>Commit.</a>
- Remove unused variable. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=d0275959748c447e2f528dedd57a492b2d967763'>Commit.</a>
- Fix build after the renaming. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=a98604eb1d447c9843a8a6f6b668f998a6739e8c'>Commit.</a>
- CMake changes around libmuonprivate. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=0a94ffe31c5b74b1de4d89326df61bb5f406f14c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/122037'>#122037</a>
- Remove spacing in the main RowLayout. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=c9acf9a396a61848a6ce14271ac666e1261d1900'>Commit.</a>
- Don't make kxmlgui deal with the toolbar either. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=4425b1eae8530568f4ab26201e83eddcb8fd2501'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/326713'>#326713</a>
- Let the layout take care about the size of the SourcesPage delegate. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=481ef1ffee4b1c3421d1a34014063b02630522dd'>Commit.</a>
- Fix crash in MessageActionsModel. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=5678047e337871a8526dbc7c0591836f14da922d'>Commit.</a>
- Use more sensible colors for Discover's message action. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=3638474dd4649def43dfe0f987e4fcbd34495937'>Commit.</a>
- Introduce high riority MessageActions. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=5ed5f0d9f738bc36d471ccdf375e873d0eab2308'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/341126'>#341126</a>
- Simplify the implementation of the main view. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=b87e71dc360d20cb14141fa013eceffae2b7e469'>Commit.</a>
- Add a dummy action of high importance. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=3187425869fe0dbe6624cf13cde3c6aa05bab73f'>Commit.</a>
- Introduce message actions on muon discover as well. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=a1a6e1002f854bd42892f620732df1ad7f13d986'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/331419'>#331419</a>
- Move the message actions from the updater interface to the backend. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=7d3c0410c414a73fd7fd6dcb732eaae8ff230078'>Commit.</a>

### <a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a>

- Find Qt5DBus, it is used internally. <a href='http://quickgit.kde.org/?p=oxygen.git&a=commit&h=d450fc370b52de99875442f0fb3c0fc03608e4f2'>Commit.</a>
- Return parent style method when option passed to tabBar rect is invalid, instead of full option rect. <a href='http://quickgit.kde.org/?p=oxygen.git&a=commit&h=e4015d51c665488bb342ee438e5388d8b978081f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344779'>#344779</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- Extract UI messages. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&a=commit&h=12f750d497c5f2def14d89ad669057e13197b6f8'>Commit.</a>
- Don't eat hover events when not visible. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&a=commit&h=258aaea34de31d2c652b0441793ee7c33313b6c7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344788'>#344788</a>
- Search for KF5GlobalAccel. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&a=commit&h=57ae6c3ce0446aff6fd0f0536be6fbe744d2e7a7'>Commit.</a>
- Close config dialog when panel gets locked from context menu. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&a=commit&h=113ea3c638b910c3ac9f411a2e41a88110650ab4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/345068'>#345068</a>
- Fix tabbar not showing up when switching to favorites from search. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&a=commit&h=336094f43d82d83f374116862a0f568fe9092224'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343524'>#343524</a>
- Fix double click. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&a=commit&h=429d1bb6bfb32c4a4ad3e9e682513ab2625c823c'>Commit.</a>
- Fix Bug 266760 - The autostart kcm doesn't show the correct name immediately after "adding program". <a href='http://quickgit.kde.org/?p=plasma-desktop.git&a=commit&h=ecbefe504d792aa5700451e8987ec05fdae2bf1d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/266760'>#266760</a>
- Fix the no checkbox being checked for displayText config. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&a=commit&h=c50e4b987dc365d741880794ee6f2924065a87a6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344711'>#344711</a>

### <a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a>

- Do not ask for a pin when it's already stored in NM or set to be not required. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=c4bdaecbdf2ae2bc30d45b5cd98d3f28648c879c'>Commit.</a>
- Add proper validation for GSM config widget. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=02a05dda064e6a345334efc6e5b80062fc2fa38a'>Commit.</a>
- Use QPointer for advanced permission dialog. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=ea499addeafbdd7fccc110a5f9f97e0338fc01d3'>Commit.</a>
- Fix 345133 - Advanced Permissions Editor doesn't work. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=48d348fb8f2c41617c7aeeb67e156e0fb51c6b5d'>Commit.</a> See bug <a href='https://bugs.kde.org/345133'>#345133</a>
- Correctly fix enabling/disabling actions when selection changes. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=9888d2b059c8cf95176742c4ef0742f0629b60f8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/345021'>#345021</a>
- Fix build. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=ca170d91ed757f34766c31c16f023e86d90c3a55'>Commit.</a>
- WPA/WPA2 Enterprise 802-1x security setting editor displays. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=5786e8c63a724dfac967436e321d377cb1b8b06b'>Commit.</a>
- Register dbus type returned from bluez, otherwise we alway get an error. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=66fa9ba6852f1755f31b8293f019f7dd98d7996c'>Commit.</a>
- Give to last used connections higher priority. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=e1a3cbd3ec99d52ee2e9fda94eb55927c49a5066'>Commit.</a>
- Make sure that connection name or SSID is not misinterpret as HTML. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=75f8e8fbe3af92df9d046438686d86501d5c6b20'>Commit.</a>
- Make sure SSID will be displayed properly when using non-ASCII characters. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=cbd1e7818471ae382fb25881c138e0228a44dac4'>Commit.</a> See bug <a href='https://bugs.kde.org/342697'>#342697</a>
- Distinguish between active connection and connection which is being activated. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=c95d75ea97f2734849ef0749ed763ea3422c4032'>Commit.</a>
- Re-check enabled/disabled actions for a selected connection when it gets changed. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=d28875db0dbdffd1f3e03f6e54aa6761da3f7d98'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343606'>#343606</a>
- Fix saving the CA cert paths. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=2bc1ad72afc501ad8ca010df705f69a59d5f4189'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344609'>#344609</a>
- Show correct connection name. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=51dab26b13082e3e5c0d26d0a2e6088940953eec'>Commit.</a> See bug <a href='https://bugs.kde.org/344411'>#344411</a>. See bug <a href='https://bugs.kde.org/334901'>#334901</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- Digitalclock: Don't reverse js timezone offset when using to generate utc from local time. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&a=commit&h=0b82fc7e455bdc2c4b419c26e1f5349205c25a65'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123079'>#123079</a>
- [applets/notifications] Remove all popups from the on-screen-list. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&a=commit&h=ef14c3847c774dd1ad4efe2f726caed7792fe76b'>Commit.</a>
- Add systemsettings5 to DrKonqi mappings. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&a=commit&h=837b9890eb977da835c6bc2dae63c4dfc5d256c7'>Commit.</a>
- Use fontSizeMode Text.Fit for NotificationIcon. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&a=commit&h=f7f12485aab02785bd7bfb880deba39615c53f02'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/122948'>#122948</a>
- Don't set a width and height. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&a=commit&h=187bc243a568c22531f1e1a3fdc3ea1127ffac40'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343657'>#343657</a>
- Show pause/stop buttons in jobs notification only for suspendable/killable jobs. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&a=commit&h=69fb206e395a9795648bd3e4ba28898f1c042c99'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/122920'>#122920</a>
- Fix displaying label1 in job notifications. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&a=commit&h=6b462e70ec6072b9861b57e720407ada9a78ac80'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/122916'>#122916</a>
- Take Shown and Hidden status in account when showing sidebar. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&a=commit&h=5b3b84b687a569731a74cc6cfa1284ebb5de169b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344795'>#344795</a>
- [libtaskmanager] Use the icon loaded from config file for launchers. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&a=commit&h=b169753ce33bff5a4f8d0983226fec7790df85d8'>Commit.</a>
- Don't set mode here, otherwise default will not work. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&a=commit&h=c12f8ee26333c1ad7dc6c115a2a0dbfa3dc36975'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344701'>#344701</a>
- Add missing import. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&a=commit&h=8af3b4084afc1d0959cd40378916b4f466ce92ed'>Commit.</a>
- [lookandfeel] Make sure the OSD is not bigger than 1/3 of the screen. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&a=commit&h=2c733431d69a23cd6bea385116b74434d0538dbe'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/122738'>#122738</a>
- Set aboutData for plasmashell. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&a=commit&h=6821b69fa1b06375123bffa0a7a492276a831a2b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/122704'>#122704</a>
- [digital-clock] Invert the JS timezone offset values. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&a=commit&h=93c512fece2d70b8881cba3b2b2eacfe5cd6ded6'>Commit.</a>

### <a name='powerdevil' href='http://quickgit.kde.org/?p=powerdevil.git'>Powerdevil</a>

- Explicitly find and use kdbusaddons. <a href='http://quickgit.kde.org/?p=powerdevil.git&a=commit&h=f82a0196dbc5623c137c14e3a7105afb799f77eb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123047'>#123047</a>
- Change default low battery threshold to 10%. <a href='http://quickgit.kde.org/?p=powerdevil.git&a=commit&h=3836e992b179eff8538c4321dd9f3861b0a0c00b'>Commit.</a>
- Fix default critical action. <a href='http://quickgit.kde.org/?p=powerdevil.git&a=commit&h=00eed59164a92c70030913728e81c37397b53551'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344905'>#344905</a>
- Prepare for kernel 4.0 ;). <a href='http://quickgit.kde.org/?p=powerdevil.git&a=commit&h=2d3a341bfc11c89dcd80269dd8abb5f991349f00'>Commit.</a>