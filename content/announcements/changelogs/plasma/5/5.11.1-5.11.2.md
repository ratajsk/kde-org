---
aliases:
- /announcements/plasma-5.11.1-5.11.2-changelog
hidden: true
plasma: true
title: Plasma 5.11.2 Complete Changelog
type: fulllog
version: 5.11.2
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Include the progress of transactions. <a href='https://commits.kde.org/discover/44f03fc0fbcae5767d0d50fe1d20175614523266'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384038'>#384038</a>
- Fix adding remote sources. <a href='https://commits.kde.org/discover/57c62928e8d39b74f3c75e129e48c6449a680419'>Commit.</a>
- List items should be lighter than normal background. <a href='https://commits.kde.org/discover/b17e2780e7c601bba22203e8d7475718c78b16d7'>Commit.</a>
- Use QQC2.Popup instead of Kirigami.OverlaySheet for screenshots. <a href='https://commits.kde.org/discover/3e0e3ce8db656595062279ad4792effcd1186ffd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385115'>#385115</a>
- Don't open a null application from the progress view (update transaction). <a href='https://commits.kde.org/discover/061360f1e6552709a5b3b13da043bf6326a72ffa'>Commit.</a>

### <a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a>

- Properly access the system's GTK settings. <a href='https://commits.kde.org/kde-gtk-config/efa8c4df5b567d382317bd6f375cd1763737ff95'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382291'>#382291</a>.

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Dictionary Engine: fix synchronization issues. <a href='https://commits.kde.org/kdeplasma-addons/02d8fbfb19285b09126d819e7da0c315e3b28d68'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8320'>D8320</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Default X font DPI to 96 on wayland. <a href='https://commits.kde.org/plasma-desktop/fae658ae90bf855b391061a5332a1a964045e914'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/8287'>8287</a>
- Kcm baloo: Fix extraction of folder basename for error message. <a href='https://commits.kde.org/plasma-desktop/5a8691900fea2b77ae194f509d17e7368235b4c1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8325'>D8325</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Do not load initially selected connection twice. <a href='https://commits.kde.org/plasma-nm/6c57f972c20580e9c79714cc2ded7ae8f4d7307e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379756'>#379756</a>
- Fix focus in DNS servers and DNS domains dialogs. <a href='https://commits.kde.org/plasma-nm/13a513b55eb467f9f1d05945c95d45ecf88e4d86'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385839'>#385839</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Sync xwayland DPI font to wayland dpi. <a href='https://commits.kde.org/plasma-workspace/a09ddff824a076b395fc7bfa801f81eaf2b8ea42'>Commit.</a>
- Dict Engine: various cleanups. <a href='https://commits.kde.org/plasma-workspace/51d8b5a1d0694afeeb7ac2f397c165ea820f5273'>Commit.</a>
- Dict Engine: improve error handling. <a href='https://commits.kde.org/plasma-workspace/c2cf81f26bfebf8c11fe5bfd6695410369ca9519'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8419'>D8419</a>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

- Fix colours not updating in systemsettings. <a href='https://commits.kde.org/systemsettings/5f9243a8bb9f7dccc60fc1514a866095c22801b8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8399'>D8399</a>