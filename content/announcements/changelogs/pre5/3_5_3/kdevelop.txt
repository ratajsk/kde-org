2006-04-01 19:44 +0000 [r525366]  dymo

	* branches/KDE/3.5/kdevelop/languages/cpp/backgroundparser.cpp:
	  Backport the fix from 3.4 branch here. Many people reported it
	  helps.

2006-04-02 20:08 +0000 [r525771]  dfaure

	* branches/KDE/3.5/kdevelop/parts/appwizard/appwizarddlg.cpp:
	  Exiting on me because of an error in a module is a bit...
	  unexpected. kdevelop (appwizard): There was an error loading the
	  module KDevCVSIntegrator kdevelop (appwizard):
	  libcvsservice.so.0: cannot open shared object file: No such file
	  or directory OK, but surely I can do without CVS in such a
	  case...

2006-04-03 19:10 +0000 [r526072]  rdale

	* branches/KDE/3.5/kdevelop/src/profiles/IDE/ScriptingLanguageIDE/RubyIDE/profile.config:
	  * Re-enable the 'Quick Open' and 'File Groups' plugins for Ruby,
	  as requested by Tim Harper

2006-04-09 04:39 +0000 [r527678]  mattr

	* branches/KDE/3.5/kdevelop/languages/cpp/subclassingdlg.cpp,
	  branches/KDE/3.5/kdevelop/parts/filelist/projectviewpart.cpp,
	  branches/KDE/3.5/kdevelop/languages/kjssupport/subclassingdlg.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/cppcodecompletion.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/cppnewclassdlg.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/createpcsdialog.cpp,
	  branches/KDE/3.5/kdevelop/parts/documentation/protocols/chm/decompress.cpp,
	  branches/KDE/3.5/kdevelop/parts/documentation/protocols/chm/chmfile.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectwidget.cpp,
	  branches/KDE/3.5/kdevelop/parts/filecreate/filecreate_part.cpp:
	  merge the following revisions from the kdevelop 3.4 branch: -
	  527665 - 527667 - 527668 - 527676

2006-04-11 00:13 +0000 [r528440]  mattr

	* branches/KDE/3.5/kdevelop/languages/python/app_templates/pyqt/app.py:
	  fix the python template so that the file dialog works out of the
	  box.

2006-04-11 00:16 +0000 [r528442]  mattr

	* branches/KDE/3.5/kdevelop/parts/documentation/plugins/chm/docchmplugin.h,
	  branches/KDE/3.5/kdevelop/parts/documentation/interfaces/kdevdocumentationplugin.h,
	  branches/KDE/3.5/kdevelop/parts/documentation/protocols/chm/chm.cpp,
	  branches/KDE/3.5/kdevelop/parts/documentation/plugins/chm/docchmplugin.cpp:
	  add support for reading TOC from CHM documentation.

2006-04-12 23:41 +0000 [r529276]  mattr

	* branches/KDE/3.5/kdevelop/languages/python/kde_pydoc.py: backport
	  new kde_pydoc.py file that adds support for python 2.4

2006-04-15 21:54 +0000 [r530247]  dymo

	* branches/KDE/3.5/kdevelop/parts/appwizard/appwizarddlg.cpp:
	  Backported the fix for QTextStream encoding.

2006-04-21 11:06 +0000 [r532146]  rdale

	* branches/KDE/3.5/kdevelop/languages/ruby/rubysupport_part.cpp: *
	  Applied patch from Tim Harper for the Ruby class parser: * This
	  will recognize classes like so: class
	  Widget::WidgetContentTextController <
	  Widget::WidgetContentController and, just discards the namespace
	  Widget:: and stores WidgetContentTextController. CCMAIL:
	  kdevelop-devel@kdevelop.org

2006-04-24 10:41 +0000 [r533279]  rdale

	* branches/KDE/3.5/kdevelop/languages/ruby/debugger/variablewidget.cpp:
	  * Fixed bug in accessing elements of Arrays and Hashs within
	  instance variables

2006-05-11 16:07 +0000 [r539734]  escuder

	* branches/KDE/3.5/kdevelop/languages/php/phpsupportpart.cpp:
	  KDevelop / PHP : Fix multiple add of the same function in the
	  global NameSpace

2006-05-15 14:03 +0000 [r541073]  rdale

	* branches/KDE/3.5/kdevelop/languages/ruby/app_templates/kapp/main.rb:
	  * Remove redundant DCOP registration call

2006-05-23 10:34 +0000 [r543997]  binner

	* branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kontact/src/main.cpp,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdepim/akregator/src/aboutdata.h,
	  branches/KDE/3.5/kdepim/kmail/kmversion.h,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdepim/kaddressbook/kabcore.cpp,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdeutils/kcalc/version.h,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version for KDE
	  3.5.3

