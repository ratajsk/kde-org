2007-10-11 12:03 +0000 [r724100]  lunakl

	* branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.cpp:
	  Don't unmount media from other backends. Fixes a dbus assert with
	  nfs mounts (':' in dbus path).

2007-10-11 18:01 +0000 [r724191]  dfaure

	* branches/KDE/3.5/kdebase/kdesktop/kdiconview.cc: Apply patch from
	  Anthony Mercatante which makes kdesktop support TryExec for
	  .desktop files on the desktop. Thanks! BUG: 144251

2007-10-19 14:13 +0000 [r727043]  lunakl

	* branches/KDE/3.5/kdebase/kioslave/thumbnail/thumbnail.cpp: Sigh
	  ... non-truecolor images have a palette, and it should not be
	  just ignored as if everything was 32bpp. Convert to 32bpp to
	  avoid the problem (bnc:334965)

2007-10-21 10:51 +0000 [r727634]  mueller

	* branches/KDE/3.5/kdebase/kcontrol/konqhtml/appearance.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/konqhtml/htmlopts.cpp: the
	  usual "daily unbreak compilation"

2007-10-24 10:09 +0000 [r728812]  wstephens

	* branches/KDE/3.5/kdebase/klipper/klipperrc.desktop: Apparently
	  Mozilla changed the WM_CLASS; add 'gecko' to list of no-action
	  windows

2007-10-31 12:38 +0000 [r731330]  ossi

	* branches/KDE/3.5/kdebase/kdesktop/xautolock.cc: micro-fixes

2007-10-31 14:25 +0000 [r731364]  ossi

	* branches/KDE/3.5/kdebase/kdesktop/xautolock.cc: don't depend on
	  DPMS if it is disabled at startup/reconfigure.

2007-10-31 14:55 +0000 [r731374]  ossi

	* branches/KDE/3.5/kdebase/kdesktop/lockeng.cc,
	  branches/KDE/3.5/kdebase/kdesktop/lockeng.h,
	  branches/KDE/3.5/kdebase/kdesktop/kdesktop.kcfg: DPMS-dependend
	  has a rather strange double-meaning: - the original meaning is
	  the question whether the autolocker should be disabled when dpms
	  is disabled by some application (tv, media player, etc.). i think
	  we decided that this option makes absolutely no sense - everybody
	  wants it on. therefore i'm deleting this meaning. - the new
	  meaning introduced by aleXXX actually makes sense: screen savers
	  are usually cpu burners which have little point in running when
	  dpms kicks in. however, there are savers like SETI@home, etc.
	  that should not be suspended.

2007-10-31 17:17 +0000 [r731425]  gyurco

	* branches/KDE/3.5/kdebase/kioslave/ldap/kio_ldap.cpp: Backport
	  memleak fix.

2007-10-31 18:30 +0000 [r731445]  ossi

	* branches/KDE/3.5/kdebase/kdesktop/xautolock_engine.c: make the
	  corner action delay slightly less excessive. this needs a rewrite
	  to be really good, though.

2007-10-31 19:00 +0000 [r731450]  ossi

	* branches/KDE/3.5/kdebase/kdesktop/lockeng.cc: don't activate the
	  autolock after unlocking if it wasn't before.

2007-10-31 21:42 +0000 [r731514]  djarvie

	* branches/KDE/3.5/kdebase/knetattach/knetattach.ui: Make it
	  compile

2007-10-31 22:18 +0000 [r731525]  djarvie

	* branches/KDE/3.5/kdebase/kxkb/kcmmiscwidget.ui: Make it compile

2007-11-01 00:35 +0000 [r731564]  djarvie

	* branches/KDE/3.5/kdebase/kcontrol/dnssd/configdialog.ui: Make it
	  compile

2007-11-01 22:10 +0000 [r731815]  ossi

	* branches/KDE/3.5/kdebase/kdmlib/dmctl.cpp: support gdm 2.19+

2007-11-03 04:30 +0000 [r732202]  jhall

	* branches/KDE/3.5/kdebase/doc/faq/about.docbook: Updating
	  maintainer email address until I get the alias fixed

2007-11-07 16:29 +0000 [r733937]  lunakl

	* branches/KDE/3.5/kdebase/ksmserver/legacy.cpp: Don't start the WM
	  again also if it uses the old SM protocol.

2007-11-10 09:50 +0000 [r734868]  lunakl

	* branches/KDE/3.5/kdebase/ksmserver/server.cpp,
	  branches/KDE/3.5/kdebase/ksmserver/server.h: Fix compile.

2007-11-13 15:10 +0000 [r736127]  adridg

	* branches/KDE/3.5/kdebase/kdesu/kdesu/kdesu.cpp: Avoid
	  NULL-pointer deref in setenv(), FBSD PR 118007 CCMAIL:
	  kde@freebsd.org

2007-11-15 15:32 +0000 [r737133]  ossi

	* branches/KDE/3.5/kdebase/kdm/kfrontend/genkdmconf.c: backport:
	  add -p switch to export cmd in Xsession's csh hack

2007-11-15 17:00 +0000 [r737152]  ossi

	* branches/KDE/3.5/kdebase/kdm/backend/session.c: backport: do not
	  auto-login again after resuming from console mode

2007-11-16 12:30 +0000 [r737523]  ossi

	* branches/KDE/3.5/kdebase/kcontrol/kdm/kdm-appear.cpp: backport:
	  sort guiStyle and colorScheme combos

2007-11-16 15:10 +0000 [r737569]  ossi

	* branches/KDE/3.5/kdebase/kdm/kfrontend/kdm_config.c: backport
	  r678811: minor fixes

2007-11-16 15:24 +0000 [r737570]  ossi

	* branches/KDE/3.5/kdebase/kcontrol/kdm/kdm-users.cpp: backport
	  r732248: you can't assign a user pic to a group ...

2007-11-21 10:50 +0000 [r739585]  ossi

	* branches/KDE/3.5/kdebase/kdm/backend/session.c,
	  branches/KDE/3.5/kdebase/kdm/backend/client.c: backport: SIGTERM
	  handling fixes. BUG: 152637

2007-11-22 12:16 +0000 [r740044]  pley

	* branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.h,
	  branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.cpp:
	  - Fixed a small memleak: use libhal_free_string_array(volumes)
	  instead of free(volumes) - USB floppy drives have media-detection
	  enabled. Thus we can react on insertion of a floppy disk - Fixed
	  empty labels for floppy drives

2007-11-22 17:55 +0000 [r740167]  dfaure

	* branches/KDE/3.5/kdebase/kioslave/home/homeimpl.h,
	  branches/KDE/3.5/kdebase/kioslave/home/kio_home.cpp:
	  m_lastErrorCode was used uninitialized, leading to "error
	  -12386136" when testing bug 133235 by opening system:/ and moving
	  trash into "users folders"

2007-11-28 14:46 +0000 [r742659]  lunakl

	* branches/KDE/3.5/kdebase/kwin/geometry.cpp: Ensure forced
	  position is really enforced.

2007-12-07 15:19 +0000 [r746023-746021]  ossi

	* branches/KDE/3.5/kdebase/kdm/kfrontend/kgreeter.cpp: backport:
	  make face loading more robust (less so than the qt4 version due
	  to image loader limitations).

	* branches/KDE/3.5/kdebase/kdm/backend/inifile.c: backport: make
	  .dmrc loading more robust

2007-12-10 16:54 +0000 [r746928-746927]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/viewer/nsplugin.cpp: Calling
	  delete on QByteArray? Interesting, but a bit crashing.

	* branches/KDE/3.5/kdebase/nsplugins/viewer/nsplugin.cpp: Without
	  debug output.

2007-12-13 15:04 +0000 [r748081]  lunakl

	* branches/KDE/3.5/kdebase/khotkeys/shared/actions.cpp: Propagate
	  the session manager when launching apps (kded doesn't have
	  $SESSION_MANAGER set).

2007-12-14 17:30 +0000 [r748513]  lunakl

	* branches/KDE/3.5/kdebase/kwin/group.cpp: Prevent transient loops
	  also when caused by 'this' (which doesn't have the new value set
	  yet). BUG: 153360

2007-12-15 17:44 +0000 [r748841]  ossi

	* branches/KDE/3.5/kdebase/kdmlib/kgreet_classic.cpp,
	  branches/KDE/3.5/kdebase/kdmlib/kgreet_winbind.cpp: backport:
	  select the password when user was clicked, so overwriting it is
	  simpler.

2007-12-20 13:10 +0000 [r750897]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/viewer/viewer.cpp,
	  branches/KDE/3.5/kdebase/nsplugins/viewer/NSPluginClassIface.h,
	  branches/KDE/3.5/kdebase/nsplugins/viewer/glibevents.cpp (added),
	  branches/KDE/3.5/kdebase/nsplugins/viewer/nsplugin.h,
	  branches/KDE/3.5/kdebase/nsplugins/nspluginloader.cpp,
	  branches/KDE/3.5/kdebase/nsplugins/sdk/npruntime.h (added),
	  branches/KDE/3.5/kdebase/nsplugins/sdk/npupp.h,
	  branches/KDE/3.5/kdebase/nsplugins/viewer/glibevents.h (added),
	  branches/KDE/3.5/kdebase/nsplugins/viewer/Makefile.am,
	  branches/KDE/3.5/kdebase/nsplugins/nspluginloader.h,
	  branches/KDE/3.5/kdebase/nsplugins/sdk/npapi.h,
	  branches/KDE/3.5/kdebase/nsplugins/sdk/prcpucfg.h,
	  branches/KDE/3.5/kdebase/nsplugins/viewer/nsplugin.cpp: Support
	  for XEmbed-based plugins and Glib2-based eventloop. Should make
	  the most recent Flash work, if you get sufficiently lucky and
	  don't run into any of the load of its bugs. Latest
	  kdelibs/kdeui/qxembed.* needed as well. CCBUG: 132138

2007-12-21 14:04 +0000 [r751255]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/nspluginloader.cpp,
	  branches/KDE/3.5/kdebase/nsplugins/nspluginloader.h: More feeble
	  attempts at Flash hacks (plus again QXEmbed update). CCBUG:
	  132138

2007-12-21 14:11 +0000 [r751257]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/sdk/npupp.h,
	  branches/KDE/3.5/kdebase/nsplugins/sdk/npapi.h: Get rid of the
	  annoying $Revision$ thing.

2007-12-21 17:05 +0000 [r751317]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/sdk/jni_md.h: One more file
	  from Firefox that is more license-friendly.

2007-12-23 09:25 +0000 [r751991]  mlaurent

	* branches/KDE/3.5/kdebase/kcontrol/kio/cache.cpp: Forward port.
	  Don't emit changed true when anything is changed

2007-12-31 01:18 +0000 [r754981]  kkofler

	* branches/KDE/3.5/kdebase/kcontrol/krdb/krdb.cpp: Fix createGtkrc
	  to set tooltip colors also for GTK+ 2.12+. (backport rev 754979
	  from trunk)

2008-01-01 18:45 +0000 [r755500]  aacid

	* branches/KDE/3.5/kdebase/l10n/cy/entry.desktop,
	  branches/KDE/3.5/kdebase/l10n/mt/entry.desktop: Malta and Cyprus
	  use Euro as of today.
	  http://en.wikipedia.org/wiki/Economic_and_Monetary_Union_of_the_European_Union

2008-01-04 19:07 +0000 [r757345]  lunakl

	* branches/KDE/3.5/kdebase/kcontrol/kcontrol/modules.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/kcontrol/modules.h: Do not put
	  kcontrol's own widgets inside of QXEmbed. BUG: 155001 CCBUG:
	  132138

2008-01-07 16:04 +0000 [r758319]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/viewer/qxteventloop.cpp: Fix
	  64bit problem. BUG: 154713

2008-01-09 16:52 +0000 [r758931]  bero

	* branches/KDE/3.5/kdebase/nsplugins/sdk/prcpucfg.h: Don't redefine
	  BITS_PER_LONG if it's already defined, broke build with gcc 4.3
	  on systems that have a BITS_PER_LONG definition in their standard
	  headers

2008-01-13 17:59 +0000 [r760922]  orlovich

	* branches/KDE/3.5/kdebase/kcontrol/input/mouse.cpp: Backport fix
	  for left-handed mice and recent X.. BUG: 150361

2008-01-15 15:07 +0000 [r761885]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/viewer/resolve.h: Fix debug
	  macro.

2008-01-15 17:26 +0000 [r761919]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/viewer/NSPluginClassIface.h,
	  branches/KDE/3.5/kdebase/nsplugins/viewer/nsplugin.h,
	  branches/KDE/3.5/kdebase/nsplugins/NSPluginCallbackIface.h,
	  branches/KDE/3.5/kdebase/nsplugins/viewer/nsplugin.cpp: Make all
	  types in all DCOP interfaces to be fixed-width types, so that
	  mixing 64b Konqueror with 32b nspluginviewer works. CCBUG: 132138

2008-01-17 11:28 +0000 [r762606]  dfaure

	* branches/KDE/3.5/kdepim/akregator/src/articlelistview.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/fonts/fonts.cpp: Don't create
	  static QPixmaps in libraries/DSOs; it requires the one who
	  dlopens the DSO to provide a QApplication with GUI enabled. This
	  crashes nspluginscan when it is asked to scan $KDEDIR/lib/kde3
	  CCBUG: 139042

2008-01-18 13:20 +0000 [r763066]  lunakl

	* branches/KDE/3.5/kdebase/kicker/kicker/core/container_extension.cpp:
	  Set different window role for each panel - this allows
	  controlling them using kwin's window-specific settings. CCBUG:
	  79531

2008-01-19 21:25 +0000 [r763599]  lunakl

	* branches/KDE/3.5/kdebase/kwin/layers.cpp: Fix fullscreen on
	  youtube with latest flash. Requiring the fullscreen window to be
	  focusable is probably unnecessary and this was breaking because
	  of the skiptasbar flag.

2008-01-21 13:54 +0000 [r764342]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/viewer/NSPluginClassIface.h,
	  branches/KDE/3.5/kdebase/nsplugins/viewer/nsplugin.h,
	  branches/KDE/3.5/kdebase/nsplugins/nspluginloader.cpp,
	  branches/KDE/3.5/kdebase/nsplugins/viewer/qxteventloop.cpp,
	  branches/KDE/3.5/kdebase/nsplugins/nspluginloader.h,
	  branches/KDE/3.5/kdebase/nsplugins/viewer/nsplugin.cpp: Make
	  keyboard work also with XEmbed - there are no real X focus events
	  with XEmbed, so the XQueryKeymap() protection protected a bit too
	  much. The focus messages from QXEmbed are received only by the
	  plugin itself due to the way XSendEvent() is used, so forward
	  focus in/out events from the embedder side.

2008-01-22 15:53 +0000 [r764788]  lunakl

	* branches/KDE/3.5/kdebase/drkonqi/krashconf.h,
	  branches/KDE/3.5/kdebase/drkonqi/debugger.cpp: Suggest a default
	  filename in case the user can't type for whatever reason. BUG:
	  142138

2008-01-31 21:50 +0000 [r769223]  ossi

	* branches/KDE/3.5/kdebase/kdm/backend/client.c: backport^2: reset
	  sigint handler after when we sure have no ctty any more.

2008-02-01 22:20 +0000 [r769726]  darafei

	* branches/KDE/3.5/kdebase/l10n/by/flag.png: Change flag to
	  official flag of the Republic of Belarus.

2008-02-05 20:30 +0000 [r771395]  dfaure

	* branches/KDE/3.5/kdebase/libkonq/konq_pixmapprovider.cc: This
	  warning is the last sore point in kdebase-3.5.8, makes konq look
	  bad to end users :)

2008-02-06 22:02 +0000 [r771783]  fabo

	* branches/KDE/3.5/kdebase/ksysguard/ksysguardd/Linux/lmsensors.c:
	  Add lmsensors 3.x support. backport from 4.x tree.

2008-02-07 15:52 +0000 [r772036]  lunakl

	* branches/KDE/3.5/kdebase/kcontrol/ebrowsing/plugins/localdomain/klocaldomainurifilterhelper.c:
	  Extend the given local name to the same fully qualified name,
	  even if the name is an alias (bnc#359563).

2008-02-11 12:17 +0000 [r773568]  lunakl

	* branches/KDE/3.5/kdebase/kwin/events.cpp: Disable resetting focus
	  from root window as it seems to cause focus problems with
	  multihead for an unknown reason. CCBUG: 157219

2008-02-13 09:48 +0000 [r774448-774447]  coolo

	* branches/KDE/3.5/kdebase/konqueror/version.h: 3.5.9

	* branches/KDE/3.5/kdebase/startkde: 3.5.9

2008-02-13 09:52 +0000 [r774465]  coolo

	* branches/KDE/3.5/kdebase/kdebase.lsm: 3.5.9

