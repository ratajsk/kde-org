------------------------------------------------------------------------
r1043604 | asimha | 2009-11-01 22:59:20 +0000 (Sun, 01 Nov 2009) | 18 lines

Backporting fix for missing stars (r1043599) to 4.3 branch

Original commit log:
--------------------
Ensure that stars with unspecified spectral types / partially
specified spectral types are drawn as well. For the time being, all
stars with undefined spectral classes are drawn with the colors
corresponding to spectral class 'A'.

This fixes the bug that Lukas Middendorf brought up on the mailing
list, regarding stars in M 29.

Many thanks to Lukas, who not only pointed out this hard-to-catch bug,
but also gave me enough information to swat it dead real easy!

CCMAIL: kstars-devel@kde.org


------------------------------------------------------------------------
r1045707 | sengels | 2009-11-06 16:57:22 +0000 (Fri, 06 Nov 2009) | 1 line

backport r1045699
------------------------------------------------------------------------
r1045723 | sengels | 2009-11-06 17:25:56 +0000 (Fri, 06 Nov 2009) | 1 line

fix stuff that was accidentally committed
------------------------------------------------------------------------
r1047250 | annma | 2009-11-10 20:04:52 +0000 (Tue, 10 Nov 2009) | 2 lines

add Kannada keyboard layout thanks to Praveena :)

------------------------------------------------------------------------
r1047494 | lueck | 2009-11-11 10:57:31 +0000 (Wed, 11 Nov 2009) | 2 lines

add missing i18n call, backport from trunk rev 1047492
BUG:197724
------------------------------------------------------------------------
r1049435 | nienhueser | 2009-11-15 09:38:09 +0000 (Sun, 15 Nov 2009) | 5 lines

Do not search for the same term more than once in a row. Avoids duplicated
result entries and crashes. Backport of commit 1048810
BUG: 210013
Please follow up in https://bugs.kde.org/show_bug.cgi?id=206534 for crashes related to multiple search queries running at the same time.

------------------------------------------------------------------------
r1049442 | nienhueser | 2009-11-15 09:41:56 +0000 (Sun, 15 Nov 2009) | 3 lines

Remove repeated (every minute) useless debug output. Backport of commit 1046166
BUG: 184033

------------------------------------------------------------------------
r1049584 | nienhueser | 2009-11-15 14:13:39 +0000 (Sun, 15 Nov 2009) | 3 lines

Do not assume fonts are arbitrarily scalable. Backport of commit 1049582
BUG: 189633

------------------------------------------------------------------------
r1049668 | nienhueser | 2009-11-15 17:04:23 +0000 (Sun, 15 Nov 2009) | 5 lines

Forward available altitude values to the UI.
Altitude needs to be set here as well, otherwise the speed estimation gets confused by seemingly changing positions. Backport of commit 1048806 and 1049655
BUG: 202162


------------------------------------------------------------------------
