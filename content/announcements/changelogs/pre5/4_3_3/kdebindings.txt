------------------------------------------------------------------------
r1030961 | sebsauer | 2009-10-03 15:56:22 +0000 (Sat, 03 Oct 2009) | 7 lines

backport r1030960 from trunk to 4.3 branch;

fix UTF-8 python strings encoding
Patch by Daniel Calviño Sánchez
BUG:209046


------------------------------------------------------------------------
