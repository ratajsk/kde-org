------------------------------------------------------------------------
r1188014 | rkcosta | 2010-10-21 15:54:31 +1300 (Thu, 21 Oct 2010) | 8 lines

Backport r1184981.

Fix build on DragonFly

Patch by Alex Hornung.

CCBUG: 247643

------------------------------------------------------------------------
r1188015 | scripty | 2010-10-21 15:54:44 +1300 (Thu, 21 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1188781 | scripty | 2010-10-23 16:11:30 +1300 (Sat, 23 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
