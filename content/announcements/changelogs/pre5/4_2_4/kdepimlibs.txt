------------------------------------------------------------------------
r961882 | mueller | 2009-04-30 21:11:38 +0000 (Thu, 30 Apr 2009) | 2 lines

update version number for 4.2.3

------------------------------------------------------------------------
r961940 | scripty | 2009-05-01 07:36:46 +0000 (Fri, 01 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r961965 | vkrause | 2009-05-01 09:42:00 +0000 (Fri, 01 May 2009) | 4 lines

Backport SVN commit 961959 by vkrause from trunk:

Fix assert due to writing to a read-only KConfigGroup.

------------------------------------------------------------------------
r963977 | smartins | 2009-05-05 19:08:03 +0000 (Tue, 05 May 2009) | 10 lines

Backport r963967 by smartins from trunk to the 4.2 branch:

Fixes two problems with Recurrence::timesInInterval( start, end ) that are only reproducible with events from lotus notes.

timesInInterval() was returning all dates in mRDates and mRDateTimes regardless if they belonged to the interval [start, end].

If the event had rdates but didn't have rrules, the first occurrence (the one specified by dtstart) wasn't returned.

Review: http://reviewboard.kde.org/r/651/

------------------------------------------------------------------------
r965556 | woebbe | 2009-05-09 10:35:49 +0000 (Sat, 09 May 2009) | 5 lines

Free singletons. It was no real leak but made valgrinding harder.

http://reviewboard.kde.org/r/677/

Approved by Volker Krause.
------------------------------------------------------------------------
r966698 | smartins | 2009-05-11 19:04:27 +0000 (Mon, 11 May 2009) | 7 lines

Backport r966325 by smartins from trunk to the 4.2 branch:

Don't do setRelatedTo() when dissociating recurring to-dos, otherwise the new to-do will appear as a child.

From what I was told, it was originaly planned to set a relation with reltype SIBLING when dissociating to-dos, but currently kcal
only supports reltype PARENT.

------------------------------------------------------------------------
r966702 | smartins | 2009-05-11 19:11:13 +0000 (Mon, 11 May 2009) | 6 lines

Backport r965793 by smartins from trunk to the 4.2 branch:

Show the incidence's created time in the user's timezone, not UTC.

Spotted by Christophe Giboudeaux.

------------------------------------------------------------------------
r967206 | smartins | 2009-05-12 21:07:45 +0000 (Tue, 12 May 2009) | 9 lines

Backport r967201 by smartins from trunk to the 4.2 branch:

Fix kcal's html export. Replace i18nc("@info", with i18nc("@info/plain" so the resulting html file doesn't get full of
<html> tags.

Shouldn't give too much work to translators as the actual strings remain untouched.

CCBUG: 192316

------------------------------------------------------------------------
r967440 | tmcguire | 2009-05-13 13:13:09 +0000 (Wed, 13 May 2009) | 25 lines

Backport r967410 by tmcguire from trunk to the 4.2 branch:

Merged revisions 965363 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

................
  r965363 | winterz | 2009-05-08 19:57:47 +0200 (Fri, 08 May 2009) | 15 lines
  
  Merged revisions 965355 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r965355 | winterz | 2009-05-08 13:22:45 -0400 (Fri, 08 May 2009) | 8 lines
    
    To be compatabile with Outlook and since RFC2445 says that the COMMENT property
    is not to be used in processing, we now publish declined invitation comments as
    a DESCRIPTION property instead of COMMENT property.
    
    Also included with this patch is a nicer formatting of the emailed decline response.
    
    fixes kolab/issue3424
  ........
................


------------------------------------------------------------------------
r968125 | winterz | 2009-05-15 01:08:11 +0000 (Fri, 15 May 2009) | 4 lines

backport SVN commit 968124 by winterz:

Add crash guards to the period rounding calcuations if the recurrence frequency is 0.

------------------------------------------------------------------------
r968155 | scripty | 2009-05-15 07:44:23 +0000 (Fri, 15 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r970519 | tmcguire | 2009-05-20 11:53:07 +0000 (Wed, 20 May 2009) | 16 lines

Backport r970502 by tmcguire from trunk to the 4.2 branch:

Merged revisions 967747 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r967747 | winterz | 2009-05-13 23:41:55 +0200 (Wed, 13 May 2009) | 6 lines
  
  The Description: and Comments: fields of the invitationsDetailsIncidence() can
  be ugly if the description() is empty, since string2HTML() always returns
  at least the "<p>".  So deal with that situation.
  
  MERGE: trunk,4.2
........


------------------------------------------------------------------------
r970520 | tmcguire | 2009-05-20 11:55:05 +0000 (Wed, 20 May 2009) | 12 lines

Backport r970503 by tmcguire from trunk to the 4.2 branch:

Merged revisions 969620 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r969620 | winterz | 2009-05-18 17:21:21 +0200 (Mon, 18 May 2009) | 2 lines
  
  remove a kDebug statement.
........


------------------------------------------------------------------------
r974224 | mueller | 2009-05-28 19:15:17 +0000 (Thu, 28 May 2009) | 2 lines

4.2.4 preparations

------------------------------------------------------------------------
