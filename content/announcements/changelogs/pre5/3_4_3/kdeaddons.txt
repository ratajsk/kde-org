2005-07-20 21:02 +0000 [r437067]  callegari

	* branches/KDE/3.4/kdeaddons/kate/cppsymbolviewer/plugin_katesymbolviewer.cpp,
	  branches/KDE/3.4/kdeaddons/kate/cppsymbolviewer/plugin_katesymbolviewer.h,
	  branches/KDE/3.4/kdeaddons/kate/cppsymbolviewer/fortran_parser.cpp
	  (added),
	  branches/KDE/3.4/kdeaddons/kate/cppsymbolviewer/Makefile.am:
	  Added Fortran parser. Thanks to Roberto Quitiliani

2005-07-25 09:38 +0000 [r438533]  sngeorgaras

	* branches/KDE/3.4/kdeaddons/kfile-plugins/mhtml/kfile_mhtml.cpp:
	  fixing an infinite loop bug

2005-08-29 22:59 +0000 [r454799]  sngeorgaras

	* branches/KDE/3.4/kdeaddons/kfile-plugins/mhtml/kfile_mhtml.cpp:
	  starting value for bool canUnfold

2005-09-07 19:50 +0000 [r458301]  osterfeld

	* branches/KDE/3.4/kdeaddons/konq-plugins/akregator/feeddetector.cpp:
	  backport of 112146: resolve entities in feed titles CCBUG: 112146

2005-10-05 13:46 +0000 [r467511]  coolo

	* branches/KDE/3.4/kdeaddons/kdeaddons.lsm: 3.4.3

