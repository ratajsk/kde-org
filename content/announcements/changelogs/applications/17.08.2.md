---
aliases:
- ../../fulllog_applications-17.08.2
hidden: true
title: KDE Applications 17.08.2 Full Log Page
type: fulllog
version: 17.08.2
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Fix cancelTask() for retrieveItems(). <a href='http://commits.kde.org/akonadi/b5f34f2abab0b5c70fff95ee0c155a39d48dc1ad'>Commit.</a> </li>
<li>akonadictl: fsck and vacuum require running Akonadi. <a href='http://commits.kde.org/akonadi/aebf8a1b727c81cb317cebf712dc19a8ec3e5944'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361542'>#361542</a></li>
<li>Introduce MimeTypeChecker::hasWantedMimeTypes. <a href='http://commits.kde.org/akonadi/fc8db03b57f279397a8284722fe859b45155231e'>Commit.</a> </li>
<li>Only remove init connections to the database on server shutdown. <a href='http://commits.kde.org/akonadi/b145f47f000978b9d39edc1882849ec7f6b3ef79'>Commit.</a> See bug <a href='https://bugs.kde.org/383991'>#383991</a></li>
<li>LIST: correctly return mimetypes for all Collections. <a href='http://commits.kde.org/akonadi/97a191a5df7b307c70e662996c4846d3d586ff61'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350219'>#350219</a></li>
</ul>
<h3><a name='akonadi-calendar' href='https://cgit.kde.org/akonadi-calendar.git'>akonadi-calendar</a> <a href='#akonadi-calendar' onclick='toggle("ulakonadi-calendar", this)'>[Hide]</a></h3>
<ul id='ulakonadi-calendar' style='display: block'>
<li>Use identity settings. <a href='http://commits.kde.org/akonadi-calendar/aaaa2b1fbe076f8f6871e162d2f08d25ab1c752b'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-contacts' href='https://cgit.kde.org/akonadi-contacts.git'>akonadi-contacts</a> <a href='#akonadi-contacts' onclick='toggle("ulakonadi-contacts", this)'>[Hide]</a></h3>
<ul id='ulakonadi-contacts' style='display: block'>
<li>We don't want to edit treeview. <a href='http://commits.kde.org/akonadi-contacts/3eba7264a701c49d62e41a035ac9118da98cd423'>Commit.</a> </li>
<li>EmailAddressSelectionWidget: use ETM signal to expand, rather than 1s timer. <a href='http://commits.kde.org/akonadi-contacts/bf7800974ed42188ebb00f28128727d08c3b8273'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-search' href='https://cgit.kde.org/akonadi-search.git'>akonadi-search</a> <a href='#akonadi-search' onclick='toggle("ulakonadi-search", this)'>[Hide]</a></h3>
<ul id='ulakonadi-search' style='display: block'>
<li>Add missing signal. <a href='http://commits.kde.org/akonadi-search/d6ec367febbf5ac55e5d17f9b23cbbba3bc66485'>Commit.</a> </li>
</ul>
<h3><a name='akregator' href='https://cgit.kde.org/akregator.git'>akregator</a> <a href='#akregator' onclick='toggle("ulakregator", this)'>[Hide]</a></h3>
<ul id='ulakregator' style='display: block'>
<li>Do not remove read messages from the unread filter immediately. <a href='http://commits.kde.org/akregator/c2d31a56f0d45858706e9a0af9227a24f8e55483'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350731'>#350731</a></li>
<li>When backing up feeds.opml, remove any existing backup first. <a href='http://commits.kde.org/akregator/be2e169f80df7fb85298b569abf0bbcfa88bf95d'>Commit.</a> </li>
<li>Make sure part is created before main windows is restored. <a href='http://commits.kde.org/akregator/d0a5f4159cddcca656ca8bbcbd6e551e3499c166'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381822'>#381822</a>. Fixes bug <a href='https://bugs.kde.org/378513'>#378513</a>. Fixes bug <a href='https://bugs.kde.org/381825'>#381825</a>. Fixes bug <a href='https://bugs.kde.org/377129'>#377129</a></li>
</ul>
<h3><a name='baloo-widgets' href='https://cgit.kde.org/baloo-widgets.git'>baloo-widgets</a> <a href='#baloo-widgets' onclick='toggle("ulbaloo-widgets", this)'>[Hide]</a></h3>
<ul id='ulbaloo-widgets' style='display: block'>
<li>Use the correct defines for a Qt 5 world. <a href='http://commits.kde.org/baloo-widgets/ad29ff51d62940c11d503f010dfc1f1ba46d82ae'>Commit.</a> </li>
</ul>
<h3><a name='blogilo' href='https://cgit.kde.org/blogilo.git'>blogilo</a> <a href='#blogilo' onclick='toggle("ulblogilo", this)'>[Hide]</a></h3>
<ul id='ulblogilo' style='display: block'>
<li>Forgot to update version. <a href='http://commits.kde.org/blogilo/337457bf8669b90b82a6c21553c791e96bd99741'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Fix build with Qt 5.10. <a href='http://commits.kde.org/dolphin/c96efc7ccba67a48920713a2d2b0461c747ca3fb'>Commit.</a> </li>
<li>Keep renamed file(s) in view. <a href='http://commits.kde.org/dolphin/478f404b8abf924a0e3e21bbf1dd49aefbe47672'>Commit.</a> See bug <a href='https://bugs.kde.org/354330'>#354330</a></li>
<li>Make sure we always have Shift+Del as shortcut. <a href='http://commits.kde.org/dolphin/cdd002c57cde0480e6e02c7942e9b92af9d0a3e7'>Commit.</a> </li>
<li>Qt 5 Porting: Q_WS_WIN -> Q_OS_WIN. <a href='http://commits.kde.org/dolphin/0006e9997e86f5522375dc082d890e0c5710a0a2'>Commit.</a> </li>
</ul>
<h3><a name='eventviews' href='https://cgit.kde.org/eventviews.git'>eventviews</a> <a href='#eventviews' onclick='toggle("uleventviews", this)'>[Hide]</a></h3>
<ul id='uleventviews' style='display: block'>
<li>Make sure that agenda is not null. <a href='http://commits.kde.org/eventviews/0330ab1ac26f7be7b3ce92eccf956d21b1b67c9f'>Commit.</a> See bug <a href='https://bugs.kde.org/384324'>#384324</a></li>
</ul>
<h3><a name='filelight' href='https://cgit.kde.org/filelight.git'>filelight</a> <a href='#filelight' onclick='toggle("ulfilelight", this)'>[Hide]</a></h3>
<ul id='ulfilelight' style='display: block'>
<li>Fix AUTOMOC warning. <a href='http://commits.kde.org/filelight/2887bf99a08e65bef51ebb6efa04c3586abbd544'>Commit.</a> </li>
<li>Require kdewin on Windows. <a href='http://commits.kde.org/filelight/6695066a1a2074a5d4a08cbafb5fa2c014753feb'>Commit.</a> </li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Fix icon colors of inline context buttons also in full screen mode. <a href='http://commits.kde.org/gwenview/46e95531c731bd0a3a325ad5fc178e5256d29f0c'>Commit.</a> See bug <a href='https://bugs.kde.org/383059'>#383059</a></li>
<li>Use standard QToolButtons so that their icons use the right colors. <a href='http://commits.kde.org/gwenview/b09b216fe7b404dfc12f78834c38f4a0793a5d4d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383059'>#383059</a></li>
<li>Gwenview: suppressed unused interface variables compiler warnings. <a href='http://commits.kde.org/gwenview/62f9183f93022c47d895ca5aa103a617b54f680c'>Commit.</a> </li>
<li>Fix Gwenview Importer doesn't use date/time from EXIF. <a href='http://commits.kde.org/gwenview/0d5915a10d5ce8e6492483bf209e654d52d8b846'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383520'>#383520</a></li>
<li>Reduce hyper-sensitive touchpad scroll-zoom speed. <a href='http://commits.kde.org/gwenview/c4a84dad6292b9c7918bec8d7ed33114d3758730'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378584'>#378584</a></li>
<li>Add 16:9 crop preset to Gwenview. <a href='http://commits.kde.org/gwenview/a930fb5452020ad1b648cf5eb96576df5561cabb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364007'>#364007</a></li>
</ul>
<h3><a name='incidenceeditor' href='https://cgit.kde.org/incidenceeditor.git'>incidenceeditor</a> <a href='#incidenceeditor' onclick='toggle("ulincidenceeditor", this)'>[Hide]</a></h3>
<ul id='ulincidenceeditor' style='display: block'>
<li>We have an identity but we don't use settings as sent folder. <a href='http://commits.kde.org/incidenceeditor/3e5bf6ae3be25ba7fcf13feb96996bc38748c614'>Commit.</a> </li>
</ul>
<h3><a name='kcachegrind' href='https://cgit.kde.org/kcachegrind.git'>kcachegrind</a> <a href='#kcachegrind' onclick='toggle("ulkcachegrind", this)'>[Hide]</a></h3>
<ul id='ulkcachegrind' style='display: block'>
<li>Fix compare functions used by std::sort. <a href='http://commits.kde.org/kcachegrind/afb704ba99b0eca12b0f1bced1bd454c7cfbea7f'>Commit.</a> </li>
</ul>
<h3><a name='kcalcore' href='https://cgit.kde.org/kcalcore.git'>kcalcore</a> <a href='#kcalcore' onclick='toggle("ulkcalcore", this)'>[Hide]</a></h3>
<ul id='ulkcalcore' style='display: block'>
<li>Remove duplicate QT_REQUIRED_VERSION from autotests. <a href='http://commits.kde.org/kcalcore/84892b60bea07ce5deea2e74227f70d4b810be65'>Commit.</a> </li>
</ul>
<h3><a name='kcalutils' href='https://cgit.kde.org/kcalutils.git'>kcalutils</a> <a href='#kcalutils' onclick='toggle("ulkcalutils", this)'>[Hide]</a></h3>
<ul id='ulkcalutils' style='display: block'>
<li>Fix a whole bunch of IncidenceFormatter unit tests. <a href='http://commits.kde.org/kcalutils/b76972cfab5b9630110e8e3d3b86f730bbd6c3ee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383890'>#383890</a></li>
</ul>
<h3><a name='kde-dev-utils' href='https://cgit.kde.org/kde-dev-utils.git'>kde-dev-utils</a> <a href='#kde-dev-utils' onclick='toggle("ulkde-dev-utils", this)'>[Hide]</a></h3>
<ul id='ulkde-dev-utils' style='display: block'>
<li>Remove unused items from kuiviewer_part.rc. <a href='http://commits.kde.org/kde-dev-utils/79adc315201c10e4b2c2b6ba74e1c3b152626fcc'>Commit.</a> </li>
<li>Use non-deprecated <gui> as root element in ui.rc file. <a href='http://commits.kde.org/kde-dev-utils/ab93ff1748f0f3112e7a8b3f0dbb24604de11d70'>Commit.</a> </li>
</ul>
<h3><a name='kdebugsettings' href='https://cgit.kde.org/kdebugsettings.git'>kdebugsettings</a> <a href='#kdebugsettings' onclick='toggle("ulkdebugsettings", this)'>[Hide]</a></h3>
<ul id='ulkdebugsettings' style='display: block'>
<li>Add missing category. <a href='http://commits.kde.org/kdebugsettings/2610eda65363974f4e137613e4d5b3dc4756266e'>Commit.</a> </li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Hide]</a></h3>
<ul id='ulkdelibs' style='display: block'>
<li>Fix build with >=enchant-2. <a href='http://commits.kde.org/kdelibs/668ef94b2b861f7ec4aa20941bcb6493bc4367be'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Fix autosave: work around KAutoSaveFile bug with non-ASCII chars. <a href='http://commits.kde.org/kdenlive/630f7bbfcf7fed53de85b3bccc2051da13391639'>Commit.</a> </li>
<li>Get ready for transform centered rotation. <a href='http://commits.kde.org/kdenlive/529e7a235b271430ddb3d3a7c594fe5b15d2495c'>Commit.</a> </li>
<li>Fix keyframes unseekable on bin effects. <a href='http://commits.kde.org/kdenlive/f4c9a045f281d11faa5c797263241c3343ce20d8'>Commit.</a> </li>
<li>Fix lift to handle negative values (requires latest MLT version). <a href='http://commits.kde.org/kdenlive/4a7088e8b052b3818b5be7e45a0b779e38a62bfb'>Commit.</a> </li>
<li>Prefer SDL2 to SDL1 (dropped by FFmpeg and so MLT). <a href='http://commits.kde.org/kdenlive/04541ba0dd0d11a510e0c252292c77df3a794109'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>Fix memory leak and crash in Plasma events plugin configuration. <a href='http://commits.kde.org/kdepim-addons/6b88cd02467e97ba9110f39e8f52b584fb9bdc12'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376220'>#376220</a></li>
<li>Allow to check all features. <a href='http://commits.kde.org/kdepim-addons/759deb9468b6ed1f387fbdf81df57093b34bc168'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-apps-libs' href='https://cgit.kde.org/kdepim-apps-libs.git'>kdepim-apps-libs</a> <a href='#kdepim-apps-libs' onclick='toggle("ulkdepim-apps-libs", this)'>[Hide]</a></h3>
<ul id='ulkdepim-apps-libs' style='display: block'>
<li>Use parent directly. <a href='http://commits.kde.org/kdepim-apps-libs/41bf720e8d91f03ccbc7e79051ac49a671703a50'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>Fix i18n. <a href='http://commits.kde.org/kdepim-runtime/8fddc00348b3c1161792a1201eba85bed79ac437'>Commit.</a> </li>
<li>DAV: fix crash if Collection disappears during Item fetch. <a href='http://commits.kde.org/kdepim-runtime/a86b0808c1d6553419aa78b7e104d820991c7080'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360792'>#360792</a></li>
</ul>
<h3><a name='kdialog' href='https://cgit.kde.org/kdialog.git'>kdialog</a> <a href='#kdialog' onclick='toggle("ulkdialog", this)'>[Hide]</a></h3>
<ul id='ulkdialog' style='display: block'>
<li>Add support for multiple name filters. <a href='http://commits.kde.org/kdialog/1a3917b67c4c5cf0bdb5a888e2e5d3c8c4edce10'>Commit.</a> </li>
<li>Bump version to 2.0, the kf5 port changed some things. <a href='http://commits.kde.org/kdialog/0abc493d0d31e20696855dd6166d3c4016ad34b4'>Commit.</a> </li>
</ul>
<h3><a name='kleopatra' href='https://cgit.kde.org/kleopatra.git'>kleopatra</a> <a href='#kleopatra' onclick='toggle("ulkleopatra", this)'>[Hide]</a></h3>
<ul id='ulkleopatra' style='display: block'>
<li>Ensure parentWidget is set for autodetection cmds. <a href='http://commits.kde.org/kleopatra/e3c12924cb1b234aac2e2993def4e42d03ea547e'>Commit.</a> </li>
<li>Fix crash when import is called without parent. <a href='http://commits.kde.org/kleopatra/8a1e8f47aeb8f8118ae17d068582f02e5e3a5818'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Add kconf_update script to adapt to .desktop name change in 17.08. <a href='http://commits.kde.org/kmail/a1704fc888ce729b0a45cc10a5852a3301e5c488'>Commit.</a> </li>
<li>Remove element not index. Now progress dialog is closed when we indexed all collections. <a href='http://commits.kde.org/kmail/73bfeb98971f5a81c1b4e4ac058332ffff65f2fe'>Commit.</a> </li>
<li>Don't eat each key event. <a href='http://commits.kde.org/kmail/fa0a2ec55cc952eea3e9f33f17c0192222fbf873'>Commit.</a> </li>
<li>Update title when we switch folder. <a href='http://commits.kde.org/kmail/934ce6e21e4b0514859acce50c48484ed5cb4aee'>Commit.</a> </li>
</ul>
<h3><a name='kompare' href='https://cgit.kde.org/kompare.git'>kompare</a> <a href='#kompare' onclick='toggle("ulkompare", this)'>[Hide]</a></h3>
<ul id='ulkompare' style='display: block'>
<li>Fix missing component data of KomparePart. <a href='http://commits.kde.org/kompare/51c2c08fb9f4329c93890d7ad2f524703b946ce1'>Commit.</a> </li>
<li>Only do i18n setup and calls after QApp instance is created. <a href='http://commits.kde.org/kompare/ddf3cafd83458b25f9a3a537cc5b587302098671'>Commit.</a> </li>
<li>Fix missing setting of translationDomain. <a href='http://commits.kde.org/kompare/048739048b989f1e4965360c52e44213c9412c52'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Ignore xterm-DCS messages. <a href='http://commits.kde.org/konsole/f3ddd3ef301d3c1e4f51405bf9a6afdc897edd8a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383976'>#383976</a></li>
</ul>
<h3><a name='kontact' href='https://cgit.kde.org/kontact.git'>kontact</a> <a href='#kontact' onclick='toggle("ulkontact", this)'>[Hide]</a></h3>
<ul id='ulkontact' style='display: block'>
<li>Restore the full help menu for Kontact. <a href='http://commits.kde.org/kontact/a5a8a394ed4fe65ef38a00c67d8f0876a952d121'>Commit.</a> </li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Hide]</a></h3>
<ul id='ulkopete' style='display: block'>
<li>Fix the name of an image (wrongly renamed in the past). <a href='http://commits.kde.org/kopete/0f1ccb3e52a67eb08aaa60b66442bbb996aaad54'>Commit.</a> </li>
</ul>
<h3><a name='korganizer' href='https://cgit.kde.org/korganizer.git'>korganizer</a> <a href='#korganizer' onclick='toggle("ulkorganizer", this)'>[Hide]</a></h3>
<ul id='ulkorganizer' style='display: block'>
<li>It's an enum not a class. Don't use const'ref. <a href='http://commits.kde.org/korganizer/962a0c043b096c910751132230ca21faaf9cf1c0'>Commit.</a> </li>
<li>No need to consider layout direction on Elide setting. <a href='http://commits.kde.org/korganizer/819fae525261049ce824fb97c73ba08020783936'>Commit.</a> </li>
<li>Set a font in calendardelegate.cpp. <a href='http://commits.kde.org/korganizer/221aa26f17564a8cd313349ccf24cd32f8f7b743'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383722'>#383722</a></li>
<li>Don't print birthdays beyond the days-ahead limit. <a href='http://commits.kde.org/korganizer/10fb4e6d599bacf84496bcef90ea089a55ba4de1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384284'>#384284</a></li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Hide]</a></h3>
<ul id='ulkpimtextedit' style='display: block'>
<li>Fix potential crash. <a href='http://commits.kde.org/kpimtextedit/d483767a3190f594e4cc74230322d82197d3cc40'>Commit.</a> </li>
</ul>
<h3><a name='libkgapi' href='https://cgit.kde.org/libkgapi.git'>libkgapi</a> <a href='#libkgapi' onclick='toggle("ullibkgapi", this)'>[Hide]</a></h3>
<ul id='ullibkgapi' style='display: block'>
<li>Lintcmake https://build.neon.kde.org/job/xenial_stable_applications_libkgapi_lintcmake/21/console Unknown CMake command "find_dependency". <a href='http://commits.kde.org/libkgapi/fea3844702de56f24d864d630cc0bb496716bbbf'>Commit.</a> </li>
<li>Fix version. <a href='http://commits.kde.org/libkgapi/86e6274c9f2540509c37cf4bc9b5a4832c07ec7a'>Commit.</a> </li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Hide]</a></h3>
<ul id='ullibksieve' style='display: block'>
<li>Don't try to rename script with same name. <a href='http://commits.kde.org/libksieve/95f053f59d8be336ede828360401d14db8310cd2'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Hide]</a></h3>
<ul id='ulmailcommon' style='display: block'>
<li>Add parent to qdialogbuttonbox. <a href='http://commits.kde.org/mailcommon/75b65d2d0ff512796d879d6eaa5ff2892708fa8f'>Commit.</a> </li>
<li>Allow to create item even if we didn't select a group. <a href='http://commits.kde.org/mailcommon/6a226d9456a0a84505d7ba836a6e90c043dd227f'>Commit.</a> </li>
<li>Show invalid filter dialog when action is not valid too. <a href='http://commits.kde.org/mailcommon/ca4e1988a097e0b8b415b4313c88ca6473ee0230'>Commit.</a> </li>
</ul>
<h3><a name='mailimporter' href='https://cgit.kde.org/mailimporter.git'>mailimporter</a> <a href='#mailimporter' onclick='toggle("ulmailimporter", this)'>[Hide]</a></h3>
<ul id='ulmailimporter' style='display: block'>
<li>Fix Bug 383073 - importwizard crashed on terminating. <a href='http://commits.kde.org/mailimporter/b17684e11eaf80cbfa0d8a25f474170c8fdd1db0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383073'>#383073</a></li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Hide]</a></h3>
<ul id='ulmarble' style='display: block'>
<li>Do away with the crashes that were triggered by zooming and panning. <a href='http://commits.kde.org/marble/065b4d499e2cdea69124708017b8ad9a9439c4d6'>Commit.</a> </li>
<li>Make m_lastPlacemarkAt a QPointer to avoid crashes. <a href='http://commits.kde.org/marble/08cb69b70fe7bee7763cf5bd51a17995d99a40ef'>Commit.</a> </li>
<li>Fix for "Some countries are invisible on the political map". <a href='http://commits.kde.org/marble/7a20f244b2350a0de6864be3a6cc0317bfd65ac0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381984'>#381984</a></li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Allow to show html source even if we have build as release. <a href='http://commits.kde.org/messagelib/935b2118c7048ac07f4685230e8e423ad1a5a197'>Commit.</a> </li>
<li>Add parent to qdialogbuttonbox. <a href='http://commits.kde.org/messagelib/eca2e4c234c5437126d00de276a81a49ad697936'>Commit.</a> </li>
</ul>
<h3><a name='okteta' href='https://cgit.kde.org/okteta.git'>okteta</a> <a href='#okteta' onclick='toggle("ulokteta", this)'>[Hide]</a></h3>
<ul id='ulokteta' style='display: block'>
<li>Add app=okteta to donation url in appdata. <a href='http://commits.kde.org/okteta/eece52445f2f5b746f1f5501aa5078d67902f4d3'>Commit.</a> </li>
<li>Use edit-delete icon for "delete view profile" action, shred is too much. <a href='http://commits.kde.org/okteta/d711840374d1bc32d540045627b172acacced1f5'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Show tooltip for annotations without handle too. <a href='http://commits.kde.org/okular/e31bea047c4cc7b17cddda333b4012cee383fc21'>Commit.</a> </li>
<li>Properly create KPixmapSequence. <a href='http://commits.kde.org/okular/1364d0e97b22c85742f0bc1c99fc72f0c7abf61c'>Commit.</a> </li>
<li>Fix sidebar labels being unreadable when selected or hovered over. <a href='http://commits.kde.org/okular/e80972e18909ed4f061323d1e5c4d2c8883c4943'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382139'>#382139</a></li>
<li>Fix automatic reload of files saved with QSaveFile. <a href='http://commits.kde.org/okular/bedc3dbeadd450a860770308ffc2ca5538cf42b9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384185'>#384185</a></li>
</ul>
<h3><a name='palapeli' href='https://cgit.kde.org/palapeli.git'>palapeli</a> <a href='#palapeli' onclick='toggle("ulpalapeli", this)'>[Hide]</a></h3>
<ul id='ulpalapeli' style='display: block'>
<li>Really fix appdata install location. <a href='http://commits.kde.org/palapeli/063e98089738bb2f026f0f716c57b171137c1c2c'>Commit.</a> </li>
<li>Fix appdata install location. <a href='http://commits.kde.org/palapeli/7db6c1f5081f65f55ee7da065148b1b15272e22c'>Commit.</a> </li>
</ul>
<h3><a name='pim-sieve-editor' href='https://cgit.kde.org/pim-sieve-editor.git'>pim-sieve-editor</a> <a href='#pim-sieve-editor' onclick='toggle("ulpim-sieve-editor", this)'>[Hide]</a></h3>
<ul id='ulpim-sieve-editor' style='display: block'>
<li>Update script name too. <a href='http://commits.kde.org/pim-sieve-editor/642c8c781116031e27610ed67def3e488084d6ea'>Commit.</a> </li>
</ul>
<h3><a name='pimcommon' href='https://cgit.kde.org/pimcommon.git'>pimcommon</a> <a href='#pimcommon' onclick='toggle("ulpimcommon", this)'>[Hide]</a></h3>
<ul id='ulpimcommon' style='display: block'>
<li>Make sure that we show last text. <a href='http://commits.kde.org/pimcommon/90251adeec160ddbaa27e2d06550d2b1e00a216f'>Commit.</a> </li>
</ul>
<h3><a name='print-manager' href='https://cgit.kde.org/print-manager.git'>print-manager</a> <a href='#print-manager' onclick='toggle("ulprint-manager", this)'>[Hide]</a></h3>
<ul id='ulprint-manager' style='display: block'>
<li>KCupsConnection: Exit authentication loop after too many  retries. <a href='http://commits.kde.org/print-manager/9da623e104ea2dd79041f3608f5f196383441c4c'>Commit.</a> </li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Hide]</a></h3>
<ul id='ulspectacle' style='display: block'>
<li>Fix .arcconfig. <a href='http://commits.kde.org/spectacle/a476dd8eac5a3acaefa422f7c5c0ad7894c7ce16'>Commit.</a> </li>
<li>Hint to drop handlers: We only want to offer copy of data, not move. <a href='http://commits.kde.org/spectacle/652c4c547ac658bfef4d8c4e1d00d52abab31768'>Commit.</a> </li>
<li>Add .arcconfig. <a href='http://commits.kde.org/spectacle/39ba6e4f50ac41a19fa3dff10c63e3883e94eb39'>Commit.</a> </li>
<li>Fix: use autosaveFilename format also for files exported via DnD or menu. <a href='http://commits.kde.org/spectacle/cedfdb40b80aa3ad042b6ef6918ddd3500d84df0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382718'>#382718</a></li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Potential crash fix. <a href='http://commits.kde.org/umbrello/8ca1b046dcf989737c63f7121535d9fd8fa7a216'>Commit.</a> </li>
</ul>