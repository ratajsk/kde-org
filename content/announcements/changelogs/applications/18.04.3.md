---
aliases:
- ../../fulllog_applications-18.04.3
hidden: true
title: KDE Applications 18.04.3 Full Log Page
type: fulllog
version: 18.04.3
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Fix a crash due to access of temporary object. <a href='http://commits.kde.org/akonadi/f6b54fe5e83bf59ad2f21141310405b1f54ddcf2'>Commit.</a> </li>
<li>Fix MariaDB initialization when mysql_install_db is missing. <a href='http://commits.kde.org/akonadi/a3d9f7647bf36a35deb996516ebd84f88976f013'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392948'>#392948</a></li>
<li>AkInit: improve error message when renaming old logfiles fails. <a href='http://commits.kde.org/akonadi/4bd807c28663556c7e84c841d45f2d31803755bb'>Commit.</a> See bug <a href='https://bugs.kde.org/392092'>#392092</a></li>
<li>StorageJanitor: handle external parts DB entries with missing filename. <a href='http://commits.kde.org/akonadi/0d7227f853888730019438f9bbc213d947cb0c02'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395743'>#395743</a></li>
<li>Server: handle race condition on connection shutdown. <a href='http://commits.kde.org/akonadi/154cfb8fc2d2bb92e429523ba62ce90e257f0118'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394071'>#394071</a></li>
<li>ActionStateManager: separate selected collections from favorites. <a href='http://commits.kde.org/akonadi/fd47fa73993d67f2f921409a4ee6eeeba3b36fc4'>Commit.</a> </li>
<li>Document the hidden mechanism by which these methods are called... <a href='http://commits.kde.org/akonadi/00265b23016148a51f89b45434ed9a661bdba183'>Commit.</a> </li>
<li>Resourcebase docu: don't call changeCommitted from itemRemoved. <a href='http://commits.kde.org/akonadi/c48ae5f8ec3cddb633bf6f52898d6f215dda83fe'>Commit.</a> </li>
<li>Fix caption/text. <a href='http://commits.kde.org/akonadi/e928ac86f3e6147d71fa66fdf9474676386f211f'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>Libzip: fix extraction of folders without zip entry. <a href='http://commits.kde.org/ark/9a349f680b1b38332511f4e4b674d736befbad4c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394542'>#394542</a></li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Show]</a></h3>
<ul id='ulcantor' style='display: none'>
<li>Fix displaying commands source code in sage backend. <a href='http://commits.kde.org/cantor/9928df33da6cd06cbfeeac9c1fe34abb5c1957cb'>Commit.</a> </li>
<li>Remove unnecessary empty line in end each text result for R backend. <a href='http://commits.kde.org/cantor/27caadf470a7cf0199f3ab51c105ea14adccc7ed'>Commit.</a> </li>
<li>Remove html output from R backend for vectors. <a href='http://commits.kde.org/cantor/e38edf10e2bbc6f790d1e43d85bce2ad872255bb'>Commit.</a> </li>
<li>Fix starting external sage viewers in Sage backend. <a href='http://commits.kde.org/cantor/35b3ef377f0ebc6650be747ad2e4e7e129276c0d'>Commit.</a> </li>
<li>Fix bug with autocomplete popup in Sage backend. <a href='http://commits.kde.org/cantor/a94e5135d4f4939af5d96678bbe4b4f2cb636927'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395844'>#395844</a></li>
<li>Disable latex output for sage by default, because it don't work very well. <a href='http://commits.kde.org/cantor/ab26a772d15f7c0cab6183ae614b0631df95eef5'>Commit.</a> </li>
<li>Fix broken sage login for Sage versions greater, than 8.0 (8.1, 8.2, 8.3, etc.) and set version 8.1 and 8.2 as recommends. <a href='http://commits.kde.org/cantor/f071930f73d9c73fcf720f4f105a915810c3cb58'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375467'>#375467</a></li>
<li>Use standart %o, %i variables instead of %O, %I in maxima backend. <a href='http://commits.kde.org/cantor/9aa0ed5f7d5ac2007983d702e74241035467b3cd'>Commit.</a> </li>
<li>Add new example worksheet for maxima backend. <a href='http://commits.kde.org/cantor/91be07f570e078a1356ed3e22c013cdea6d4acbc'>Commit.</a> </li>
<li>Avoiding a lot of suggest-override warnings. <a href='http://commits.kde.org/cantor/82c486a24a75c690eb6dca35133b8ef11f26902d'>Commit.</a> </li>
<li>Fix bug with QFileDialog filter for backends, which support scripts extensions (Sage backend, for example, supports python script extension (*.py)). <a href='http://commits.kde.org/cantor/5bf1e249a39b3d2469b56c990beea7fa3ece6b2d'>Commit.</a> </li>
<li>Remove useless qDebug, which don't print anything. <a href='http://commits.kde.org/cantor/977c707643542f1f9df2c5ee6b7f935c1fd4ad44'>Commit.</a> </li>
<li>Fix kf5.kxmlgui problem for qalculate plot assistant. <a href='http://commits.kde.org/cantor/fdb2644c4b2cbafbfdec21373b9dde63ee67ada4'>Commit.</a> </li>
<li>Add worksheet example for plotting in Maxima. <a href='http://commits.kde.org/cantor/e501eead21abc90a2da9a61b9e6c6274747be6bd'>Commit.</a> </li>
<li>Add missing plot command to Maxima backend. <a href='http://commits.kde.org/cantor/193a5174aa7985b96274f7f42a3c388364a5948c'>Commit.</a> </li>
<li>Add worksheet examples for Octave. <a href='http://commits.kde.org/cantor/d23a60b7577c20ee5deb7260e527784f9f038117'>Commit.</a> </li>
<li>Add missing 2d plot command (plotmatrix) for Octave. <a href='http://commits.kde.org/cantor/044d0c6d92c978d0a3144fdb6719bfcb1023cc8b'>Commit.</a> </li>
<li>Solve a problem with cantor_plot() statement, which are appeared in Octave worksheets. <a href='http://commits.kde.org/cantor/2c8d7f3e994619fc10cdbf65f1cce3871ca957cf'>Commit.</a> </li>
<li>Fix kf5.kxmlgui problem with deprecated location of rc files. <a href='http://commits.kde.org/cantor/23d61553d9a8ad91b27ba8eabb0d560b4848f5c0'>Commit.</a> </li>
<li>Save and load latex entries render results into cws. <a href='http://commits.kde.org/cantor/c5f38507003a10c640b51ff6edc7d9d5ee325d74'>Commit.</a> </li>
<li>Remove info about QT_PLUGIN_PATH in README.md. <a href='http://commits.kde.org/cantor/76e58c8863228e2f83234be3b75473363eb02565'>Commit.</a> </li>
<li>Remove the need to configure QT_PLUGIN_PATH before running Cantor by expanding default Qt plugins path. <a href='http://commits.kde.org/cantor/c95d98d7943101cdc15fc98166e08a0a145213a9'>Commit.</a> </li>
<li>Disable qDebug for CMake Release target. <a href='http://commits.kde.org/cantor/05498f43b1ade6c9018ad970c3c3c61438fefbec'>Commit.</a> </li>
<li>Fix warning. <a href='http://commits.kde.org/cantor/6046b0faaca9387b645762d93a161c3badd7c546'>Commit.</a> </li>
<li>Clazy warnings, part 2. <a href='http://commits.kde.org/cantor/89a2d449d9e71a59cdd05716f39a7a823d1d2f44'>Commit.</a> </li>
<li>Update README.md. <a href='http://commits.kde.org/cantor/e12d30a77efeb2a6d96d7f3e7ea9ee5827a07c20'>Commit.</a> </li>
<li>Support python-3.7. <a href='http://commits.kde.org/cantor/6286f14eefeb3fcf984b647d717b58280eaedac5'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Fix loop of FocusIn events. <a href='http://commits.kde.org/dolphin/9616edbb66a8efbdd2bbc9be18e24aaf38a45b59'>Commit.</a> </li>
<li>[KBalooRolesProvider] Support properties of type QStringList. <a href='http://commits.kde.org/dolphin/2f6635f4e8c25d30b8e9483b0611cf613fc6f043'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395033'>#395033</a></li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Fix external application menu occasionally slowing down startup. <a href='http://commits.kde.org/gwenview/07a2e7f9eccd92c19da4ef269f453d61a44b6846'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395925'>#395925</a></li>
<li>Fix issues with cursors and unwanted actions when right-clicking. <a href='http://commits.kde.org/gwenview/20d474e07f4930330f0a998fc11dd62143e41a0e'>Commit.</a> </li>
<li>Reset zoom cursor more reliably. <a href='http://commits.kde.org/gwenview/84e6a0a9a45ec2a84fdee8c584377d0648c98a66'>Commit.</a> </li>
<li>Fix QFileDialog::selectUrl() setting initial directory. <a href='http://commits.kde.org/gwenview/49a52e748904ae41b4991f6828ce50de9c9e05c9'>Commit.</a> </li>
<li>Enable AutoErrorHandling for rename job. <a href='http://commits.kde.org/gwenview/47c66913c6ec11408504cdee7c6519d9224b6798'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395890'>#395890</a></li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Hide]</a></h3>
<ul id='uljuk' style='display: block'>
<li>Use KDE_INSTALL_METAINFODIR. <a href='http://commits.kde.org/juk/3e418eb3b83013e12a1b838ba0dab2ac5539c4dd'>Commit.</a> </li>
</ul>
<h3><a name='kamoso' href='https://cgit.kde.org/kamoso.git'>kamoso</a> <a href='#kamoso' onclick='toggle("ulkamoso", this)'>[Hide]</a></h3>
<ul id='ulkamoso' style='display: block'>
<li>Fix qml warning. <a href='http://commits.kde.org/kamoso/b7cba4f2b0f5cbd3eb4ef6afcb408596fad67f00'>Commit.</a> </li>
<li>Unneedd identifier. <a href='http://commits.kde.org/kamoso/d07920c9f14b12fb36a5bf7cb342e05ac43b8d32'>Commit.</a> </li>
<li>Make sure we don't offer a null geometry. <a href='http://commits.kde.org/kamoso/c827dd3d59b481e0aa97aeb91ffa680c6e563e3a'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Hide]</a></h3>
<ul id='ulkate' style='display: block'>
<li>Fix 'if' to actually have a comparison. <a href='http://commits.kde.org/kate/68bd86421c886a48ccad562c45e927ee81f7c9bf'>Commit.</a> </li>
<li>Fix QuickOpen with Qt 5.11. <a href='http://commits.kde.org/kate/291b68994d7c6176dbc1983c956e2bb7135559e0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395039'>#395039</a></li>
</ul>
<h3><a name='kdebugsettings' href='https://cgit.kde.org/kdebugsettings.git'>kdebugsettings</a> <a href='#kdebugsettings' onclick='toggle("ulkdebugsettings", this)'>[Hide]</a></h3>
<ul id='ulkdebugsettings' style='display: block'>
<li>Disable only debug message as requested by david. <a href='http://commits.kde.org/kdebugsettings/1543d43dca0f1023df6ad727e6c87584fcdf9439'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>Don't try to EXPUNGE read-only mailboxes. <a href='http://commits.kde.org/kdepim-runtime/a07c0b06f030f69922321f8706d6b101ab82c43f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395417'>#395417</a></li>
<li>IMAP: pre-fill current password in the password dialog. <a href='http://commits.kde.org/kdepim-runtime/df9e0b2b68f9d07059514152e665793aa36060d7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395964'>#395964</a></li>
</ul>
<h3><a name='kfind' href='https://cgit.kde.org/kfind.git'>kfind</a> <a href='#kfind' onclick='toggle("ulkfind", this)'>[Hide]</a></h3>
<ul id='ulkfind' style='display: block'>
<li>Changed the minimum required Qt version from 5.6.0 to 5.9.0. <a href='http://commits.kde.org/kfind/c290e496fda461a11135608d022ada53bc65fdd4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393343'>#393343</a></li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Hide]</a></h3>
<ul id='ulkgpg' style='display: block'>
<li>Fix handling of photoid.jpeg.size question when adding photo ids. <a href='http://commits.kde.org/kgpg/ba2658398b6587fe9897e7da3625fac6c0e2ad76'>Commit.</a> </li>
</ul>
<h3><a name='kimap' href='https://cgit.kde.org/kimap.git'>kimap</a> <a href='#kimap' onclick='toggle("ulkimap", this)'>[Hide]</a></h3>
<ul id='ulkimap' style='display: block'>
<li>SELECT: handle READ-ONLY result. <a href='http://commits.kde.org/kimap/1ba33dd9a1e8fa912686a8b7f37a5956939e81b8'>Commit.</a> </li>
<li>Re-enable and update the LoginJobTest. <a href='http://commits.kde.org/kimap/5ceb8b70be4969df14cb2db361a367dfe7cc59f6'>Commit.</a> </li>
<li>Fix STARTTLS support detection. <a href='http://commits.kde.org/kimap/7c4570b17a3bdc94c32ee4ac17e822b25ffb9755'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395249'>#395249</a></li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Hide]</a></h3>
<ul id='ulkio-extras' style='display: block'>
<li>Fix sftp links with new uds implementation. <a href='http://commits.kde.org/kio-extras/bca40518682a038aa08823fbbfb2cc4b9ef63802'>Commit.</a> </li>
</ul>
<h3><a name='kmag' href='https://cgit.kde.org/kmag.git'>kmag</a> <a href='#kmag' onclick='toggle("ulkmag", this)'>[Hide]</a></h3>
<ul id='ulkmag' style='display: block'>
<li>Fix HiDPI support. <a href='http://commits.kde.org/kmag/f76e555c3ee94f06c1dbb43c11870760a47c1f54'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394308'>#394308</a></li>
<li>Fix "Stays On Top" toggling. <a href='http://commits.kde.org/kmag/bf1031a963c6612211c86b309938f9ab91d2194e'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Fix Bug 395353 - kmail doesn't honor Subject and In-Reply-To in mailto: links. <a href='http://commits.kde.org/kmail/da6b4b1b1591d4ddcc504a60e79ed1581c2dd39d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395353'>#395353</a></li>
<li>KMail: don't move the mainwindow to the current desktop when invoking a composer. <a href='http://commits.kde.org/kmail/7b5d2c0fc097f1c9ab2eaa34866a18e70cf82331'>Commit.</a> </li>
<li>Fix typo in argument name. <a href='http://commits.kde.org/kmail/db6f5fba52436f36f588859bb88c3384e0eac3bb'>Commit.</a> </li>
<li>Allow to make working as an email when we forward message as attachment. <a href='http://commits.kde.org/kmail/0b279ab3fe6c170bea296dad60dd233902e3d323'>Commit.</a> </li>
</ul>
<h3><a name='knotes' href='https://cgit.kde.org/knotes.git'>knotes</a> <a href='#knotes' onclick='toggle("ulknotes", this)'>[Hide]</a></h3>
<ul id='ulknotes' style='display: block'>
<li>Fix notes did not drag. <a href='http://commits.kde.org/knotes/d7555e0e92b358a47a0c35262a89ade44a1124e1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358973'>#358973</a></li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Only consider startDragDistance for drag-n-drop tabs. <a href='http://commits.kde.org/konsole/b2d90711a20787daa9dce7e5e5b710725bb3526c'>Commit.</a> </li>
</ul>
<h3><a name='kontactinterface' href='https://cgit.kde.org/kontactinterface.git'>kontactinterface</a> <a href='#kontactinterface' onclick='toggle("ulkontactinterface", this)'>[Hide]</a></h3>
<ul id='ulkontactinterface' style='display: block'>
<li>PimUniqueApplication: make newInstance virtual. <a href='http://commits.kde.org/kontactinterface/926f4d843afc416f5d2a802cad1a79cbf4fd0c67'>Commit.</a> </li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Hide]</a></h3>
<ul id='ulkopete' style='display: block'>
<li>Fix pipes plugin build (missing moc include). <a href='http://commits.kde.org/kopete/5995be5d2b4eecf15c5493888a7cc4cb5de425ea'>Commit.</a> </li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Hide]</a></h3>
<ul id='ulkpimtextedit' style='display: block'>
<li>Fix default size. <a href='http://commits.kde.org/kpimtextedit/b065f8b146e37f5de1d6c9e3c502cfc40249dd63'>Commit.</a> </li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Hide]</a></h3>
<ul id='ullibksieve' style='display: block'>
<li>Fix show new email. <a href='http://commits.kde.org/libksieve/f321a3fb9d540dfb3594c27c89a652b09e103fa5'>Commit.</a> </li>
<li>Fix create script job. <a href='http://commits.kde.org/libksieve/bea3030c42e1355110fad8e570f1a1401060ab3e'>Commit.</a> </li>
<li>Fix bug found by David. <a href='http://commits.kde.org/libksieve/20c3cd9f4e2a4b423dd29afcca7b2afcd1a7dcaa'>Commit.</a> </li>
<li>Fix found active or not vacation script. <a href='http://commits.kde.org/libksieve/e46a75d7394be114bfc1b5045f7975bd6cfbe046'>Commit.</a> </li>
<li>Fix close dialogbox. <a href='http://commits.kde.org/libksieve/8a40d96230f0e428c15df28b5a79439dfa518187'>Commit.</a> </li>
<li>Fix generate USER script. <a href='http://commits.kde.org/libksieve/0eea39e083768173db8463ad138760e7495f5cdd'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Hide]</a></h3>
<ul id='ulmailcommon' style='display: block'>
<li>FolderSettings: Fix "no resource found in collection" assert. <a href='http://commits.kde.org/mailcommon/cbd7b6a08443e5cb082fb3d0d8771661cde33a36'>Commit.</a> </li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Hide]</a></h3>
<ul id='ulmarble' style='display: block'>
<li>Fix build of tour-preview example. <a href='http://commits.kde.org/marble/4c3740819af0d128533aa1b90d53beb1199e7d60'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Avoid to display all message in preview. <a href='http://commits.kde.org/messagelib/6c2755f2d59926a4d4d4cc62663f2bf2e81a2bf5'>Commit.</a> </li>
<li>Fix dnd attachment with qt5.11. <a href='http://commits.kde.org/messagelib/fe7ac5b4d9112d75115420a0d22dedb900a8f3d2'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Do not crash on right-click on TOC item with no associated page. <a href='http://commits.kde.org/okular/14dc396cdb110b35cc76864ca8397586dbd9b81e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396326'>#396326</a></li>
</ul>
<h3><a name='pim-data-exporter' href='https://cgit.kde.org/pim-data-exporter.git'>pim-data-exporter</a> <a href='#pim-data-exporter' onclick='toggle("ulpim-data-exporter", this)'>[Hide]</a></h3>
<ul id='ulpim-data-exporter' style='display: block'>
<li>Fix Bug 395911 - pimsettingsexporter "contacts" directory path incorrect. <a href='http://commits.kde.org/pim-data-exporter/7c50e9a829770aa0b23b153c4a80a2f1d575d924'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395911'>#395911</a></li>
</ul>