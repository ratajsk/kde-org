---
title : "KDE 2.2.2 Info Page"
unmaintained: true
publishDate: 2001-11-21
---

<p>
KDE 2.2.2 was released on November 21, 2001. Read the
<a href="/announcements/announce-2.2.2">official announcement</a>.
</p>

<p>For a list of changes since KDE 2.2.1, see the
<a href="/announcements/changelogs/changelog2_2_1to2_2_2">list of changes</a></p>

<p>For a high-level overview of the features of KDE, see the
<a href="/info">KDE info page</a></p>

<p>For a graphical tutorial on using KDE 2, see this
<a href="http://www.linux-mandrake.com/en/demos/Tutorial/">tutorial page</a> from Linux Mandrake</p>

<p>This page will be updated to reflect changes in the status of
2.2.2 so check back for new information.</p>

<h2>FAQ</h2>

See the <a href="faq.php">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed to the
<a href="http://konqueror.kde.org/faq/">Konqueror FAQ</a> and sound related
questions are answered in the <a href="http://www.arts-project.org/doc/handbook/faq.html">Arts FAQ</a>

<h2>Download and Installation</h2>

See the links listed in the <a
href="/announcements/announce-2.2.2.php">announcement</a>. The KDE
<a href="/documentation/faq/install.html">Installation FAQ</a>
provides generic instruction about installation issues.

<p>If you want to compile from sources we offer
<a href="http://developer.kde.org/build/">instructions</a> and help for common
problems in the <a href="http://developer.kde.org/build/compilationfaq.html">Compilation
FAQ</a>.</p>

<h2>Updates</h2>

<p>Please refer to <a href="/info/3.0.php">KDE 3.0</a>,
the next stable release.</p> 

<h2>Security Issues</h2>

<ul>
<li>
<p> Konqueror fails to correctly initialize the site domains for sub-(i)frames
    and may as a result allow access to forein cookies. 
</p>
<p>
It is strongly recommended to upgrade at least kdelibs to KDE 3.0.3a in which
this bug is fixed. 
</p>
<p><a href="ftp://ftp.kde.org/pub/kde/security_patches/post-2.2.2-kdelibs-khtml.diff">A patch</a>
   is also available for download to address this particular problem.
</p>
</li>
<li>
<p>KDE's SSL implementation fails to check the basic constraints on certificates 
and as a result may accept certificates as valid that were signed by an 
issuer who was not authorized to do so.
</p>
<p>
Due to this, users of Konqueror and other SSL enabled KDE software may fall victim to a 
malicious man-in-the-middle attack without noticing. In such case the user 
will be under the impression that there is a secure connection with a trusted 
site while in fact a different site has been connected to.
</p>
<p>
We recommend users to upgrade to at least KDE 3.0.3 but a 
<a href="ftp://ftp.kde.org/pub/kde/security_patches/post-2.2.2-kdelibs-kssl.diff">
patch for KDE 2.2.2</a> is also available.
</p>
</li>
<li>A Denial of Service vulnerability has been found in the
<a href="http://www.arts-project.org/">aRts</a> soundserver. All versions of
KDE 2.2.x and KDE 3.0.x are affected. If you allow untrusted users to login,    it is recommended to remove the sUID bit of the <tt>artswrapper</tt> application. To achieve this, please
run the following command in the directory <tt>artswrapper</tt> is installed in:
<pre>
  chmod u-s artswrapper
</pre>
</li>
<li>
<p>Several buffer overflows have been found in code KGhostview shared from
    other postscript viewers. Read the <a href="security/advisory-20021008-1.txt">detailed 
    advisory</a>. Update to KDE 3.0.4 is recommended. 
</p>
<p><a href="ftp://ftp.kde.org/pub/kde/security_patches/kghostview-2.2.2.diff">A patch</a>
   is also available for download to address this particular problem.
</p>
</li>
<li>Several vulnerabilites have been found in LISa/resLISa and the rlan:// protocol, 
 including the possibility to escalate the privileges to root via a remote attack. See the 
<a href="http://www.kde.org/info/security/advisory-20021111-1.txt">detailed advisory</a> for
 an explanation and instructions for immediate workaround. There is no patch available, other
than the recommended workarounds you're encouraged to upgrade to a KDE 3.x release. 
<b> The use of LISa/resLISa is strongly discouraged
in any security relevant area. Never make it available outside your local, trusted network.</b> </li>
<li>the rlogin and the telnet protocol implementation in KIO allows remote command execution. See the
<a href="http://www.kde.org/info/security/advisory-20021111-2.txt">detailed advisory</a> for
an explanation and instructions for immediate workaround. There is no patch available, other
than the recommended workarounds you're encouraged to upgrade to a KDE 3.x release. 
</li>
<li>
<p>Several shell escaping vulnerabilities have been found throughout KDE which allow a remote attacker to execute commands as the local user.
   Read the <a href="security/advisory-20021220-1.txt">detailed 
   advisory</a>. It is strongly recommended to update to KDE 3.0.5a. 
</p>
<p><a href="ftp://ftp.kde.org/pub/kde/security_patches/">Several patches</a>
   that address these issues have been made available for those who are unable 
   to update to KDE 3.x.
</p>
</li>
<li>
<p>Several problems with KDE's use of Ghostscript where discovered that allow the execution of
arbitrary commands contained in PostScript (PS) or PDF files with the privileges of the victim.
Read the <a href="security/advisory-20030409-1.txt">detailed advisory</a>. 
It is strongly recommended to update to KDE 3.1.1a
</p>
<p><a href="ftp://ftp.kde.org/pub/kde/security_patches/">Several patches</a>
   that address these issues have been made available for those who are unable 
   to update to KDE 3.x.
</p>
</li>
<li>
<p>
KDE's SSL implementation in KDE 2.x matches certificates
based on IP number instead of hostname. Due to this, users of Konqueror and 
other SSL enabled KDE software may fall victim
to a malicious man-in-the-middle attack without noticing. In such case the
user will be under the impression that there is a secure connection with a
trusted site while in fact a different site has been connected to.
Read the <a href="security/advisory-20030602-1.txt">detailed advisory</a>. 
</p>
<p>
We recommend users to upgrade to the KDE 3.x series but patches 
for KDE 2.2.2 
[<a href="ftp://ftp.kde.org/pub/kde/security_patches/post-2.2.2-kdelibs-kio-2.diff">1</a>,
<a href="ftp://ftp.kde.org/pub/kde/security_patches/post-2.2.2-kdelibs-kssl-2.diff">2</a>]
are also available.
</p>
</li>
<li>
A HTTP authentication credentials leak via the a "Referrer" was discovered by George Staikos
in Konqueror. If the HTTP authentication credentials were part of the URL they would be possibly sent
in the referer header to a 3rd party web site.
Read the <a href="security/advisory-20030729-1.txt">detailed advisory</a>. KDE 3.1.3 and newer
are not vulnerable.
We recommend users to upgrade to the KDE 3.x series but patches
for KDE 2.2.2
[<a href="ftp://ftp.kde.org/pub/kde/security_patches/post-2.2.2-kdelibs-http.patch">patch</a>]
are also available.
</li>
</ul>

<h2>Bugs</h2>

<p>No major bugs are known.</p>

<p>Please check the <a href="http://bugs.kde.org">bug database</a>
before filing any bug reports. Also check for possible updates that
might fix your problem.</p>

<h2>Developer Info</h2>

If you need help porting your application to KDE 2.x see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/2.2/kdelibs/KDE2PORTING.html">
porting guide</a> or discuss your problems with fellow developers
on the <a href="http://mail.kde.org/mailman/listinfo/kde2-porting">&#107;&#100;&#101;&#x32;-por&#x74;&#105;&#x6e;g&#0064;kde&#46;org</a>
mailing list. 
<p>There is also info on the <a
href="http://developer.kde.org/documentation/library/kdeqt/kde2arch/index.html">architecture</a>
and the <a
href="http://developer.kde.org/documentation/library/2.2-api/classref/index.html">
programming interface of KDE 2.2.2</a>.</p>

<!-- END CONTENT -->
<?php
  include "footer.inc";
?>
