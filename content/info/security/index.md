---
title: KDE Security Advisories
---

## Reporting

Please send all security concerns and findings to <a href="mailto:secur&#105;&#116;&#121;&#64;&#107;&#100;e.org">s&#101;&#99;&#117;r&#105;ty&#64;k&#100;&#101;&#46;&#111;&#114;g</a>.

If you are under active threat of interception, and the information you desire to transmit is exceedingly sensitive, you may send it to one of individual team members listed below using PGP, who may then choose to
forward the message to other team members or take other appropriate action, as determined by the security team recipient. However, due to time constraints, you are far more likely to receive a prompt response by contacting security@kde.org.

<table border=1 class="mt-3 mb-3 table">
<tr>
<th>Name</th><th>Email</th><th>PGP Key</th>
</tr>
<tr>
<td>David Faure</td><td>faure@kde.org</td><td><a href="
http://pgp.mit.edu/pks/lookup?op=get&search=0x53E6B47B45CEA3E0D5B7457758D0EE648A48B3BB
">53E6B47B45CEA3E0D5B7457758D0EE648A48B3BB</a></td>
</tr>
<tr>
<td>Albert Astals Cid</td><td>aacid@kde.org</td><td><a href="
http://pgp.mit.edu/pks/lookup?op=get&search=0x8692A42FB1A8B666C51053919D17D97FD8224750
">8692A42FB1A8B666C51053919D17D97FD8224750</a></td>
</tr>
<tr>
<td>Jason A. Donenfeld</td><td>jdonenfeld@kde.org</td><td><a href="
http://pgp.mit.edu/pks/lookup?op=get&search=0x3C16CDE0E58B28C80A5A361DC3BD4FF850130054
">3C16CDE0E58B28C80A5A361DC3BD4FF850130054</a></td>
</tr>
</table>

Reported security problems are handled according to the <a href="policy.php">KDE Security Policy</a>.

## Advisories

The KDE Security Advisories are crosslinked in the KDE Information Pages of
the KDE versions to which they apply to. The listing below is in chronological
order.

+ <a href="./advisory-20220216-1.txt">2022-02-16 kcron: Invalid temporary file handling.</a>
+ <a href="./advisory-20220131-1.txt">2022-01-31 KTextEditor/Kate: Missing validation of binaries executed via QProcess.</a>
+ <a href="./advisory-20211118-1.txt">2021-11-18 KMail: Encryption is ignored when "Server requires authentication" not checked in UI.</a>
+ <a href="./advisory-20211118-2.txt">2021-11-18 KMail: Endless loop, if the TLS certificate marked as bad.</a>
+ <a href="./advisory-20210429-1.txt">2021-04-29 KMail: Deleting attachments can disclose content of encrypted messages.</a>
+ <a href="./advisory-20210310-1.txt">2021-03-10 Discover: Missing URI scheme validation.</a>
+ <a href="./advisory-20201017-1.txt">2020-10-17 KDE Partition Manager: kpmcore_externalcommand helper can be exploited in local privilege escalation.</a>
+ <a href="./advisory-20201002-1.txt">2020-10-02 KDE Connect: packet manipulation can be exploited in a Denial of Service attack.</a>
+ <a href="./advisory-20200827-1.txt">2020-08-27 Ark: maliciously crafted TAR archive with symlinks can install files outside the extraction directory.</a>
+ <a href="./advisory-20200730-1.txt">2020-07-30 Ark: maliciously crafted archive can install files outside the extraction directory.</a>
+ <a href="./advisory-20200510-1.txt">2020-05-10 kio_fish stores the typed password in KWallet even if the user doesn't check the "Remember" box.</a>
+ <a href="./advisory-20200312-1.txt">2020-03-12 Okular: Local binary execution via action links.</a></li>
+ <a href="./advisory-20190807-1.txt">2019-08-07 kconfig: malicious .desktop files (and others) would execute code.</a>
+ <a href="./advisory-20190721-1.txt">2019-07-21 Windows: Incorrect behavior of uninstall.exe.</a>
+ <a href="./advisory-20190209-1.txt">2019-02-09 kauth: Insecure handling of arguments in helpers.</a>
+ <a href="./advisory-20181128-1.txt">2018-11-28 messagelib: HTML email can open browser window automatically.</a>
+ <a href="./advisory-20181112-1.txt">2018-11-12 kio-extras: HTML Thumbnailer automatic remote file access.</a>
+ <a href="./advisory-20180503-1.txt">2018-05-03 kwallet-pam: Access to privileged files.</a>
+ <a href="./advisory-20180208-2.txt">2018-02-08 Plasma Desktop: Arbitrary command execution in the removable device notifier.</a>
+ <a href="./advisory-20180208-1.txt">2018-02-08 Plasma: Notifications can expose user IP address.</a>
+ <a href="./advisory-20171112-1.txt">2017-11-12 Konversation: Crash in IRC message parsing.</a>
+ <a href="./advisory-20170615-1.txt">2017-06-15 KMail: Send Later with Delay bypasses OpenPGP.</a>
+ <a href="./advisory-20170510-2.txt">2017-05-10 smb4k: unauthorized local command execution as root.</a>
+ <a href="./advisory-20170510-1.txt">2017-05-10 kauth: Local privilege escalation.</a>
+ <a href="./advisory-20170228-1.txt">2017-02-28 kio: Information Leak when accessing https when using a malicious PAC file.</a>
+ <a href="./advisory-20170227-1.txt">2017-02-27 ktnef: Directory Traversal.</a>
+ <a href="./advisory-20170214-1.txt">2017-02-14 Kopete: XMPP User Impersonation Vulnerability.</a>
+ <a href="./advisory-20170112-1.txt">2017-01-12 Ark: unintended execution of scripts and executable files.</a>
+ <a href="./advisory-20161114-1.txt">2016-11-14 KDE neon: insecure package archive.</a>
+ <a href="./advisory-20161006-1.txt">2016-10-06 KMail: HTML injection in plain text viewer.</a>
+ <a href="./advisory-20161006-2.txt">2016-10-06 KMail: JavaScript access to local and remote URLs.</a>
+ <a href="./advisory-20161006-3.txt">2016-10-06 KMail: JavaScript execution in HTML Mails.</a>
+ <a href="./advisory-20160930-1.txt">2016-09-30 kdesu: Displayed command truncated by unicode string terminator.</a>
+ <a href="./advisory-20160724-1.txt">2016-07-24 karchive: KNewstuff downloads can install files outside the extraction directory.</a>
+ <a href="./advisory-20160621-1.txt">2016-06-21 kinit: World readable X11 Cookie key logger</a>
+ <a href="./advisory-20160209-1.txt">2016-02-09 Turning all screens off while the lock screen is shown can result in the screen being unlocked when turning a screen on again.</a>
+ <a href="./advisory-20150122-2.txt">2015-01-22 kde-workspace, plasma-workspace: X11 clients can eavesdrop input events while screen is locked</a>
+ <a href="./advisory-20150122-1.txt">2015-01-22 plasma-workspace: Network access from screen locker</a>
+ <a href="./advisory-20150109-1.txt">2015-01-09 kwallet: Fix CBC encryption handling</a>
+ <a href="./advisory-20141113-1.txt">2014-11-13 kwebkitpart, kde-runtime, kio-extras: Insufficient Input Validation</a>
+ <a href="./advisory-20141106-1.txt">2014-11-06 kde-workspace, plasma-desktop: privilage escalation</a>
+ <a href="./advisory-20141104-1.txt">2014-11-04 Konversation out-of-bounds read on a heap-allocated array</a>
+ <a href="./advisory-20140923-1.txt">2014-09-23 krfb multiple security issues in libvncserver</a>
+ <a href="./advisory-20140803-1.txt">2014-08-03 krfb integer overflow</a>
+ <a href="./advisory-20140730-1.txt">2014-07-30 KAuth PID Reuse Flaw</a>
+ <a href="./advisory-20140618-1.txt">2014-06-18 KMail/KIO POP3 SSL MITM Flaw</a>
+ <a href="./advisory-20120810-1.txt">2012-08-10 Calligra and KOffice Input Validation Failure</a>
+ <a href="./advisory-20111003-1.txt">2011-10-03 KSSL and Rekonq Input Validation Failure</a>
+ <a href="./advisory-20110411-1.txt">2011-04-11 Konqueror Partially Universal XSS Vulnerability in Error Pages</a>
+ <a href="./advisory-20100825-1.txt">2010-08-25 Okular PDB Processing Memory Corruption Vulnerability</a>
+ <a href="./advisory-20100513-1.txt">2010-05-13 KGet Directory Traversal and Insecure File Operation Vulnerabilities</a>
+ <a href="./advisory-20100413-1.txt">2010-04-13 KDM Local Privilege Escalation Vulnerability</a>
+ <a href="./advisory-20100217-1.txt">2010-02-17 KRunner lock module race condition</a>
+ <a href="./advisory-20091027-1.txt">2009-10-27 XMLHttpRequest vulnerability and kioslave input validation issues</a>
+ <a href="./advisory-20080426-2.txt">2008-04-26 start_kdeinit multiple vulnerabilities</a>
+ <a href="./advisory-20080426-1.txt">2008-04-26 KHTML PNG Loader Buffer Overflow</a>
+ <a href="./advisory-20071107-1.txt">2007-11-07 kpdf/kword/xpdf multiple xpdf based vulnerabilities</a>
+ <a href="./advisory-20070919-1.txt">2007-09-19 KDM passwordless login vulnerability</a>
+ <a href="./advisory-20070914-1.txt">2007-09-14 Konqueror address bar spoofing</a>
+ <a href="./advisory-20070730-1.txt">2007-07-30 kpdf/kword/xpdf stack based buffer overflow</a>
+ <a href="./advisory-20070326-1.txt">2007-03-26 KIO FTP ioslave PASV vulnerability</a>
+ <a href="./advisory-20070206-1.txt">2007-02-06 KHTML/Konqueror &lt;title&gt; XSS vulnerability</a>
+ <a href="./advisory-20070115-1.txt">2007-01-15 kpdf/kword/xpdf denial of service vulnerability</a>
+ <a href="./advisory-20070109-1.txt">2007-01-09 ksirc denial of service vulnerability</a>
+ <a href="./advisory-20061205-1.txt">2006-12-05 KOffice OLEfilter integer overflow</a>
+ <a href="./advisory-20061129-1.txt">2006-11-29 JPEG-EXIF Meta Information DoS vulnerability</a>
+ <a href="./advisory-20060614-1.txt">2006-06-14 KDM symlink attack vulnerability</a>
+ <a href="./advisory-20060614-2.txt">2006-06-14 artswrapper return value checking vulnerability</a>
+ <a href="./advisory-20060404-1.txt">2006-04-04 Kaffeine http_peek() buffer overflow</a>
+ <a href="./advisory-20060310-1.txt">2006-03-10 kpdf/xpdf heap based buffer overflow</a>
+ <a href="./advisory-20060202-1.txt">2006-02-02 kpdf/xpdf heap based buffer overflow</a>
+ <a href="./advisory-20060119-1.txt">2006-01-19 kjs encodeuri/decodeuri heap overflow vulnerability</a>
+ <a href="./advisory-20051207-2.txt">2006-01-03 kpdf/xpdf multiple integer overflows</a>
+ <a href="./advisory-20051011-1.txt">2005-10-11 KOffice KWord RTF import buffer overflow</a>
+ <a href="./advisory-20050905-1.txt">2005-09-05 kcheckpass local root vulnerability</a>
+ <a href="./advisory-20050815-1.txt">2005-08-15 langen2kvtml tempfile handling vulnerability</a>
+ <a href="./advisory-20050809-1.txt">2005-08-09 kpdf infinite temp file DoS</a>
+ <a href="./advisory-20050721-1.txt">2005-07-21 libgadu multiple vulnerabilities</a>
+ <a href="./advisory-20050718-1.txt">2005-07-18 Kate backup file permission leak</a>
+ <a href="./advisory-20050504-1.txt">2005-05-04 Patch updates for kimgio and Kommander</a>
+ <a href="./advisory-20050421-1.txt">2005-04-21 kimgio input validation errors</a>
+ <a href="./advisory-20050420-1.txt">2005-04-20 Kommander untrusted code execution</a>
+ <a href="./advisory-20050316-1.txt">2005-03-16 Local DCOP denial of service vulnerability</a>
+ <a href="./advisory-20050316-2.txt">2005-03-16 Konqueror International Domain Name Spoofing</a>
+ <a href="./advisory-20050316-3.txt">2005-03-16 Insecure temporary file creation by dcopidlng</a>
+ <a href="./advisory-20050228-1.txt">2005-02-28 kppp Privileged fd Leak Vulnerability</a>
+ <a href="./advisory-20050215-1.txt">2005-02-15 Buffer overflow in fliccd of kdeedu/kstars/indi</a>
+ <a href="./advisory-20050121-1.txt">2005-01-21 Multiple vulnerabilities in Konversation</a>
+ <a href="./advisory-20050120-1.txt">2005-01-20 KOffice PDF import filter buffer overflow (third)</a>
+ <a href="./advisory-20050119-1.txt">2005-01-19 kpdf buffer overflow</a>
+ <a href="./advisory-20050101-1.txt">2005-01-01 ftp kioslave command injection</a>
+ <a href="./2004_xpdf_integer_overflow_2.php">2004-12-23 KOffice PDF importer integer overflow vulnerability</a>
+ <a href="./advisory-20041223-1.txt">2004-12-23 kpdf Buffer Overflow Vulnerability</a>
+ <a href="./advisory-20041220-1.txt">2004-12-20 Konqueror Java Vulnerability</a>
+ <a href="./advisory-20041213-1.txt">2004-12-13 Konqueror Window Injection Vulnerability</a>
+ <a href="./advisory-20041209-2.txt">2004-12-09 kfax libtiff vulnerabilities</a>
+ <a href="./advisory-20041209-1.txt">2004-12-09 plain text password exposure</a>
+ <a href="./2004_xpdf_integer_overflow.php">2004-10-30 KOffice PDF importer integer overflow vulnerability</a>
+ <a href="./advisory-20041021-1.txt">2004-10-21 multiple kpdf integer overflows</a>
+ <a href="./advisory-20040823-1.txt">2004-08-23 Konqueror Cross-Domain Cookie Injection</a>
+ <a href="./advisory-20040811-1.txt">2004-08-11 Temporary Directory Vulnerability</a>
+ <a href="./advisory-20040811-2.txt">2004-08-11 DCOPServer Temporary Filename Vulnerability</a>
+ <a href="./advisory-20040811-3.txt">2004-08-11 Konqueror Frame Injection Vulnerability</a>
+ <a href="./advisory-20040517-1.txt">2004-05-17 URI Handler Vulnerabilities</a>
+ <a href="./advisory-20040114-1.txt">2004-01-14 VCF file information reader vulnerability</a>
+ <a href="./advisory-20030916-1.txt">2003-09-16 KDM local root / weak session cookie generation</a>
+ <a href="./advisory-20030729-1.txt">2003-07-29 Konqueror HTTP Authentication credential leak</a>
+ <a href="./advisory-20030602-1.txt">2003-06-02 KDE 2.2 / Konqueror Embedded SSL vulnerability</a>
+ <a href="./advisory-20030409-1.txt">2003-04-09 PS/PDF file handling vulnerability</a>
+ <a href="./advisory-20021220-1.txt">2002-12-20 Multiple KDE vulnerabilities</a>
+ <a href="./advisory-20021111-2.txt">2002-11-11 resLISa / LISa Vulnerabilities</a>
+ <a href="./advisory-20021111-1.txt">2002-11-11 rlogin.protocol and telnet.protocol URL KIO Vulnerability</a>
+ <a href="./advisory-20021008-2.txt">2002-10-08 kpf Directory traversal</a>
+ <a href="./advisory-20021008-1.txt">2002-10-08 KGhostview Arbitrary Code Execution</a>
+ <a href="./advisory-20020908-2.txt">2002-09-08 Konqueror Cross Site Scripting Vulnerability </a>
+ <a href="./advisory-20020908-1.txt">2002-09-08 Secure Cookie Vulnerability</a>
+ <a href="./advisory-20020818-1.txt">2002-08-18 Konqueror SSL vulnerability</a>
+ <a href="./advisory-19981118-1.txt">1998-11-18 KDE Screensaver Vulnerability</a>
