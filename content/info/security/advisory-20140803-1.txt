KDE Project Security Advisory
=============================

Title:          krfb: possible denial of service or code execution via integer overflow
Risk Rating:    Medium
CVE:            CVE-2014-4607
Platforms:      All
Versions:       krfb < 4.14
Author:         Albert Astals Cid <aacid@kde.org>
Date:           3 August 2014

Overview
========

krfb embeds libvncserver which embeds liblzo2, it contains various flaws
that result in integer overflow problems.

Impact
======

This potentially allows a malicious application to create a possible denial of service or code execution.
Due to the need to exploit precise details of the target architecture and threading
it is unlikely that remote code execution can be achieved in practice.

Workaround
==========

None.

Solution
========

Upgrade to krfb 4.14 or apply one of these two patches:
fix vulnerability: http://quickgit.kde.org/?p=krfb.git&a=commit&h=0bff5df104906c7e45545817c26c7e4907adc569
unbundle libvncserver: http://quickgit.kde.org/?p=krfb.git&a=commit&h=1c85dc7d85570c9e3a5fcc57572feb04e57fe6db

Credits
=======

Thanks to Don A. Bailey for finding the flaw http://seclists.org/oss-sec/2014/q2/665
Thanks to Johannes Schindelin for fixing libvncserver https://github.com/LibVNC/libvncserver/commit/3238443dc1ff5e7d0a9457dce5e0fb46c7bc1624
Thanks to Martin Sansmark for applying the patch in krfb repository.
Thanks to Johannes Huber for creating the patch to unbundle libvncserver.