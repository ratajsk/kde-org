KDE Project Security Advisory
=============================

Title:          Konversation: out-of-bounds read on a heap-allocated array
Risk Rating:    Low
CVE:            CVE-2014-8483
Platforms:      All
Versions:       konversation < 1.5.1
Author:         Eike Hein <hein@kde.org>
Date:           04 November 2014

Overview
========

Konversation's Blowfish ECB encryption support assumes incoming blocks
to be the expected 12 bytes. The lack of a sanity-check for the actual
size can cause a denial of service and an information leak to the local
user.

Konversation 1.5.1 has been released with a patch to address this issue.

Impact
======

When using Blowfish ECB encryption with another party (an IRC channel
or user), sending malformed blocks to Konversation can result in a
crash or an information leak up to 11 bytes to the local user, due to
an out-of-bounds read on a heap-allocated array.

Solution
========

Upgrade Konversation to 1.5.1 (released November 4th, 2014) or apply
the following patch to Konversation 1.5:

http://quickgit.kde.org/?p=konversation.git&a=commit&h=1f55cee8b3d0956adc98834f7b5832e48e077ed7

Credits
=======

Thanks to Pierre Schweitzer for bringing the bug to the attention of
oss-security.

Thanks to Manuel Nickschas from Quassel for notifying us about the
problem (Quassel incorporated the faulty Konversation code in 2009)
and writing the patch adopted for Konversation 1.5.1.