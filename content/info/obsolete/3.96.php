<?php
  $page_title = "KDE 3.96 Info Page";
  $site_root = "../";
  include "header.inc";
?>

<p>
<a href="../announcements/announce-4.0-rc1.php">KDE 3.96 was released</a> on November 20th, 2007.
</p>

<p>This page will be updated to reflect changes in the status of 3.96
release cycle so check back for new information.</p>

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="m&#x61;i&#00108;&#x74;o:&#115;ec&#117;&#x72;&#00105;&#x74;&#121;&#x40;kde.&#00111;&#x72;g">&#x73;&#101;&#x63;u&#114;i&#x74;y&#x40;kd&#101;.&#x6f;&#00114;&#103;</a>.</p>

<p>Unstable KDE releases are not supported by security updates, however if you find issues then
please do report them.</p>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release was packaged:</p>

<ul>
<li>The version number within kdelibs has been incorrect for the released tarballs.
The application version will probably default to "KDE 4.0 Beta4", which makes identifying
your bugreports against this new version difficult.
<a href="ftp://ftp.kde.org/pub/kde/unstable/3.96/src/kdelibs-3.96-fix-version.diff">Please
apply the patch to fix the version number.</a></li>
</ul>

<p>Please check the <a href="http://bugs.kde.org/">bug database</a>
before filing any bug reports. Also check for possible updates on this page
that might describe or fix your problem.</p>

<h2>FAQ</h2>

See the <a href="faq.php">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed
<a href="http://konqueror.kde.org/faq/">to the Konqueror FAQ</a>.

<h2>Download and Installation</h2>

<p>
<u>Build instructions</u>.
 <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">Build instructions</a>
 are separately maintained.
</p>
<p>The KDE release is split into a platform release and a desktop release.
</p>

<h3><a name='platform'>KDE 4.0 Development Platform release</a></h3>

<p>The KDE 4.0 Development Platform release consists of the modules kdelibs, kdepimlibs
and kdebase-runtime. It is the minimum number of modules that are needed to build
and run KDE 4.0 applications. Install this if you want to build and run a 3rd party
application.</p>

<p>
  The complete source code for KDE 3.96 (platform release) is available for download:
</p>

<?php
include "source-3.96-platform.inc"
?>

<h3><a name='desktop'>KDE 4.0 Desktop RC1 release</a></h3>

<p>The KDE 4.0 desktop release consists of all KDE application modules
and the KDE 4.0 workspace module which provides a desktop environment to you.
Install this <em>after you installed the KDE 4.0 platform</em> if you want
to run KDE 4.0 on your desktop.</p>

<p>
  The complete source code for KDE 3.96 (platform release) is available for download:
</p>

<?php
include "source-3.96.inc"
?>

<u><a name="binary">Binary packages</a></u>

<p>
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.96 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.96/">http or FTP mirrors</a>.
</p>

<p>
  Currently pre-compiled packages are available for:
</p>

<?php
include "binary-3.96.inc"
?>

<h2>Developer Info</h2>

<p>
If you need help porting your application to KDE 4 see the <a
href="http://websvn.kde.org/*checkout*/trunk/KDE/kdelibs/KDE4PORTING.html">
porting guide</a> or subscribe to the
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">KDE Devel Mailinglist</a>
to ask specific questions about porting your applications.
<p>

<?php
  include "footer.inc";
?>
