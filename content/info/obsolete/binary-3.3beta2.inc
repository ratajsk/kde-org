<ul>

<!-- KDE RedHat -->
<li>
 <a href="http://kde-redhat.sourceforge.net/">KDE RedHat (unofficial) Packages</a>
 (<a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.92/Fedora2-uo/README.txt">README</a>)
  :
 <ul type="disc">
   <li>
     Fedora Core 2: <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.92/Fedora2-uo/RPMS/">Intel i386</a>
   </li>
 </ul>
 <p />
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution)
 (<a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.92/contrib/Slackware/10.0/README">README</a>)
   :
   <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.92/contrib/Slackware/noarch/">Language packages</a></li>
    </li>
     <li>
       10.0: <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.92/contrib/Slackware/10.0/">Intel i486</a>
     </li>
   </ul>
  <p />
</li>

<!--   SUSE LINUX -->
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
<!--  (<a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.92/SuSE/README">README</a>) -->
  :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.92/SuSE/noarch/">Language
        packages</a> (all versions and architectures)</li>
    </li>
    <li>
      9.1:
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.92/SuSE/ix86/9.1/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.92/SuSE/x86_64/9.1/">AMD x86-64</a>
    </li>
    <li>
      9.0:
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.92/SuSE/ix86/9.0/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.92/SuSE/x86_64/9.0/">AMD x86-64</a>
    </li>
    <li>
      8.2:
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.92/SuSE/ix86/8.2/">Intel i586</a> and
    </li>
  </ul>
  <p />
</li>

<!-- YOPER LINUX -->
<li>
  <a href="http://www.yoper.com/">Yoper</a>:
  <ul type="disc">
    <li>
      1.0:
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.92/Yoper/">Intel i686 rpm</a>
    </li>
  </ul>
  <p />
</li>

</ul>
